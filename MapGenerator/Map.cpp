
#include "stdio.h"
#include <iostream>
#include <vector>
#include <random>
#include <iostream>
using namespace std;
#define SIZE_I 11
#define SIZE_J 11
#define NUM_TURRET 5
#define NUM_RESPAWN 1


int generarRandom(double in, double out){
	std::random_device rd;
    std::mt19937 mt(rd());
	std::uniform_real_distribution<double> dist(in, out);
	return (int) dist(mt);
}

void generarMapa(){
	std::string myMap[SIZE_I][SIZE_J];
	int in_turret = 0;
	int in_respawn = 0;
	int numNucleo = 0;
	
	for (size_t i = 0; i < SIZE_I; ++i){
    	for (size_t j = 0; j < SIZE_J; ++j){
    			bool in = true;
        		if (i == (SIZE_I/2) && j == (SIZE_J/2)){
        			myMap[i][j] = "N";
        			numNucleo = ((( i*10 )) +j);
					in = false;
				}
				if ( (i == 0 || j == 0 || j == (SIZE_J - 1) || i == (SIZE_I - 1)) && in ){
					myMap[i][j] = "P";
					in = false;
				} 
				if(in){
					myMap[i][j] = "0";
				}	
		}
	}
				
	bool generarCam = true;
	int unidad = numNucleo % 10;
	int decena = (numNucleo / 10) % 10;
				
	while(generarCam){
		for (int i = 0; i < (SIZE_J/2 - 1) ; ++i){
		
			std::cout << unidad << " " << decena << std::endl;
			decena = decena -1 ;
			std::cout << unidad << " " << decena << std::endl;
			
			myMap[unidad][decena] = "C";	

			if(myMap[unidad][decena-1] == "P"){
				generarCam = false;
				myMap[unidad][decena] = "R";
			}
			
		}	
	}

	for (size_t i = 0; i < SIZE_I; ++i){
    	for (size_t j = 0; j < SIZE_J; ++j){
    		if(myMap[i][j] == "0"){
    			myMap[i][j] = "T";	
    		}
		}
	}
		
	for (size_t p = 0; p < SIZE_I; ++p){
    	for (size_t l = 0; l < SIZE_J; ++l){
        	std::cout << myMap[p][l];	
		}
		std::cout << std::endl;
	}
}



int main(int argc, char const *argv[]){

	generarMapa();
	return 0;

}





