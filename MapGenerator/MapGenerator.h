
#ifndef MapGenerator_H
#define MapGenerator_H

#include "stdio.h"
#include <iostream>
#include <vector>
#include <random>
#include <iostream>
#include <Constants.h>

using namespace std;

class MapGenerator {
	public:
		MapGenerator();
		void generateMap();
		int generateRandom(double in, double out);

	private:
		std::string _myMap[SIZE_I][SIZE_J];	
};

#endif 