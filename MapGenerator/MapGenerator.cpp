#include "MapGenerator.h"

int MapGenerator::generateRandom(double in, double out){
	std::random_device rd;
    std::mt19937 mt(rd());
	std::uniform_real_distribution<double> dist(in, out);
	return (int) dist(mt);
}

std::string[][] MapGenerator::generateMap(){

	std::string myMap[maxH][maxV];
	int in_turret = 0;
	int in_respawn = 0;
	int numNucleo = 0;

	for (size_t i = 0; i < maxH; ++i){
    	for (size_t j = 0; j < maxV; ++j){
    			bool in = true;
        		if (i == (maxH/2) && j == (maxV/2)){
        			myMap[i][j] = "N";
        			numNucleo = ((( i*10 )) +j);
					in = false;
				}
				if ( (i == 0 || j == 0 || j == (maxV - 1) || i == (maxH - 1)) && in ){
					myMap[i][j] = "P";
					in = false;
				}
				if(in){
					myMap[i][j] = "0";
				}
		}
	}

	bool generarCam = true;
	int unidad = numNucleo % 10;
	int decena = (numNucleo / 10) % 10;

	while(generarCam){
		for (int i = 0; i < (maxH/2 - 1) ; ++i){

			std::cout << unidad << " " << decena << std::endl;
			decena = decena -1 ;
			std::cout << unidad << " " << decena << std::endl;

			myMap[unidad][decena] = "C";

			if(myMap[unidad][decena-1] == "P"){
				generarCam = false;
				myMap[unidad][decena] = "R";
			}

		}
	}

	for (size_t i = 0; i < maxH; ++i){
    	for (size_t j = 0; j < maxV; ++j){
    		if(myMap[i][j] == "0"){
    			myMap[i][j] = "T";
    		}
		}
	}

	for (size_t p = 0; p < maxH; ++p){
    	for (size_t l = 0; l < maxV; ++l){
        	std::cout << myMap[p][l];
		}
		std::cout << std::endl;
	}
	return myMap;
}
