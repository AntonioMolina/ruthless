#ifndef Cell_H
#define Cell_H

#include "Constants.h"

class Cell {
 public:
  Cell () {}
  Cell (int row, int column, GameEnums::TypeCell type = GameEnums::TypeCell::FreeCell);
  Cell (const Cell& other);
  ~Cell () {}

  /* Priority Inverse */
  bool operator>(const Cell& otro) const {
    return _distance > otro.getDistance();
  }

  int getRow () const;
  int getColumn () const;
  GameEnums::TypeCell getType () const;
  int getDistance () const;

  void setRow (int row);
  void setColumn (int column);
  void setType (GameEnums::TypeCell type);
  void setDistance (int distance);

 private:
  int _row;
  int _column;
  GameEnums::TypeCell _type;

  /* A* Variables */
  int _distance;
  int _cost;

};

#endif /* Cell_H */
