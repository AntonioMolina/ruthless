#ifndef AStar_H
#define AStar_H

#include <Ogre.h>

#include "Cell.h"
#include "Constants.h"

class AStar {
 public:
  AStar (Cell** mapMatrix, int rows, int columns, Ogre::Vector2 core,
    std::vector<Ogre::Vector2> randomPoints, std::vector<Ogre::Vector2> spawns);
  ~AStar ();

  Cell** createPath ();

 private:
  Cell** _mapMatrix;
  int _rows;
  int _columns;
  Ogre::Vector2 _core;
  std::vector<Ogre::Vector2> _randomPoints;
  std::vector<Ogre::Vector2> _spawns;

  double estimateCost(Cell cell_1, Cell cell_2);

  std::vector<Cell> getAdjacents (Cell** mapMatrix, int row, int column);
};

#endif /* ASTAR_H */
