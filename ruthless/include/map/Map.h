#ifndef Map_H
#define Map_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>

#include "Cell.h"
#include "Constants.h"

class Map
{
 public:
  Map () {}
  Map (Cell** mapMatrix, int rows, int columns,
       TypesDef::GraphPtr graph, Ogre::Vector2 core,
       std::vector<Ogre::Vector2> points, std::vector<Ogre::Vector2> spawns);
  Map(Ogre::SceneNode* sNode);
  ~Map ();

  /* Getters */
  Ogre::SceneNode* getSceneNode () const { return _nodeMap; }
  Ogre::Entity* getEntity () const { return _entMap; }

  OgreBulletCollisions::CollisionShape* getCollisionShape () const { return _shapeMap; }
  OgreBulletDynamics::RigidBody* getRigidBody () const { return _rigBodyMap; }

  Cell** getMapMatrix () const { return _mapMatrix; }
  int getRows () const { return _rows; }
  int getColumns () const { return _columns; }

  Ogre::Vector3 getCorePosition();
  Ogre::Vector3 getSpawnPosition(Ogre::Vector2 vect);


  TypesDef::GraphPtr getMapGraph () const { return _mapGraph; }
  Ogre::Vector2 getCore () const { return _core; }
  std::vector<Ogre::Vector2> getPoints () const { return _points; }
  std::vector<Ogre::Vector2> getSpawns () const { return _spawns; }

  /* Setters */
  void setSceneNode (Ogre::SceneNode* node) { _nodeMap = node; }
  void setEntity (Ogre::Entity* ent) { _entMap = ent; }

  void setCollisionShape (OgreBulletCollisions::CollisionShape* shape) { _shapeMap = shape; }
  void setRigidBody (OgreBulletDynamics::RigidBody* body) { _rigBodyMap = body; }

  void setMapMatrix (Cell** mapMatrix) { _mapMatrix = mapMatrix; }
  void setMapGraph (TypesDef::GraphPtr graph) { _mapGraph = graph; }
  void setCore (Ogre::Vector2 core) { _core = core; }
  void setPoints (std::vector<Ogre::Vector2> points) { _points = points; }
  void setSpawns (std::vector<Ogre::Vector2> spawns) { _spawns = spawns; }

 private:
  Ogre::Entity* _entMap;
  Ogre::SceneNode* _nodeMap;

  OgreBulletCollisions::CollisionShape* _shapeMap;
  OgreBulletDynamics::RigidBody* _rigBodyMap;

  Cell** _mapMatrix;
  int _rows;
  int _columns;
  TypesDef::GraphPtr _mapGraph;
  /* A spawn is a 2d point in matrix. */
  Ogre::Vector2 _core;
  std::vector<Ogre::Vector2> _points;
  std::vector<Ogre::Vector2> _spawns;
};

#endif
