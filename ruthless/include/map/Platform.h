#ifndef Platform_H
#define Platform_H

#include <Ogre.h>

#include "Constants.h"
#include "Graph.h"
#include "PhysicsManager.h"

class Platform {
 public:
  Platform (TypesDef::MapPtr map, int row, int column) :
            _row(row), _column(column)
  {
    Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
  	    getSceneManager("SceneManager");

    Ogre::SceneNode* nodeMap = map->getSceneNode();
    TypesDef::GraphPtr graph = map->getMapGraph();
    int columns = map->getColumns();

    std::stringstream entName;
    entName.str("");

    entName << "platform_" << row << "_" << column;
    _entPlatform = sceneMgr->createEntity(entName.str(), "plataforma.mesh");
    _entPlatform->setQueryFlags(Masks::PLATFORM_MASK);
    _nodePlatform = sceneMgr->createSceneNode(entName.str());
    _nodePlatform->attachObject(_entPlatform);
    nodeMap->addChild(_nodePlatform);

    Ogre::Vector3 plat_Position = graph->getVertex(row * columns + column)->getData().getPosition();
    _nodePlatform->setPosition(plat_Position);
  }

  ~Platform ()
  {
  }

  Ogre::Entity* getEntity () const { return _entPlatform; }
  Ogre::SceneNode* getSceneNode () const { return _nodePlatform; }

  int getRow () const { return _row; }
  int getColumn () const { return _column; }

 private:
  Ogre::Entity* _entPlatform;
  Ogre::SceneNode* _nodePlatform;

  int _row;
  int _column;
};

#endif /* Platform_H */
