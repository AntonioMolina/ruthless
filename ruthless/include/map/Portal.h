#ifndef Portal_H
#define Portal_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "Constants.h"
#include "Graph.h"
#include "PhysicsManager.h"

class Portal {
 public:
  Portal (TypesDef::MapPtr map, int i, int j, int nPortal)
  {
    Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
  	    getSceneManager("SceneManager");

    Ogre::SceneNode* nodeMap = map->getSceneNode();
    TypesDef::GraphPtr graph = map->getMapGraph();
    int columns = map->getColumns();
    int rows = map->getRows();

    std::stringstream entName;
    entName.str("");
    entName << "portal_" << nPortal;

    _entPortal = sceneMgr->createEntity(entName.str(), "portal.mesh");
    _nodePortal = sceneMgr->createSceneNode(entName.str());
    _nodePortal->attachObject(_entPortal);
    nodeMap->addChild(_nodePortal);

    Ogre::Vector3 portal_Position = graph->getVertex(i * columns + j)->getData().getPosition();
    Ogre::Vector3 body_Position;
    if (i == (rows - 1))
    {
      _nodePortal->setPosition(portal_Position.x, portal_Position.y, portal_Position.z + (GameConstants::sidePlatform / 2));
      body_Position = Ogre::Vector3(portal_Position.x, portal_Position.y, portal_Position.z + (GameConstants::sidePlatform / 2));
      _nodePortal->yaw(Ogre::Degree(-90));
    } else if (j == 0)
    {
      _nodePortal->setPosition(portal_Position.x - (GameConstants::sidePlatform / 2), portal_Position.y, portal_Position.z);
      body_Position = Ogre::Vector3(portal_Position.x - (GameConstants::sidePlatform / 2), portal_Position.y, portal_Position.z);
      _nodePortal->yaw(Ogre::Degree(180));
    } else if (j == (columns - 1))
    {
      _nodePortal->setPosition(portal_Position.x + (GameConstants::sidePlatform / 2), portal_Position.y, portal_Position.z);
      body_Position = Ogre::Vector3(portal_Position.x + (GameConstants::sidePlatform / 2), portal_Position.y, portal_Position.z);
    }

    OgreBulletCollisions::StaticMeshToShapeConverter* trimeshPortal = new
        OgreBulletCollisions::StaticMeshToShapeConverter(_entPortal);
    _shapePortal = trimeshPortal->createTrimesh();

    _rigBodyPortal = new OgreBulletDynamics::RigidBody(entName.str(), PhysicsManager::getSingletonPtr()->getWorld());
    _rigBodyPortal->setStaticShape(_shapePortal, 0.1, 0.8, body_Position, _nodePortal->getOrientation());
  }

  ~Portal ()
  {
  }

  Ogre::Entity* getEntity () const { return _entPortal; }
  Ogre::SceneNode* getSceneNode () const { return _nodePortal; }

  OgreBulletCollisions::CollisionShape* getCollisionShape () const { return _shapePortal; }
  OgreBulletDynamics::RigidBody* getRigidBody () const { return _rigBodyPortal; }

 private:
  Ogre::Entity* _entPortal;
  Ogre::SceneNode* _nodePortal;

  OgreBulletCollisions::CollisionShape* _shapePortal;
  OgreBulletDynamics::RigidBody* _rigBodyPortal;
};

#endif /* Portal_H */
