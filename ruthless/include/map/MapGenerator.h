#ifndef MapGenerator_H
#define MapGenerator_H

#include <Ogre.h>

#include "Constants.h"
#include "Graph.h"
#include "Map.h"

class MapGenerator : public Ogre::Singleton<MapGenerator>
{
 public:
  MapGenerator () {}
  ~MapGenerator () {}

  TypesDef::MapPtr generateMap ();

  static MapGenerator& getSingleton ();
  static MapGenerator* getSingletonPtr ();
};

#endif /* MapGenerator_H */
