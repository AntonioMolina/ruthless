#ifndef Wall_H
#define Wall_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "Constants.h"
#include "Graph.h"
#include "PhysicsManager.h"

class Wall {
 public:
  Wall (TypesDef::MapPtr map, int i, int j, int nWall, int inRow)
  {
    Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
  	    getSceneManager("SceneManager");

    Ogre::SceneNode* nodeMap = map->getSceneNode();
    TypesDef::GraphPtr graph = map->getMapGraph();
    int columns = map->getColumns();
    int rows = map->getRows();

    std::stringstream entName;
    entName.str("");
    entName << "entWall_" << nWall;

    _entWall = sceneMgr->createEntity(entName.str(), "Barrera.mesh");
    _nodeWall = sceneMgr->createSceneNode(entName.str());
    _nodeWall->attachObject(_entWall);
    nodeMap->addChild(_nodeWall);

    Ogre::Vector3 wall_Position = graph->getVertex(i * columns + j)->getData().getPosition();
    Ogre::Vector3 body_Position;
    if (inRow == 0)
    {
      body_Position = Ogre::Vector3(wall_Position.x, wall_Position.y, wall_Position.z);
      if (i == 0)
      {
        body_Position.z = wall_Position.z - (GameConstants::sidePlatform / 2);
        wall_Position.z = wall_Position.z - (GameConstants::sidePlatform / 2);
        _nodeWall->setPosition(wall_Position);
      } else if (i == (rows - 1))
      {
        body_Position.z = wall_Position.z + (GameConstants::sidePlatform / 2);
        wall_Position.z = wall_Position.z + (GameConstants::sidePlatform / 2);
        _nodeWall->setPosition(wall_Position);
      }
      if ((j % 2) == 0)
      {
        _nodeWall->yaw(Ogre::Degree(180));
      }
    } else
    {
      if (j == 0)
      {
        body_Position = Ogre::Vector3(wall_Position.x - (GameConstants::sidePlatform / 2), wall_Position.y, wall_Position.z);
        _nodeWall->setPosition(wall_Position.x - (GameConstants::sidePlatform / 2), wall_Position.y, wall_Position.z);
        _nodeWall->yaw(Ogre::Degree(90));
      } else if (j == (columns - 1))
      {
        body_Position = Ogre::Vector3(wall_Position.x + (GameConstants::sidePlatform / 2), wall_Position.y, wall_Position.z);
        _nodeWall->setPosition(wall_Position.x + (GameConstants::sidePlatform / 2), wall_Position.y, wall_Position.z);
        _nodeWall->yaw(Ogre::Degree(90));
      }
      if ((i % 2) == 0)
      {
        _nodeWall->yaw(Ogre::Degree(180));
      }
    }

    OgreBulletCollisions::StaticMeshToShapeConverter* trimeshWall = new
        OgreBulletCollisions::StaticMeshToShapeConverter(_entWall);
    _shapeWall = trimeshWall->createTrimesh();

    _rigBodyWall = new OgreBulletDynamics::RigidBody(entName.str(), PhysicsManager::getSingletonPtr()->getWorld());
    _rigBodyWall->setStaticShape(_shapeWall, 0.1, 0.8, body_Position, _nodeWall->getOrientation());
  }

  ~Wall ()
  {
  }

  Ogre::Entity* getEntity () const { return _entWall; }
  Ogre::SceneNode* getSceneNode () const { return _nodeWall; }

  OgreBulletCollisions::CollisionShape* getCollisionShape () const { return _shapeWall; }
  OgreBulletDynamics::RigidBody* getRigidBody () const { return _rigBodyWall; }

 private:
  Ogre::Entity* _entWall;
  Ogre::SceneNode* _nodeWall;

  OgreBulletCollisions::CollisionShape* _shapeWall;
  OgreBulletDynamics::RigidBody* _rigBodyWall;
};

#endif /* Wall_H */
