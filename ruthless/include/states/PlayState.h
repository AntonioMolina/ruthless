/*********************************************************************
 * Módulo 1. Curso de Experto en Desarrollo de Videojuegos
 * Autor: David Vallejo Fernández    David.Vallejo@uclm.es
 *
 * Código modificado a partir de Managing Game States with OGRE
 * http://www.ogre3d.org/tikiwiki/Managing+Game+States+with+OGRE
 * Inspirado en Managing Game States in C++
 * http://gamedevgeek.com/tutorials/managing-game-states-in-c/
 *
 * You can redistribute and/or modify this file under the terms of the
 * GNU General Public License ad published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * and later version. See <http://www.gnu.org/licenses/>.
 *
 * This file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *********************************************************************/

#ifndef PlayState_H
#define PlayState_H

#include <memory>

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>

#include <iostream>
#include <sstream>
#include <string>

#include <OgreBulletDynamicsRigidBody.h>

#include "GameState.h"

#include "PauseState.h"
#include "IntroState.h"
#include "FinalState.h"

#include "AmmoDrop.h"
#include "Core.h"
#include "Drop.h"
#include "Enemy.h"
#include "HealthRestoreDrop.h"
#include "Map.h"
#include "Platform.h"
#include "Player.h"
#include "Portal.h"
#include "Turret.h"
#include "Wall.h"
#include "Weapon.h"

#include "DropFactory.h"
#include "EnemyFactory.h"

#include "Constants.h"

#include "SoundFXManager.h"
#include "TrackManager.h"

class PlayState : public Ogre::Singleton<PlayState>, public GameState
{
 public:
  PlayState () {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static PlayState& getSingleton ();
  static PlayState* getSingletonPtr ();

  std::vector<TypesDef::EnemyPtr> getEnemies () { return _enemies; }
  unsigned int getRound () { return _round; }

  void addEnemyDie (Ogre::Vector3 posEnemy) {
    ++_enemiesDied;
    int probDrop = rand () % (101);
    if (probDrop < GameConstants::chanceTotalDrop)
    {
      probDrop = rand () % (101);
      TypesDef::DropPtr drop;
      if (probDrop < GameConstants::chanceAmmoDrop)
      {
        drop = DropFactory::getSingletonPtr()->createDrop(GameEnums::TypesDrop::Drop_Ammo, posEnemy, _nDrop);
      } else
      {
        drop = DropFactory::getSingletonPtr()->createDrop(GameEnums::TypesDrop::Drop_Health, posEnemy, _nDrop);
      }
      if (drop != nullptr)
      {
        _drops.push_back(drop);
      }
      ++_nDrop;
    }
  }

  TypesDef::MapPtr getMap () { return _map; }
  TypesDef::PlayerPtr getPlayer () { return _player; }

  unsigned int getNDrop () { return _nDrop; }

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

 private:
  Ogre::Real _deltaT;
  CEGUI::Window* _sheet;

  bool _exitGame;

  /* Game Variables */
  GameEnums::StateGame _currentState;

  unsigned int _round;
  unsigned int _totalMonstersKilled;

  Ogre::SceneNode* _mainCameraNode;
  bool _rotateCamToLeft;
  bool _rotateCamToRight;

  GameEnums::TypeTurret _typeSelectedTurret;
  TypesDef::TurretPtr _selectedTurret;

  Ogre::Real _timeToNextRound;

  /* Wave State variables */
  Ogre::Real _timeToStartRound;
  Ogre::Real _timeLastSpawn;
  bool _roundStarted;

  int _enemiesToSpawn;
  int _enemiesSpawned;
  int _enemiesDied;

  OgreBulletDynamics::DynamicsWorld* _world;

  TypesDef::MapPtr _map;

  std::vector<Platform> _platforms;
  std::vector<Wall> _walls;
  std::vector<Portal> _portals;

  TrackManager* _pTrackManager;
  SoundFXManager* _pSoundFXManager;

  TrackPtr _pTrackGame;

  SoundFXPtr _effectShoot;

  TypesDef::CorePtr _core;
  TypesDef::PlayerPtr _player;
  std::vector<TypesDef::TurretPtr> _turrets;
  std::vector<TypesDef::EnemyPtr> _enemies;
  std::vector<TypesDef::DropPtr> _drops;

  unsigned int _nDrop;

  Ogre::OverlayManager* _overlayManager;

  Ogre::RaySceneQuery* _raySceneQuery;

  inline void updateTaticalState ();
  inline void updateWaveState ();

  void changeToTacticalState ();
  void changeToWaveState ();

  void createGUITacticalState ();
  void destroyGUITacticalState ();
  inline void showGUITacticalState ();
  inline void hideGUITacticalState ();
  inline void updateGUITacticalState ();

  void createGUIWaveState ();
  void destroyGUIWaveState ();
  inline void showGUIWaveState ();
  inline void hideGUIWaveState ();
  inline void updateGUIWaveState ();

  Ogre::Ray setRayQuery (int posx, int posy, uint32_t mask);

  void hitCore (unsigned int dmg);
  inline bool isAllEnemiesDead ();

  void createScene ();
  void createMap ();

  inline void createEnemy ();

  void updateGameGui(std::string nametext , int data);
  void updateTacticalGui(std::string nametext , int data);
  void updateTime(std::string nametext , Ogre::Real data);
  bool machineButtonC(const CEGUI::EventArgs &e);
  bool cannonButtonC(const CEGUI::EventArgs &e);

};

#endif
