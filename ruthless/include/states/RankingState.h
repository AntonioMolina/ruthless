#ifndef RankingState_H
#define RankingState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

using namespace Ogre;

class RankingState : public Singleton<RankingState>, public GameState
{
    public:
        RankingState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

        static RankingState& getSingleton ();
        static RankingState* getSingletonPtr ();

    protected:
        CEGUI::Window* _sheet;

        Root* _root;
        SceneManager* _sceneMgr;
        Viewport* _viewport;
        Camera* _camera;

        bool _back;

    private:
        void createGUI ();
        void deleteGUI ();
        void createBackground ();
        void deleteBackground ();

        bool back (const CEGUI::EventArgs &e);
};

#endif /* RankingState_H */
