#ifndef MenuState_H
#define MenuState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

#include "SoundFXManager.h"
#include "TrackManager.h"

class MenuState : public Ogre::Singleton<MenuState>,
                  public GameState {
 public:
  MenuState ();

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  static MenuState& getSingleton ();
  static MenuState* getSingletonPtr ();

 protected:
  CEGUI::Window* _sheet;

  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  bool _exit;

 private:
  void createBackground ();
  void deleteBackground ();

  void createGUI ();
  void deleteGUI ();

  bool start (const CEGUI::EventArgs &e);
  bool quit (const CEGUI::EventArgs &e);
  bool credits (const CEGUI::EventArgs &e);
  bool ranking (const CEGUI::EventArgs &e);
  bool controls (const CEGUI::EventArgs &e);

  TrackManager* _pTrackManager;
  SoundFXManager* _pSoundFXManager;

  TrackPtr _pTrackMenu;
};

#endif /* MenuState_H */
