#ifndef FinalState_H
#define FinalState_H

#include <Ogre.h>
#include <OIS/OIS.h>

#include <CEGUI.h>

#include "GameState.h"
#include "IntroState.h"
#include "PlayState.h"
#include "RankingPosition.h"
#include "RankingManager.h"
#include "MenuState.h"

#include <iostream>
#include <sstream>
#include <string>


class FinalState : public Ogre::Singleton<FinalState>, public GameState
{
 public:
  FinalState() {}

  void enter ();
  void exit ();
  void pause ();
  void resume ();

  void keyPressed (const OIS::KeyEvent &e);
  void keyReleased (const OIS::KeyEvent &e);

  void mouseMoved (const OIS::MouseEvent &e);
  void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
  void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

  bool frameStarted (const Ogre::FrameEvent& evt);
  bool frameEnded (const Ogre::FrameEvent& evt);

  // Heredados de Ogre::Singleton.
  static FinalState& getSingleton ();
  static FinalState* getSingletonPtr ();

  bool _back;
  bool _mainmenu;

  unsigned int _points;
  unsigned int _round;


  void updateInfo(unsigned int points, unsigned int round);


  //GUI
  void createGUIFinalState();
  void destroyGUI();
  void hideGUI();
  bool exitB(const CEGUI::EventArgs &e);
  bool mainB(const CEGUI::EventArgs &e);

  CEGUI::Window* _sheet;
  CEGUI::Window* finalGui;

  RankingPosition* _rankPos;

 protected:
  Ogre::Root* _root;
  Ogre::SceneManager* _sceneMgr;
  Ogre::Viewport* _viewport;
  Ogre::Camera* _camera;

  bool _exitGame;
};

#endif
