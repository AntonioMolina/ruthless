#ifndef CreditsState_H
#define CreditsState_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>
#include <RendererModules/Ogre/Renderer.h>

#include "GameState.h"

using namespace Ogre;

class CreditsState : public Singleton<CreditsState>, public GameState {
    public:
        CreditsState ();

        void enter ();
        void exit ();
        void pause ();
        void resume ();

        void keyPressed (const OIS::KeyEvent &e);
        void keyReleased (const OIS::KeyEvent &e);

        void mouseMoved (const OIS::MouseEvent &e);
        void mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id);
        void mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id);

        bool frameStarted (const FrameEvent& evt);
        bool frameEnded (const FrameEvent& evt);

        static CreditsState& getSingleton ();
        static CreditsState* getSingletonPtr ();

    protected:
        CEGUI::Window* _sheet;

        Root* _root;
        SceneManager* _sceneMgr;
        Viewport* _viewport;
        Camera* _camera;

        bool _back;

    private:
        void createGUI ();
        void deleteGUI ();
        void createBackground ();
        void deleteBackground ();

        bool back (const CEGUI::EventArgs &e);
};

#endif /* CreditsState_H */
