#ifndef Rifle_H
#define Rifle_H

#include "Weapon.h"

class Rifle : public Weapon {
 public:
  Rifle ();
  ~Rifle ();

  void update (Ogre::Real deltaT) override;

  void addReserveAmmo (unsigned int ammo) override;

  void reload () override;
  void shoot (std::vector<TypesDef::EnemyPtr> enemies) override;
};

#endif /* Rifle_H */
