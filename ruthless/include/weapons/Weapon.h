#ifndef Weapon_H
#define Weapon_H

#include <CEGUI.h>
#include <Ogre.h>

#include <OgreBulletCollisionsRay.h>
#include <OgreBulletDynamicsRigidBody.h>

#include "Constants.h"
#include "PhysicsManager.h"

class Weapon {
 public:
  Weapon (unsigned int dmg, unsigned int ammoMagazine,
          unsigned int ammoReserve, Ogre::Real timeBetweenShoots) :
              _dmg(dmg),
              _ammoMagazine(ammoMagazine),
              _ammoReserve(ammoReserve),
              _timeLastShoot(timeBetweenShoots),
              _isReloading(false),
              _timeReloading(0.0f) {};

  ~Weapon () {
  }

  virtual void update (Ogre::Real deltaT) = 0;

  unsigned int getDmg () const { return _dmg; };
  unsigned int getAmmoMagazine () const { return _ammoMagazine; };
  unsigned int getAmmoReserve () const { return _ammoReserve; };

  bool getIsReloading () const { return _isReloading; };
  Ogre::Real getTimeReloading () const { return _timeReloading; };

  Ogre::SceneNode* getWeaponNode () const { return _weaponNode; };
  Ogre::Entity* getWeaponEnt () const { return _weaponEnt; };

  void swap () {
    /* If is active weapon */
    if (_weaponEnt->isVisible()) {
      if (_isReloading) {
        _timeLastShoot = 99.9f; /* 99.9 seconds */

        _isReloading = false;
      }
    };

    _weaponEnt->setVisible(!_weaponEnt->isVisible());
  };

  void setVisibility (bool visible) {
    _weaponEnt->setVisible(visible);
  };

  virtual void addReserveAmmo (unsigned int ammo) = 0;

  virtual void reload () = 0;
  virtual void shoot (std::vector<TypesDef::EnemyPtr> enemies) = 0;

  OgreBulletDynamics::RigidBody* hitEnemy ()
  {
    Ogre::Vector3 collisionPoint;

    Ogre::Root* root = Ogre::Root::getSingletonPtr();
    Ogre::SceneManager* sceneMgr = root->getSceneManager("SceneManager");
    Ogre::Camera* cameraPlayer = sceneMgr->getCamera("PlayerCamera");

    float posx = 0.5f;
    float posy = 0.5f;

    OgreBulletDynamics::DynamicsWorld* world = PhysicsManager::getSingletonPtr()->getWorld();

    Ogre::Ray r = cameraPlayer->getCameraToViewportRay (posx, posy);
    OgreBulletCollisions::CollisionClosestRayResultCallback cQuery =
        OgreBulletCollisions::CollisionClosestRayResultCallback(r, world, 1000.0f);
    world->launchRay(cQuery);

    if (cQuery.doesCollide())
    {
      OgreBulletDynamics::RigidBody* body = (OgreBulletDynamics::RigidBody*) (cQuery.getCollidedObject());
      collisionPoint = cQuery.getCollisionPoint();
      return body;
    }

    return nullptr;
  }

 protected:
  unsigned int _dmg;

  /* Variables for ammo */
  unsigned int _ammoMagazine;
  unsigned int _ammoReserve;

  Ogre::Real _timeLastShoot;

  bool _isReloading;
  Ogre::Real _timeReloading;

  bool _isPosibleShoot;

  Ogre::SceneNode* _weaponNode;
  Ogre::Entity* _weaponEnt;

  /* ray query for shoot */
  Ogre::RaySceneQuery* _raySceneQuery;
};

#endif /* Weapon_H */
