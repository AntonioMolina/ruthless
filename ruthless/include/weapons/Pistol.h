#ifndef Pistol_H
#define Pistol_H

#include <Ogre.h>

#include "Weapon.h"

class Pistol : public Weapon {
 public:
  Pistol ();
  ~Pistol ();

  void update (Ogre::Real deltaT) override;

  void addReserveAmmo (unsigned int ammo) override;

  void reload () override;
  void shoot (std::vector<TypesDef::EnemyPtr> enemies) override;
  void stopShooting () { _shot = false; }

 private:
  bool _shot;
};

#endif /* Pistol_H */
