#ifndef PhysicsManager_H
#define PhysicsManager_H

#include <Ogre.h>

#include <OgreBulletDynamicsWorld.h>

class PhysicsManager : public Ogre::Singleton<PhysicsManager>
{
 public:
  PhysicsManager () {
    _gravity = Ogre::Vector3(0, -9.8, 0);
    _worldBounds = Ogre::AxisAlignedBox (Ogre::Vector3 (-1000, -1000, -1000),
                                         Ogre::Vector3 (1000,  1000,  1000));
    _world = nullptr;
  }

  ~PhysicsManager () {
    if (_world != nullptr)
    {
      delete _world;
    }
  }

  OgreBulletDynamics::DynamicsWorld* getWorld () { return _world; }

  OgreBulletDynamics::DynamicsWorld* createNewWorld ();

  static PhysicsManager& getSingleton();
  static PhysicsManager* getSingletonPtr();

 private:
  Ogre::Vector3 _gravity;
  Ogre::AxisAlignedBox _worldBounds;

  OgreBulletDynamics::DynamicsWorld* _world;
  #ifdef _DEBUG
  OgreBulletCollisions::DebugDrawer* _debugDrawer;
  #endif
};

#endif /* PhysicsManager_H */
