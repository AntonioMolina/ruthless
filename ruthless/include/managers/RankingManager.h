#ifndef RankingManager_H
#define RankingManager_H

#include "RankingPosition.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include <Ogre.h>

using namespace Ogre;
using namespace std;

class RankingManager : public Singleton<RankingManager> {
    public:
        RankingManager ();

        bool saveResult (RankingPosition *puntuation);
        void createRankingFile ();
        std::vector<RankingPosition> createStringRanking ();
        string createStringRankingAnterior ();

        static RankingManager& getSingleton ();
        static RankingManager* getSingletonPtr ();

        std::vector<std::string> split (std::string str, char delimiter);
};

#endif /* RankingManager_H */
