#ifndef AnimationManager_H
#define AnimationManager_H

#include <Ogre.h>

#include <fstream>
#include <iostream>
#include <sstream>

#include "Constants.h"

#include "Enemy.h"

class AnimationManager : public Ogre::Singleton<AnimationManager>
{
 public:
   
  AnimationManager ();
  ~AnimationManager ();
  static AnimationManager& getSingleton();
  static AnimationManager* getSingletonPtr();

private:

  void launchanimation(Ogre::SceneManager* _sceneMgr, std::string nameAnim , std::string nameEnt);
  void stopanimation(Ogre::SceneManager* _sceneMgr, std::string nameAnim , std::string nameEnt);

};
#endif /* Enemy_Factory_H */
