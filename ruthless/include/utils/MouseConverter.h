#ifndef MouseConverter_H
#define MouseConverter_H

#include <CEGUI.h>
#include <Ogre.h>
#include <OIS/OIS.h>

class MouseConverter : public Ogre::Singleton<MouseConverter> {
 public:
  MouseConverter () {}
  ~MouseConverter () {}

  CEGUI::MouseButton convertMouseButton (OIS::MouseButtonID id);

  static MouseConverter& getSingleton ();
  static MouseConverter* getSingletonPtr ();
};

#endif /* MouseConverter_H */
