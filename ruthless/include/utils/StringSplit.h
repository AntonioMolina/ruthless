#ifndef StringSplit_H
#define StringSplit_H

#include <Ogre.h>

class StringSplit : public Ogre::Singleton<StringSplit> {
 public:
  StringSplit () {}
  ~StringSplit () {}

  std::vector<std::string> split (std::string str, char delimiter)
  {
    std::vector<std::string> internal;
    std::stringstream ss(str);
    std::string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
  }

  static StringSplit& getSingleton ()
  {
    assert(msSingleton);
    return *msSingleton;
  }

  static StringSplit* getSingletonPtr ()
  {
    return msSingleton;
  }
};

#endif /* StringSplit_H */
