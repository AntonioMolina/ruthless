#ifndef Constants_H
#define Constants_H

#include <Ogre.h>

class Player;
class Core;

class Enemy;
class Enemy_Type_1;
class Enemy_Type_2;

class Drop;
class HealthRestoreDrop;
class AmmoDrop;

class Pistol;
class Rifle;
class Weapon;

class CannonTurret;
class MachineGunTurret;
class Turret;

class Map;

class Graph;
class GraphEdge;
class GraphVertex;

namespace GameConstants {
  /* Player Constants */
  const unsigned int numWeapons = 2;
  const unsigned int numMainWeapon = 0;
  const unsigned int numSecondaryWeapon = 1;
  const unsigned int swapingWeapons = 2;
  const Ogre::Real timeBetweenChangeWeapons = 0.0f;
  const Ogre::Real playerSpeed = 3.5f;
  const unsigned int initialCoins = 200;

  /* Core Constants */
  const int maxLifeCore = 150;

  /* Pistol Constants */
  const unsigned int dmgPistol = 10;
  const unsigned int maxAmmoPistol = 19;
  const unsigned int maxAmmoReservePistol = 999;
  const Ogre::Real timeBetweenShootsPistol = 0.4f;
  const Ogre::Real timeReloadPistol = 1.5f;

  /* Rifle Constants */
  const unsigned int dmgRifle = 30;
  const unsigned int initialAmmoReserveRifle = 150;
  const unsigned int maxAmmoRifle = 30;
  const unsigned int maxAmmoReserveRifle = 500;
  const Ogre::Real timeBetweenShootsRifle = 0.08f;
  const Ogre::Real timeReloadRifle = 2.5f;

  /* Drop Constants */
  const Ogre::Real chanceTotalDrop = 15.0f; /* 15% drop item */
  const Ogre::Real chanceAmmoDrop = 90.0f;
  const unsigned int magazinesToRecover = 2;
  const Ogre::Real chanceHealthCoreDrop = 10.0f;
  const unsigned int healthToRecover = 20;

  const unsigned int minAmmoDrop = 30;
  const unsigned int maxAmmoDrop = 60;
  const unsigned int minHealtRestoreCore = 10;
  const unsigned int maxHealthRestoreCore = 30;

  /* Enemy Constants */
  const Ogre::Real enemySpeed = 2.0f;

  /* Enemy_Type_1 Constants */
  const unsigned int dmgEnemy_1 = 5;
  const unsigned int healthEnemy_1 = 100;
  const unsigned int increasedHealthRoundEnemy_1 = 40;
  const unsigned int pointsEnemy_1 = 15;

  /* Enemy_Type_2 Constants */
  const unsigned int dmgEnemy_2 = 15;
  const unsigned int healthEnemy_2 = 75;
  const unsigned int increasedHealthRoundEnemy_2 = 35;
  const unsigned int pointsEnemy_2 = 10;

  /* MachineGunTurret Constants */
  const unsigned int dmgMachineGunTurret = 5;
  const Ogre::Real rateOfFire_MachineGunTurret = 0.2f;
  const Ogre::Real rangeMachineGunTurret = 7.0f;
  const Ogre::Real squareRangeMachineGunTurret = 49.0f;
  const unsigned int costMachineGunTurret = 100;

  /* CannonTurret Constants */
  const unsigned int dmgCannonTurret = 60;
  const Ogre::Real rateOfFire_CannonTurret = 1.5f;
  const Ogre::Real rangeCannonTurret = 5.0f;
  const Ogre::Real squareRangeCannonTurret = 25.0f;
  const unsigned int costCannonTurret = 150;

  /* MapGenerator Constants */
  const unsigned int minRows = 15;
  const unsigned int maxRows = 25;
  const unsigned int minColumns = 15;
  const unsigned int maxColumns = 25;
  const unsigned int minNumSpawns = 1;
  const unsigned int maxNumSpawns = 3;

  const int sidePlatform = 2;

  /* Game Constants */
  const Ogre::Real maxTimeBetweenRounds = 30.0f;
  const Ogre::Real timeToStartRound = 5.0f;

  /* Spawn Enemies Constants */
  const Ogre::Real timeBetweenSpawnMonsters = 1.75f;

  /* Camera rotation constants */
  const Ogre::Real rotationSpeedCamera = 2.0f;
}

/* Masks for queries */
namespace Masks {
  const uint32_t ENEMY_MASK = 1 << 0;
  const uint32_t PLATFORM_MASK = 1 << 1;
}

namespace GameEnums {
  enum TypeCell : unsigned int {
    CoreCell = 0,
    FreeCell,
    PathCell,
    PointCell,
    SpawnCell,
  };

  enum TypeWeapon : unsigned int {
    Pistol = 0,
    Rifle,
  };

  enum TypeTurret : unsigned int {
    NoneType = 0,
    CannonTurret,
    MachineGunTurret,
  };

  enum TypeEnemy : unsigned int {
    Enemy_Type_1 = 0,
    Enemy_Type_2,
  };

  enum TypesDrop : unsigned int {
    Drop_Ammo = 0,
    Drop_Health,
  };

  enum StateGame : unsigned int {
    TacticalState = 0,
    WaveState,
  };
}

namespace TypesDef {
  typedef std::shared_ptr<Player> PlayerPtr;
  typedef std::shared_ptr<Core> CorePtr;

  typedef std::shared_ptr<Enemy> EnemyPtr;
  typedef std::shared_ptr<Enemy_Type_1> Enemy_Type_1Ptr;
  typedef std::shared_ptr<Enemy_Type_2> Enemy_Type_2Ptr;

  typedef std::shared_ptr<Drop> DropPtr;
  typedef std::shared_ptr<HealthRestoreDrop> HealthRestoreDropPtr;
  typedef std::shared_ptr<AmmoDrop> AmmoDropPtr;

  typedef std::shared_ptr<Pistol> PistolPtr;
  typedef std::shared_ptr<Rifle> RiflePtr;
  typedef std::shared_ptr<Weapon> WeaponPtr;

  typedef std::shared_ptr<Turret> TurretPtr;
  typedef std::shared_ptr<MachineGunTurret> MachineGunTurretPtr;
  typedef std::shared_ptr<CannonTurret> CannonTurretPtr;

  typedef std::shared_ptr<Map> MapPtr;

  typedef std::shared_ptr<Graph> GraphPtr;
  typedef std::shared_ptr<GraphVertex> GraphVertexPtr;
  typedef std::shared_ptr<GraphEdge> GraphEdgePtr;
}

#endif /* Constants_H */
