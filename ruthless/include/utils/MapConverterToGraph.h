#ifndef MapConverterToGraph_H
#define MapConverterToGraph_H

#include <Ogre.h>

#include "Cell.h"
#include "Constants.h"
#include "Map.h"

class MapConverterToGraph : public Ogre::Singleton<MapConverterToGraph> {
 public:
  MapConverterToGraph () {}
  ~MapConverterToGraph () {}

  TypesDef::GraphPtr convertMatrix (TypesDef::MapPtr map);

  static MapConverterToGraph& getSingleton ();
  static MapConverterToGraph* getSingletonPtr ();
};

#endif /* MapConverterToGraph_H */
