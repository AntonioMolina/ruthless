#ifndef RankingPosition_H
#define RankingPosition_H

#include <string>

class RankingPosition {
 public:
  RankingPosition (unsigned int rounds, unsigned int points, std::string name);
  ~RankingPosition ();

  void setRound(unsigned int rounds);
  void setPoints(unsigned int points);
  void setName(std::string name);

  unsigned int getPoints();
  unsigned int getRound();
  std::string getName();

 private:
  unsigned int _rounds;
  unsigned int _points;
  std::string _name;
};

#endif /* RankingPosition_H */
