#ifndef Health_Restore_Drop_H
#define Health_Restore_Drop_H

#include "Drop.h"

#include "Constants.h"

class HealthRestoreDrop : public Drop {
 public:
  HealthRestoreDrop (Ogre::Vector3 position, unsigned int id);
  ~HealthRestoreDrop ();

  unsigned int getHealthToRecover () { return _healthToRecover; }
  Ogre::Vector3 getPosition () { return _position; }

  void setHealthToRecover (unsigned int healthToRecover) { _healthToRecover = healthToRecover; }
  void setPosition (Ogre::Vector3 position) { _position = position; _nodeDrop->setPosition(position); }

  void update(Ogre::Real deltaT) override;

 private:
  unsigned int _healthToRecover;
  Ogre::Vector3 _position;
};

#endif /* Health_Restore_Drop_H */
