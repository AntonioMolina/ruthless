#ifndef Drop_H
#define Drop_H

#include <Ogre.h>

class Drop {
 public:
  Drop () {}
  ~Drop () {}

  Ogre::SceneNode* getNode () { return _nodeDrop; }
  Ogre::Entity* getEntity () { return _entDrop; }

  void setNode (Ogre::SceneNode* nodeDrop) { _nodeDrop = nodeDrop; }
  void setEntity (Ogre::Entity* entDrop) { _entDrop = entDrop; }

  virtual void update(Ogre::Real deltaT) = 0;

 protected:
  Ogre::SceneNode* _nodeDrop;
  Ogre::Entity* _entDrop;
};

#endif /* Drop_H */
