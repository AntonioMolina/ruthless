#ifndef Ammo_Drop_H
#define Ammo_Drop_H

#include "Drop.h"

#include "Constants.h"

class AmmoDrop : public Drop {
 public:
  AmmoDrop (Ogre::Vector3 position, unsigned int id);
  ~AmmoDrop ();

  unsigned int getMagazineToRecover () { return _magazineToRecover; }
  Ogre::Vector3 getPosition () { return _position; }

  void setMagazineToRecover (unsigned int magazineToRecover) { _magazineToRecover = magazineToRecover; }
  void setPosition (Ogre::Vector3 position) { _position = position; _nodeDrop->setPosition(position); }

  void update (Ogre::Real deltaT) override;

 private:
  unsigned int _magazineToRecover;
  Ogre::Vector3 _position;

  bool _firstAnimEnded;
};

#endif /* Ammo_Drop_H */
