#ifndef GRAPHNODE_H
#define GRAPHNODE_H

#include <iostream>
#include <OgreVector3.h>

#include "Constants.h"

class GraphNode
{
 public:
  GraphNode ();
  GraphNode (const int& index, const Ogre::Vector3& position,
        const GameEnums::TypeCell type = GameEnums::TypeCell::FreeCell);
  ~GraphNode ();

  int getIndex () const { return _index; }
  GameEnums::TypeCell getType () const { return _type; }
  Ogre::Vector3 getPosition () const { return _position; }
  void setPosition(Ogre::Vector3 position){ _position = position; }
  operator std::string () const;

 private:
  int _index;
  Ogre::Vector3 _position;
  GameEnums::TypeCell _type;
};

#endif /* GRAPHNODE_H */
