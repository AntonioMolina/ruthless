#ifndef GRAPH_H
#define GRAPH_H

#include <iostream>
#include <vector>

#include "GraphVertex.h"
#include "GraphEdge.h"

class Graph
{
 public:
  Graph ();
  ~Graph ();

  void addVertex (TypesDef::GraphVertexPtr pVertex);
  void addEdge (TypesDef::GraphVertexPtr pOrigin,
                TypesDef::GraphVertexPtr pDestination,
		            bool undirected = true);

  std::vector<TypesDef::GraphVertexPtr> adjacents (int index) const;

  TypesDef::GraphVertexPtr getVertex (int index) const;
  std::vector<TypesDef::GraphVertexPtr> getVertexes () const { return _vertexes; }
  std::vector<TypesDef::GraphEdgePtr> getEdges () const { return _edges; }

 private:
  std::vector<TypesDef::GraphVertexPtr> _vertexes;
  std::vector<TypesDef::GraphEdgePtr> _edges;
};

#endif
