#ifndef GRAPHEDGE_H
#define GRAPHEDGE_H

#include <iostream>
#include <vector>

#include "Constants.h"

class GraphVertex;

class GraphEdge
{
 public:
  GraphEdge (TypesDef::GraphVertexPtr origin, TypesDef::GraphVertexPtr dest);
  ~GraphEdge ();

  void setOrigin (TypesDef::GraphVertexPtr origin) { _pOrigin = origin; }
  TypesDef::GraphVertexPtr getOrigin () const { return _pOrigin; }

  void setDestination (TypesDef::GraphVertexPtr dest) { _pDestination = dest; }
  TypesDef::GraphVertexPtr getDestination () const { return _pDestination; }

 private:
  TypesDef::GraphVertexPtr _pOrigin;
  TypesDef::GraphVertexPtr _pDestination;
};

#endif
