#ifndef GRAPHVERTEX_H
#define GRAPHVERTEX_H

#include <iostream>
#include <vector>

#include "GraphEdge.h"
#include "GraphNode.h"

#include "Constants.h"

class GraphVertex
{
 public:
  GraphVertex (const GraphNode& data);
  GraphVertex();
  ~GraphVertex ();

  GraphNode getData () const { return _data; }
  void addEdge (TypesDef::GraphEdgePtr pEdge) { _edges.push_back(pEdge); }

  std::vector<TypesDef::GraphEdgePtr> getEdges () const { return _edges; }
  std::vector<TypesDef::GraphVertexPtr> adjacents () const;

 private:
  GraphNode _data;
  std::vector<TypesDef::GraphEdgePtr> _edges;
};

#endif
