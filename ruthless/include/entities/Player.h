#ifndef Player_H
#define Player_H

#include <memory>

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <OgreBulletCollisionsShape.h>

#include "Constants.h"
#include "Weapon.h"

class Player {
 public:
  Player ();
  ~Player ();

  unsigned int getCoins () const { return _coins; };
  unsigned int getPuntuation () const { return _puntuation; };

  TypesDef::WeaponPtr getCurrentWeapon () const { return _currentWeapon; }

  void substractCoins (unsigned coins) { _coins -= coins; }

  Ogre::SceneNode* getSceneNode () const { return _nodePlayer; }
  Ogre::Entity* getEntity () const { return _entPlayer; }
  OgreBulletCollisions::CollisionShape* getCollisionShape () const { return _playerShape; }
  OgreBulletDynamics::RigidBody* getRigidBody () const { return _bodyPlayer; }

  void setSceneNode (Ogre::SceneNode* sceneNode)
  {
    _nodePlayer = sceneNode;
  }
  void setCollisionShape (OgreBulletCollisions::CollisionShape* collisionShape)
  {
    _playerShape = collisionShape;
  }
  void setRigidBody (OgreBulletDynamics::RigidBody* rigidBody)
  {
    _bodyPlayer = rigidBody;
  }

  void moveFront () { _front = true; }
  void moveBack () { _back = true; }
  void moveLeft () { _left = true; }
  void moveRight () { _right = true; }

  void stopFront () { _front = false; };
  void stopBack () { _back = false; };
  void stopLeft () { _left = false; };
  void stopRight () { _right = false; };

  void addPointsMonsterKilled (unsigned int points) {
    _puntuation += points;
  };

  void addCoinsMonsterKilled (unsigned int coins) {
    _coins += coins;
  };

  void update (Ogre::Real deltaT, std::vector<TypesDef::EnemyPtr> enemies);

  void rotateCameraPlayer (Ogre::Real rot_x, Ogre::Real rot_y);

  void jump ();

  void hidePlayer ();
  void showPlayer ();

  void reload ();
  void swapWeapons ();
  void startShooting ();
  void stopShooting ();

  Ogre::Camera* getCamera () { return _cameraPlayer; }

  unsigned int getIdCurrentWeapon() { return _idCurrentWeapon; }

  void addAmmo (unsigned int magazines)
  {
    _weapons.at(GameConstants::numMainWeapon)->addReserveAmmo(magazines * GameConstants::maxAmmoRifle);
  }

 private:
  unsigned int _coins;
  unsigned int _puntuation;

  /* Variables for movement */
  bool _front, _back, _left, _right;
  bool _isJumping;

  /* SceneNode & Entity */
  Ogre::SceneManager* _sceneMgr;
  Ogre::SceneNode* _nodePlayer;
  Ogre::Entity* _entPlayer;

  OgreBulletCollisions::CollisionShape* _playerShape;
  OgreBulletDynamics::RigidBody* _bodyPlayer;

  /* Variables for camera */
  Ogre::Camera* _cameraPlayer;

  /* Variables for weapons swap */
  Ogre::SceneNode* _nodeWeapon;
  std::vector<TypesDef::WeaponPtr> _weapons;
  unsigned int _idCurrentWeapon;
  TypesDef::WeaponPtr _currentWeapon;
  bool _shooting;

  bool _swapingWeapons;
  Ogre::Real _timeLeftSwapingWeapons;

  void _createWeapons ();
  inline void _checkJumpingState ();
};

#endif /* Player_H */
