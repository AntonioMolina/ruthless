#ifndef Core_H
#define Core_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <OgreBulletCollisionsShape.h>

#include "Constants.h"
#include "Graph.h"
#include "Map.h"

class Core {
 public:
  Core (TypesDef::MapPtr map, int i, int j);
  ~Core ();

  void hit (unsigned int dmg);
  void restoreHealth (unsigned int healthToRestore)
  {
    if (_health < GameConstants::maxLifeCore)
    {
      if ((healthToRestore + _health) > GameConstants::maxLifeCore)
      {
        _health = GameConstants::maxLifeCore;
      } else
      {
        _health += healthToRestore;
      }
    }
  }

  inline unsigned int getHealth () const { return _health; }

  Ogre::Entity* getEntity () const { return _entCore; }
  Ogre::SceneNode* getSceneNode () const { return _nodeCore; }

  OgreBulletCollisions::CollisionShape* getCollisionShape () const { return _shapeCore; }
  OgreBulletDynamics::RigidBody* getRigidBody () const { return _rigBodyCore; }

  void setEntity (Ogre::Entity* ent) { _entCore = ent; }
  void setSceneNode (Ogre::SceneNode* node) { _nodeCore = node; }

  void setCollisionShape (OgreBulletCollisions::CollisionShape* shape) { _shapeCore = shape; }
  void setRigidBody (OgreBulletDynamics::RigidBody* body) { _rigBodyCore = body; }

 private:
  unsigned int _health;

  Ogre::Entity* _entCore;
  Ogre::SceneNode* _nodeCore;

  OgreBulletCollisions::CollisionShape* _shapeCore;
  OgreBulletDynamics::RigidBody* _rigBodyCore;
};

#endif /* Core_H */
