#ifndef CannonTurret_H
#define CannonTurret_H

#include <Ogre.h>

#include "Constants.h"
#include "Turret.h"

class CannonTurret : public Turret {
 public:
  CannonTurret (unsigned int id, Ogre::Vector3 position);
  ~CannonTurret ();

  void update (Ogre::Real deltaT, std::vector<TypesDef::EnemyPtr> enemies) override;
  TypesDef::EnemyPtr getBestEnemy (std::vector<TypesDef::EnemyPtr> enemies) override;
};

#endif /* CannonTurret_H */
