#ifndef MachineGunTurret_H
#define MachineGunTurret_H

#include <Ogre.h>

#include "Constants.h"
#include "Turret.h"

class MachineGunTurret : public Turret {
 public:
  MachineGunTurret (unsigned int id, Ogre::Vector3 position);
  ~MachineGunTurret ();

  void update (Ogre::Real deltaT, std::vector<TypesDef::EnemyPtr> enemies) override;
  TypesDef::EnemyPtr getBestEnemy (std::vector<TypesDef::EnemyPtr> enemies) override;
};

#endif /* MachineGunTurret_H */
