#ifndef Turret_H
#define Turret_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsTrimeshShape.h>

#include "Constants.h"
#include "Enemy.h"

class Turret {
 public:
  Turret (unsigned int dmg, Ogre::Real rateOfFire, unsigned int id, Ogre::Vector3 position) :
              _dmg(dmg),
              _rateOfFire(rateOfFire),
              _timeLastShoot(0.0f),
              _id(id),
              _pos(position)
  {

  }

  ~Turret () {}

  unsigned int getDmg () { return _dmg; }
  unsigned int getID () { return _id; }
  Ogre::Vector3 getPosition () { return _pos; }

  virtual void update (Ogre::Real deltaT, std::vector<TypesDef::EnemyPtr> enemies) = 0;

 protected:
  unsigned int _dmg;
  Ogre::Real _rateOfFire;

  Ogre::Real _timeLastShoot;

  unsigned int _id;
  Ogre::Vector3 _pos;

  Ogre::SceneNode* _turretNode;
  Ogre::Entity* _turretEnt;

  OgreBulletCollisions::CollisionShape* _shapeTurret;
  OgreBulletDynamics::RigidBody* _rigBodyTurret;

  TypesDef::EnemyPtr _currentEnemy;

  virtual TypesDef::EnemyPtr getBestEnemy (std::vector<TypesDef::EnemyPtr> enemies) = 0;
};

#endif /* Turret_H */
