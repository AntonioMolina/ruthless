  #ifndef Enemy_Type_2_H
#define Enemy_Type_2_H

#include "Enemy.h"

#include "SoundFXManager.h"
#include "TrackManager.h"

class Enemy_Type_2 : public Enemy {
 public:
  Enemy_Type_2 (TypesDef::MapPtr map, unsigned int round, unsigned int index);
  ~Enemy_Type_2 ();

  void hit (unsigned int dmgTaken) override;

private:

  SoundFXManager* _pSoundFXManager;
  SoundFXPtr _effectDied;
};

#endif /* Enemy_Type_2_H */
