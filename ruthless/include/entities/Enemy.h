#ifndef Enemy_H
#define Enemy_H

#include <Ogre.h>

#include <OgreBulletDynamicsRigidBody.h>
#include <OgreBulletCollisionsShape.h>

#include "Constants.h"
#include "Core.h"
#include "Graph.h"
#include "GraphVertex.h"
#include "Map.h"

class Enemy {
 public:
  Enemy (unsigned int dmg, unsigned int health, TypesDef::MapPtr map) :
      _dmg(dmg), _health(health), _map(map)
  {}

  ~Enemy () {
  }

  unsigned int getDmg () const { return _dmg; }
  unsigned int getHealth () const { return _health; }

  Ogre::SceneNode* getNode () const { return _enemyNode; }
  Ogre::Entity* getEntity () const { return _enemyEnt; }

  OgreBulletCollisions::CollisionShape* getShape () { return _enemyShape; }
  OgreBulletDynamics::RigidBody* getBody () { return _bodyEnemy; }

  TypesDef::GraphVertexPtr getactVertex () const { return _currentVertex;}
  TypesDef::GraphVertexPtr getnextVertex () const { return _nextVert;}
  int getDirection () const { return _direction; }

  void setactVertex (TypesDef::GraphVertexPtr actVert) { _currentVertex = actVert; }
  void setnextVertex (TypesDef::GraphVertexPtr nextVert) { _nextVert = nextVert; }
  void setDirection (int direction) { _direction = direction; }

  inline void update (Ogre::Real deltaT, TypesDef::CorePtr core) {
    if (_health > 0)
    {
      if (_enemyEnt->getWorldBoundingBox().intersects(core->getEntity()->getWorldBoundingBox()))
      {
        core->hit(_dmg);
        respawn();
      } else
      {
        /* Check if arrived to next vertex */
        if (_direction == 0 &&
            //_enemyNode->getPosition().z < _nextVert->getData().getPosition().z)
            _bodyEnemy->getWorldPosition().z < _nextVert->getData().getPosition().z)
        {
          int currentIndex = _currentVertex->getData().getIndex();
          Ogre::Vector3 nextVertexPos =  _nextVert->getData().getPosition();

          /* Set current position to next vertex */
          nextVertexPos.y = nextVertexPos.y + 0.1;
          //_enemyNode->setPosition(nextVertexPos);
          _bodyEnemy->setPosition(nextVertexPos);

          if (_nextVert->getData().getType() != GameEnums::TypeCell::CoreCell)
          {
            /* Update current and next vertexes */
            _currentVertex = _nextVert;
            _nextVert = getBestVertex(_nextVert, currentIndex);

            /* Update direction */
            if (nextVertexPos.z > _nextVert->getData().getPosition().z)
            {
              _direction = 0;
            } else if (nextVertexPos.x > _nextVert->getData().getPosition().x)
            {
              _direction = -1;
            } else if (nextVertexPos.x < _nextVert->getData().getPosition().x)
            {
              _direction = 1;
            }
          } else
          {
            respawn();
          }
        }

        if (_direction == 1 &&
            //_enemyNode->getPosition().x > _nextVert->getData().getPosition().x)
            _bodyEnemy->getWorldPosition().x > _nextVert->getData().getPosition().x)
        {
          int currentIndex = _currentVertex->getData().getIndex();
          Ogre::Vector3 nextVertexPos =  _nextVert->getData().getPosition();

          /* Set current position to next vertex */
          nextVertexPos.y = nextVertexPos.y + 0.1;
          //_enemyNode->setPosition(nextVertexPos);
          _bodyEnemy->setPosition(nextVertexPos);

          if (_nextVert->getData().getType() != GameEnums::TypeCell::CoreCell)
          {
            /* Update current and next vertexes */
            _currentVertex = _nextVert;
            _nextVert = getBestVertex(_nextVert, currentIndex);

            /* Update direction */
            if (nextVertexPos.z > _nextVert->getData().getPosition().z)
            {
              _direction = 0;
            } else if (nextVertexPos.x > _nextVert->getData().getPosition().x)
            {
              _direction = -1;
            } else if (nextVertexPos.x < _nextVert->getData().getPosition().x)
            {
              _direction = 1;
            }
          } else
          {
            respawn();
          }
        }

        if (_direction == -1 &&
            //_enemyNode->getPosition().x < _nextVert->getData().getPosition().x)
            _bodyEnemy->getWorldPosition().x < _nextVert->getData().getPosition().x)
        {
          int currentIndex = _currentVertex->getData().getIndex();
          Ogre::Vector3 nextVertexPos =  _nextVert->getData().getPosition();

          /* Set current position to next vertex */
          nextVertexPos.y = nextVertexPos.y + 0.1;
          //_enemyNode->setPosition(nextVertexPos);
          _bodyEnemy->setPosition(nextVertexPos);

          if (_nextVert->getData().getType() != GameEnums::TypeCell::CoreCell)
          {
            /* Update current and next vertexes */
            _currentVertex = _nextVert;
            _nextVert = getBestVertex(_nextVert, currentIndex);

            /* Update direction */
            if(nextVertexPos.z > _nextVert->getData().getPosition().z)
            {
              _direction = 0;
            } else if (nextVertexPos.x > _nextVert->getData().getPosition().x)
            {
              _direction = -1;
            } else if (nextVertexPos.x < _nextVert->getData().getPosition().x)
            {
              _direction = 1;
            }
          } else
          {
            respawn();
          }
        }

        if (_direction == 1)
        {
          _bodyEnemy->setOrientation(0.0f, 0.0f, 1.0f, 0.0f);
          _bodyEnemy->setPosition(_bodyEnemy->getWorldPosition() +
                                  Ogre::Vector3((GameConstants::enemySpeed * deltaT), 0.0, 0.0));
        } else if (_direction == -1)
        {
          _bodyEnemy->setOrientation(1.0f, 0.0f, 0.0f, 0.0f);
          _bodyEnemy->setPosition(_bodyEnemy->getWorldPosition() +
                                  Ogre::Vector3(-(GameConstants::enemySpeed * deltaT), 0.0, 0.0));
        } else if (_direction == 0)
        {
          _bodyEnemy->setOrientation(1.0f, 0.0f, -1.0f, 0.0f);
          _bodyEnemy->setPosition(_bodyEnemy->getWorldPosition() +
                                  Ogre::Vector3(0.0, 0.0, -(GameConstants::enemySpeed * deltaT)));
        }
      }
    }
  }

  void respawn ()
  {
    std::vector<Ogre::Vector2> spawns = _map->getSpawns();
    int columns = _map->getColumns();

    /* Get random spawn */
    unsigned int randomSpawn = rand() % (spawns.size());
    Ogre::Vector2 spawnVect = spawns.at(randomSpawn);

    _currentVertex = _map->getMapGraph()->getVertex((int) spawnVect.x * columns + (int)spawnVect.y);

    Ogre::Vector3 position = _currentVertex->getData().getPosition();
    position.y = position.y + 0.1;

    /* Get next Vertex (only a path cell) */
    for (unsigned int i = 0; i < _currentVertex->adjacents().size(); ++i)
    {
      if (_currentVertex->adjacents().at(i)->getData().getType() == GameEnums::TypeCell::PathCell)
      {
        _nextVert = _currentVertex->adjacents().at(i);
        if (_nextVert->getData().getPosition().z < _currentVertex->getData().getPosition().z)
        {
          _direction = 0;
        }
        if (_nextVert->getData().getPosition().x < _currentVertex->getData().getPosition().x)
        {
          _direction = -1;
        }
        if (_nextVert->getData().getPosition().x > _currentVertex->getData().getPosition().x)
        {
          _direction = 1;
        }
      }
    }

    /* Set position Enemy */
    //_enemyNode->setPosition(position);
    _bodyEnemy->setPosition(position);

    /* Set orientation */
    if (_direction == 1)
    {
      _enemyNode->setOrientation(0.0f, 0.0f, 1.0f, 0.0f);
    } else if (_direction == -1)
    {
      _enemyNode->setOrientation(1.0f, 0.0f, 0.0f, 0.0f);
    } else if (_direction == 0)
    {
      _enemyNode->setOrientation(1.0f, 0.0f, -1.0f, 0.0f);
    }
  }

  inline TypesDef::GraphVertexPtr getBestVertex (TypesDef::GraphVertexPtr currentNextVertex, int currentIndex)
  {
    TypesDef::GraphVertexPtr nextVertexToReturn;
    std::vector<TypesDef::GraphVertexPtr> adjacents = currentNextVertex->adjacents();

    bool bestVertexFound = false;

    /* Get core cell */
    for (unsigned int i = 0; i < adjacents.size(); ++i)
    {
      if (adjacents.at(i)->getData().getType() == GameEnums::TypeCell::CoreCell &&
          currentIndex != adjacents.at(i)->getData().getIndex())
      {
        nextVertexToReturn = adjacents.at(i);
        bestVertexFound = true;
      }
    }

    /* Vertex for core (z > current z) */
    if(bestVertexFound == false)
    {
      for (unsigned int i = 0; i < adjacents.size(); ++i)
      {
        if (adjacents.at(i)->getData().getType() == GameEnums::TypeCell::PathCell &&
            currentIndex != adjacents.at(i)->getData().getIndex())
        {
          if (adjacents.at(i)->getData().getPosition().z < currentNextVertex->getData().getPosition().z)
          {
            nextVertexToReturn = adjacents.at(i);
            bestVertexFound = true;
          }
        }
      }
    }

    /* If not core and not cell in same column */
    if(bestVertexFound == false)
    {
      for (unsigned int i = 0; i < adjacents.size(); ++i)
      {
        if (adjacents.at(i)->getData().getType() == GameEnums::TypeCell::PathCell &&
            currentIndex != adjacents.at(i)->getData().getIndex())
        {
          nextVertexToReturn = adjacents.at(i);
        }
      }
    }

    return nextVertexToReturn;
  }

  virtual void hit (unsigned int dmgTaken) = 0;

 protected:
  unsigned int _dmg;
  unsigned int _health;

  Ogre::SceneNode* _enemyNode;
  Ogre::Entity* _enemyEnt;

  OgreBulletCollisions::CollisionShape* _enemyShape;
  OgreBulletDynamics::RigidBody* _bodyEnemy;

  /* Variables for movement */
  TypesDef::MapPtr _map;
  TypesDef::GraphVertexPtr _currentVertex;
  TypesDef::GraphVertexPtr _nextVert;
  int _direction;
};

#endif /* Enemy_H */
