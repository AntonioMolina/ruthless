#ifndef Weapon_Factory_H
#define Weapon_Factory_H

#include <memory>

#include <Ogre.h>

#include "Constants.h"

#include "Pistol.h"
#include "Rifle.h"
#include "Weapon.h"

class WeaponFactory : public Ogre::Singleton<WeaponFactory> {
 public:
  WeaponFactory () {}
  ~WeaponFactory () {}
  
  TypesDef::WeaponPtr createWeapon (GameEnums::TypeWeapon type);

  static WeaponFactory& getSingleton ();
  static WeaponFactory* getSingletonPtr ();
};

#endif /* Weapon_Factory_H */
