#ifndef Turret_Factory_H
#define Turret_Factory_H

#include <memory>

#include <Ogre.h>

#include "Constants.h"

#include "CannonTurret.h"
#include "MachineGunTurret.h"
#include "Turret.h"

class TurretFactory : public Ogre::Singleton<TurretFactory> {
 public:
  TurretFactory () {}
  ~TurretFactory () {}
  
  TypesDef::TurretPtr createTurret (GameEnums::TypeTurret type, unsigned int id,
      Ogre::Vector3 position);

  static TurretFactory& getSingleton ();
  static TurretFactory* getSingletonPtr ();
};


#endif /* Turret_Factory_H */
