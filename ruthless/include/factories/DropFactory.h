#ifndef Drop_Factory_H
#define Drop_Factory_H

#include <memory>

#include <Ogre.h>

#include "Constants.h"

class DropFactory : public Ogre::Singleton<DropFactory> {
 public:
  DropFactory () {}
  ~DropFactory () {}

  TypesDef::DropPtr createDrop (GameEnums::TypesDrop type, Ogre::Vector3 pos, unsigned int index);

  static DropFactory& getSingleton ();
  static DropFactory* getSingletonPtr ();
};

#endif /* Drop_Factory_H */
