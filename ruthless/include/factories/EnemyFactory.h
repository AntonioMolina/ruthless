#ifndef Enemy_Factory_H
#define Enemy_Factory_H

#include <memory>

#include <Ogre.h>

#include "Constants.h"

#include "Enemy.h"
#include "Enemy_Type_1.h"
#include "Enemy_Type_2.h"

class EnemyFactory : public Ogre::Singleton<EnemyFactory> {
 public:
  EnemyFactory () {}
  ~EnemyFactory () {}

  TypesDef::EnemyPtr createEnemy (GameEnums::TypeEnemy type, TypesDef::MapPtr map,
      unsigned int round, unsigned int index);

  static EnemyFactory& getSingleton ();
  static EnemyFactory* getSingletonPtr ();
};

#endif /* Enemy_Factory_H */
