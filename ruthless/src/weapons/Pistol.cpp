#include "Pistol.h"

#include "Constants.h"
#include "Enemy.h"
#include "StringSplit.h"
#include "Weapon.h"

Pistol::Pistol () :
      Weapon(GameConstants::dmgPistol, GameConstants::maxAmmoPistol,
             GameConstants::maxAmmoReservePistol,
             GameConstants::timeBetweenShootsPistol)
{
  _isPosibleShoot = false;

  _weaponNode = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->
      getSceneNode("WeaponNode");
  _weaponEnt = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->
      createEntity("PistolEntity", "pistola.mesh");
  _weaponNode->attachObject(_weaponEnt);

  _weaponEnt->setVisible(false);

  _shot = false;
}

Pistol::~Pistol () {
  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");

  sceneMgr->destroyEntity(_weaponEnt);
}

void Pistol::update (Ogre::Real deltaT) {
  if (_isReloading) {
    _timeReloading -= deltaT;
    if (_timeReloading <= 0.0f) {
      _isReloading = false;

      _ammoMagazine = GameConstants::maxAmmoPistol;
      _timeLastShoot = GameConstants::timeBetweenShootsPistol;
    }
  } else
  {
    if (_timeLastShoot > 0)
    {
      _timeLastShoot -= deltaT;
    }

    if (_timeLastShoot <= 0)
    {
      _isPosibleShoot = true;
    }
  }
}

void Pistol::addReserveAmmo (unsigned int ammo) {}

void Pistol::reload () {
  _isReloading = true;
  _timeReloading = GameConstants::timeReloadPistol;
}

void Pistol::shoot (std::vector<TypesDef::EnemyPtr> enemies) {
  if (_isPosibleShoot && !_shot)
  {
    if (_ammoMagazine == 0)
    {
      if (!_isReloading)
      {
        reload();
      }
    } else
    {
      /* Shoot on screen center */
      OgreBulletDynamics::RigidBody* bodyhitted = hitEnemy();
      if (bodyhitted != nullptr)
      {
        std::string nameBodyHitted = bodyhitted->getName();
        std::vector<std::string> nameSplit = StringSplit::getSingletonPtr()->split(nameBodyHitted, '_');

        /* Check if is a Enemy */
        if (nameSplit[0] == "enemy")
        {
          for (unsigned int i = 0; i < enemies.size(); ++i)
          {
            TypesDef::EnemyPtr enemy = enemies.at(i);
            if (enemy->getNode()->getName() == nameBodyHitted)
            {
              enemy->hit(_dmg);
              break;
            }
          }
        }
      }

      --_ammoMagazine;
      _timeLastShoot = GameConstants::timeBetweenShootsPistol;
      _isPosibleShoot = false;
      _shot = true;
    }
  }
}
