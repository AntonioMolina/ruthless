#include "Rifle.h"

#include "Constants.h"
#include "Enemy.h"
#include "StringSplit.h"
#include "Weapon.h"

Rifle::Rifle () :
      Weapon(GameConstants::dmgRifle, GameConstants::maxAmmoRifle,
             GameConstants::initialAmmoReserveRifle,
             GameConstants::timeBetweenShootsRifle)
{
  _isPosibleShoot = false;

  _weaponNode = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->
      getSceneNode("WeaponNode");
  _weaponEnt = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager")->
      createEntity("RifleEntity", "rifle.mesh");
  _weaponNode->attachObject(_weaponEnt);

  _weaponEnt->setVisible(false);
}

Rifle::~Rifle () {
  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");

  sceneMgr->destroyEntity(_weaponEnt);
}

void Rifle::update (Ogre::Real deltaT) {
  if (_isReloading) {
    _timeReloading -= deltaT;
    if (_timeReloading <= 0.0f) {
      _isReloading = false;

      if (_ammoMagazine == 0)
      {
        if (_ammoReserve >= GameConstants::maxAmmoRifle) {
          _ammoMagazine = GameConstants::maxAmmoRifle;
          _ammoReserve -= GameConstants::maxAmmoRifle;
        } else {
          _ammoMagazine = _ammoReserve;
          _ammoReserve = 0;
        }
      } else
      {
        unsigned int ammoToRestore = GameConstants::maxAmmoRifle - _ammoMagazine;
        if (_ammoReserve >= ammoToRestore) {
          _ammoMagazine = GameConstants::maxAmmoRifle;
          _ammoReserve -= ammoToRestore;
        } else {
          _ammoMagazine += _ammoReserve;
          _ammoReserve = 0;
        }
      }

      _timeLastShoot = GameConstants::timeBetweenShootsRifle;
    }
  } else
  {
    if (_timeLastShoot > 0)
    {
      _timeLastShoot -= deltaT;
    }

    if (_timeLastShoot <= 0)
    {
      _isPosibleShoot = true;
    }
  }
}

void Rifle::addReserveAmmo (unsigned int ammo) {
  if (_ammoReserve < GameConstants::maxAmmoReserveRifle) {
    if ((_ammoReserve + ammo ) > GameConstants::maxAmmoReserveRifle) {
      _ammoReserve = GameConstants::maxAmmoReserveRifle;
    } else {
      _ammoReserve += ammo;
    }
  }
}

void Rifle::reload () {
  if (_ammoMagazine < GameConstants::maxAmmoRifle)
  {
    _isReloading = true;
    _timeReloading = GameConstants::timeReloadRifle;
  }
}

void Rifle::shoot (std::vector<TypesDef::EnemyPtr> enemies) {
  if (_isPosibleShoot)
  {
    if (_ammoMagazine == 0)
    {
      if (!_isReloading)
      {
        reload();
      }
    } else
    {
      /* Shoot on screen center */
      OgreBulletDynamics::RigidBody* bodyhitted = hitEnemy();
      if (bodyhitted != nullptr)
      {
        std::string nameBodyHitted = bodyhitted->getName();
        std::vector<std::string> nameSplit = StringSplit::getSingletonPtr()->split(nameBodyHitted, '_');

        /* Check if is a Enemy */
        if (nameSplit[0] == "enemy")
        {
          for (unsigned int i = 0; i < enemies.size(); ++i)
          {
            TypesDef::EnemyPtr enemy = enemies.at(i);
            if (enemy->getNode()->getName() == nameBodyHitted)
            {
              enemy->hit(_dmg);
              break;
            }
          }
        }
      }

      --_ammoMagazine;
      _timeLastShoot = GameConstants::timeBetweenShootsRifle;
      _isPosibleShoot = false;
    }
  }
}
