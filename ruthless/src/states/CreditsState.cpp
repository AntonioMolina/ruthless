#include "CreditsState.h"

#include "MouseConverter.h"

template<> CreditsState* Singleton<CreditsState>::msSingleton = 0;

CreditsState::CreditsState () : _sheet(nullptr), _root(nullptr),
        _sceneMgr(nullptr), _camera(nullptr), _back(false) {
}

void CreditsState::enter () {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _back = false;
    createGUI();
    createBackground();

    std::cout<< "entrando en CreditsState";
}

void CreditsState::exit () {
    _root->getAutoCreatedWindow()->removeAllViewports();
    deleteGUI();
    deleteBackground();
}

void CreditsState::pause () {}

void CreditsState::resume () {}

void CreditsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
        case OIS::KC_C:
            _back = true;
            break;
        default:
            break;
    }
}

void CreditsState::keyReleased (const OIS::KeyEvent &e) {}

void CreditsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void CreditsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonDown(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

void CreditsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonUp(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

bool CreditsState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool CreditsState::frameEnded (const FrameEvent& evt) {
    if (_back) {
        popState();
    }

    return true;
}

CreditsState& CreditsState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

CreditsState* CreditsState::getSingletonPtr () {
    return msSingleton;
}

void CreditsState::createGUI () {
  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Credits");

  /* Back Menu button */

  CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");

    backButton->setText("Volver");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&CreditsState::back, this));

    CEGUI::Window* creditsTitle = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "CreditsTitle");
    creditsTitle->setText("[font='DejaVuSans-25'][colour='FFD7D7D7']Developers");
    creditsTitle->setSize(CEGUI::USize(CEGUI::UDim(0.35,0),CEGUI::UDim(0.15,0)));
    creditsTitle->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.35/2,0),CEGUI::UDim(0.5/2,0)));

    CEGUI::Window* credits = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "CreditsName");
    credits->setText("[font='DejaVuSans-12'][colour='FFD7D7D7']Maldonado Burgos, Julia\nMolina Maroto, Antonio\nMendoza Madronal, Fco Javier");
    credits->setSize(CEGUI::USize(CEGUI::UDim(0.35,0),CEGUI::UDim(0.15,0)));
    credits->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.35/2,0),CEGUI::UDim(0.5-0.35/2,0)));


    CEGUI::Window* creditsDesigner = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "Designers");
    creditsDesigner->setText("[font='DejaVuSans-25'][colour='FFD7D7D7']Designers");
    creditsDesigner->setSize(CEGUI::USize(CEGUI::UDim(0.35,0),CEGUI::UDim(0.15,0)));
    creditsDesigner->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.35/2,0),CEGUI::UDim(0.5-0.15/2,0)));

    CEGUI::Window* creditsDesignerName = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "DesignersName");
    creditsDesignerName->setText("[font='DejaVuSans-12'][colour='FFD7D7D7']Heliodoro Carbonell Cuevas \nSergio Such Pico");
    creditsDesignerName->setSize(CEGUI::USize(CEGUI::UDim(0.35,0),CEGUI::UDim(0.15,0)));
    creditsDesignerName->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.35/2,0),CEGUI::UDim(0.5-0.01/2,0)));

    _sheet->addChild(backButton);
    _sheet->addChild(credits);
    _sheet->addChild(creditsTitle);
    _sheet->addChild(creditsDesigner);
    _sheet->addChild(creditsDesignerName);

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);
}

void CreditsState::createBackground () {
  MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
  material->getTechnique(0)->getPass(0)->createTextureUnitState("backgroundMenu.png");
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

  Rectangle2D* rect = new Ogre::Rectangle2D(true);
  rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  rect->setMaterial("Background");

  rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);

  AxisAlignedBox aabInf;
  aabInf.setInfinite();
  rect->setBoundingBox(aabInf);

  SceneNode* node = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundMenu");
  node->attachObject(rect);
}

void CreditsState::deleteBackground () {
  _sceneMgr->clearScene();
}

void CreditsState::deleteGUI () {
  _sheet->destroyChild("BackButton");

  CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->destroyChild(_sheet);
}

bool CreditsState::back (const CEGUI::EventArgs &e) {
    _back = true;
    return true;
}
