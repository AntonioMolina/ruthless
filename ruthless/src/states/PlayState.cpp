#include "PlayState.h"

#include "FinalState.h"

#include "Cell.h"
#include "MapGenerator.h"
#include "PhysicsManager.h"
#include "StringSplit.h"
#include "RankingManager.h"

#include "EnemyFactory.h"
#include "TurretFactory.h"

#include "MouseConverter.h"

template<> PlayState* Ogre::Singleton<PlayState>::msSingleton = 0;




void PlayState::enter () {
  _root = Ogre::Root::getSingletonPtr();

  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("MainCamera");
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

  _overlayManager = Ogre::OverlayManager::getSingletonPtr();

  _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible(false);

  _exitGame = false;

  _round = 0;
  _totalMonstersKilled = 0;

  _mainCameraNode = _sceneMgr->getRootSceneNode()->createChildSceneNode("_CameraNode");
  _mainCameraNode->attachObject(_camera);
  _mainCameraNode->setPosition(0.0f, 0.0f, 0.0f);

  _rotateCamToLeft = false;
  _rotateCamToRight = false;


  _pTrackManager = TrackManager::getSingletonPtr();
  _pSoundFXManager = SoundFXManager::getSingletonPtr();

  _effectShoot=_pSoundFXManager->load("Sounds/shootGun.wav");

  /* Creation of Physics World */
  _world = PhysicsManager::getSingletonPtr()->createNewWorld();

  createScene();

  /* Create Player */
  _player = std::make_shared<Player>();

  /* Create GUIs */
  createGUITacticalState();
  createGUIWaveState();

  _pTrackGame = _pTrackManager->load("Music/GameMusic.mp3");
  _pTrackGame->play();

  /* Init as TacticalState */
  changeToTacticalState();

  _nDrop = 0;
}

void PlayState::exit () {
  _mainCameraNode->detachObject(_camera);

  FinalState::getSingletonPtr()->updateInfo(_player-> getPuntuation() , _round);

  /* Destroy GUIs */
  hideGUITacticalState();
  hideGUIWaveState();
  destroyGUITacticalState();
  destroyGUIWaveState();

  /* Set Position of camera and look at origen */
  _camera->setPosition(0.0f, 30.0f, -25.0f);
  _camera->lookAt(0.0f, 0.0f, 0.0f);

  /* Clear Turrets */
  for (unsigned int i = 0; i < _turrets.size(); ++i)
  {
    _turrets.pop_back();
  }

  /* Clear Enemies */
  for (unsigned int i = 0; i < _enemies.size(); ++i)
  {
    _enemies.pop_back();
  }

  /* Clear Drops */
  for (unsigned int i = 0; i < _drops.size(); ++i)
  {
    _drops.pop_back();
  }

  _core = nullptr;
  _player = nullptr;

  _map = nullptr;

  _sceneMgr->destroyQuery(_raySceneQuery);

  //_sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void PlayState::pause() {
}

void PlayState::resume() {
}

bool PlayState::frameStarted (const Ogre::FrameEvent& evt) {
  _deltaT = evt.timeSinceLastFrame;
  _world->stepSimulation(_deltaT, 1);

  if (_currentState == GameEnums::StateGame::WaveState)
  {
    updateWaveState();
  } else if (_currentState == GameEnums::StateGame::TacticalState)
  {
    updateTaticalState();
  }

  return true;
}

bool PlayState::frameEnded (const Ogre::FrameEvent& evt) {
  _deltaT = evt.timeSinceLastFrame;
  _world->stepSimulation(_deltaT, 1);

  return true;
}

void PlayState::keyPressed (const OIS::KeyEvent &e) {
  if (_currentState == GameEnums::StateGame::TacticalState)
  {
    /* Stop rotating camera */
    if (e.key == OIS::KC_A || e.key == OIS::KC_RIGHT)
    {
      _rotateCamToRight = false;
      _rotateCamToLeft = true;
    }

    if (e.key == OIS::KC_D || e.key == OIS::KC_LEFT)
    {
      _rotateCamToLeft = false;
      _rotateCamToRight = true;
    }

    /* Select Turret with 1 & 2 keys */
    if (e.key == OIS::KC_1)
    {
      _typeSelectedTurret = GameEnums::TypeTurret::MachineGunTurret;
    } else if (e.key == OIS::KC_2)
    {
      _typeSelectedTurret = GameEnums::TypeTurret::CannonTurret;
    }

    /* Chage to WaveState when player wants */
    if (e.key == OIS::KC_SPACE)
    {
      changeToWaveState();
    }
  } else if (_currentState == GameEnums::StateGame::WaveState)
  {
    /* Move Player */
    if (e.key == OIS::KC_W || e.key == OIS::KC_UP)
    {
      _player->stopBack();
      _player->moveFront();
    }

    if (e.key == OIS::KC_S || e.key == OIS::KC_DOWN)
    {
      _player->stopFront();
      _player->moveBack();
    }

    if (e.key == OIS::KC_A || e.key == OIS::KC_LEFT)
    {
      _player->stopRight();
      _player->moveLeft();
    }

    if (e.key == OIS::KC_D || e.key == OIS::KC_RIGHT)
    {
      _player->stopLeft();
      _player->moveRight();
    }

    /* Jump */
    if (e.key == OIS::KC_SPACE)
    {
      _player->jump();
    }

    /* Reload weapon */
    if (e.key == OIS::KC_R)
    {
      _player->reload();
    }

    /* Swap weapons */
    if (e.key == OIS::KC_Q)
    {
      _player->swapWeapons();
    }
  }

  if (_exitGame)
  {
    changeState(FinalState::getSingletonPtr());
  }
}

void PlayState::keyReleased (const OIS::KeyEvent &e) {
  if (_currentState == GameEnums::StateGame::TacticalState)
  {
    /* Stop moving camera */
    if (e.key == OIS::KC_A || e.key == OIS::KC_RIGHT)
    {
      _rotateCamToLeft = false;
    }

    if (e.key == OIS::KC_D || e.key == OIS::KC_LEFT)
    {
      _rotateCamToRight = false;
    }
  } else if (_currentState == GameEnums::StateGame::WaveState)
  {
    /* Stop moving player */
    if (e.key == OIS::KC_W || e.key == OIS::KC_UP)
    {
      _player->stopFront();
    }

    if (e.key == OIS::KC_S || e.key == OIS::KC_DOWN)
    {
      _player->stopBack();
    }

    if (e.key == OIS::KC_A || e.key == OIS::KC_LEFT)
    {
      _player->stopLeft();
    }

    if (e.key == OIS::KC_D || e.key == OIS::KC_RIGHT)
    {
      _player->stopRight();
    }
  }

  if (e.key == OIS::KC_ESCAPE || e.key == OIS::KC_P) {
    pushState(PauseState::getSingletonPtr());
  }

  #ifdef _DEBUG
  if (e.key == OIS::KC_H)
  {
    if (_currentState == GameEnums::StateGame::WaveState)
    {
      changeToTacticalState();
    }
  }
  #endif
}

void PlayState::mouseMoved (const OIS::MouseEvent &e) {
    Ogre::Real x = e.state.X.rel;
    Ogre::Real y = e.state.Y.rel;

    if (_currentState == GameEnums::StateGame::TacticalState)
    {
      CEGUI::System::getSingleton().getDefaultGUIContext().
          injectMouseMove(e.state.X.rel, e.state.Y.rel);

      /* Poder cambiar de color a las plataformas */
    } else if (_currentState == GameEnums::StateGame::WaveState)
    {
      Ogre::Real rot_x = x * _deltaT * -1;
      Ogre::Real rot_y = y * _deltaT * -1;

      /* Rotate player camera */
      _player->rotateCameraPlayer(rot_x, rot_y);
    }
}

void PlayState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
  CEGUI::System::getSingleton().getDefaultGUIContext()
      .injectMouseButtonDown(MouseConverter::getSingletonPtr()->convertMouseButton(id));
  if (_currentState == GameEnums::StateGame::TacticalState)
  {
    if (_typeSelectedTurret != GameEnums::TypeTurret::NoneType)
    {
      if (id == OIS::MB_Left)
      {
        /* Mouse pos */
        float posx = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition().d_x;
        float posy = CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().getPosition().d_y;
        uint32_t mask = Masks::PLATFORM_MASK;
        setRayQuery(posx, posy, mask);

        Ogre::RaySceneQueryResult& result = _raySceneQuery->execute();
        Ogre::RaySceneQueryResult::iterator it;

        it = result.begin();
        while (it != result.end())
        {
          Ogre::Node* nodeAux = it->movable->getParentNode();

          if (nodeAux->getName() != "TF")
          {
            std::string nameNode = nodeAux->getName();
            std::vector<std::string> nameNodeSplit =
                StringSplit::getSingletonPtr()->split(nameNode, '_');

            if (strcmp(nameNodeSplit[0].c_str(), "platform") == 0)
            {
              unsigned int row = atoi(nameNodeSplit[1].c_str());
              unsigned int column = atoi(nameNodeSplit[2].c_str());

              TypesDef::GraphPtr graph = _map->getMapGraph();
              TypesDef::GraphVertexPtr vertex = graph->
                  getVertex(row * _map->getColumns() + column);

              bool validPlat = true;
              Ogre::Vector3 platPosition = vertex->getData().getPosition();
              for (unsigned int i = 0; i < _turrets.size(); ++i)
              {
                Ogre::Vector3 posTurret = _turrets.at(i)->getPosition();
                if (posTurret.x == platPosition.x && posTurret.z == platPosition.z)
                {
                  validPlat = false;
                }
              }

              if (validPlat)
              {
                if (_typeSelectedTurret == GameEnums::TypeTurret::CannonTurret &&
                    _player->getCoins() >= GameConstants::costCannonTurret)
                {
                  _turrets.push_back(TurretFactory::getSingletonPtr()->
                      createTurret(GameEnums::TypeTurret::CannonTurret,
                                   _turrets.size(), platPosition));

                  /* Substract the cost of turrets to coins player */
                  _player->substractCoins(GameConstants::costCannonTurret);
                } else if (_typeSelectedTurret == GameEnums::TypeTurret::MachineGunTurret &&
                    _player->getCoins() >= GameConstants::costMachineGunTurret)
                {
                  _turrets.push_back(TurretFactory::getSingletonPtr()->
                      createTurret(GameEnums::TypeTurret::MachineGunTurret,
                                   _turrets.size(), platPosition));

                  /* Substract the cost of turrets to coins player */
                  _player->substractCoins(GameConstants::costMachineGunTurret);
                }
              }
            }
          }

          it++;
        }
      }
    }
  } else if (_currentState == GameEnums::StateGame::WaveState)
  {
    if (id == OIS::MB_Left)
    {
      _player->startShooting();
      _effectShoot->play();
    }
  }
}

void PlayState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
  CEGUI::System::getSingleton().getDefaultGUIContext()
        .injectMouseButtonUp(MouseConverter::getSingletonPtr()->convertMouseButton(id));
  if (_currentState == GameEnums::StateGame::TacticalState)
  {

  } else if (_currentState == GameEnums::StateGame::WaveState)
  {
    switch (id)
    {
      case OIS::MB_Left:
        _player->stopShooting();
        break;
      default:
        break;
    }
  }
}

PlayState* PlayState::getSingletonPtr () {
  return msSingleton;
}

PlayState& PlayState::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

void PlayState::updateTaticalState ()
{
  _timeToNextRound -= _deltaT;

  if (_timeToNextRound <= 0)
  {
    changeToWaveState();
  } else {
    if (_rotateCamToLeft)
    {
      _mainCameraNode->yaw(Ogre::Radian(GameConstants::rotationSpeedCamera * _deltaT * -1));
    } else if (_rotateCamToRight)
    {
      _mainCameraNode->yaw(Ogre::Radian(GameConstants::rotationSpeedCamera * _deltaT));
    }
  }

  updateGUITacticalState();
}

void PlayState::updateWaveState ()
{
  /* Update Player */
  _player->update(_deltaT, _enemies);

  /* Update Enemies */
  for (unsigned int i = 0; i < _enemies.size(); ++i)
  {
    _enemies.at(i)->update(_deltaT, _core);
  }

  /* Update Turrets */
  for (unsigned int i = 0; i < _turrets.size(); ++i)
  {
    _turrets.at(i)->update(_deltaT, _enemies);
  }

  /* Update Drops */
  for (unsigned int i = 0; i < _drops.size(); ++i)
  {
    _drops.at(i)->update(_deltaT);
  }

  if (!_roundStarted)
  {
    _timeToStartRound -= _deltaT;
    if (_timeToStartRound <= 0)
    {
      _roundStarted = true;
      _timeLastSpawn = 0.0f;
    }
  } else
  {
    if (_enemiesSpawned < _enemiesToSpawn)
    {
      _timeLastSpawn -= _deltaT;

      if (_timeLastSpawn <= 0.0f)
      {
        createEnemy();
      }
    }

    if (_core->getHealth() <= 0)
    {
      _exitGame = true;
    }

    if (isAllEnemiesDead())
    {
      changeToTacticalState();
    }
  }

  for (unsigned int i = 0; i < _drops.size(); ++i)
  {
    TypesDef::DropPtr drop = _drops.at(i);
    if (_player->getEntity()->getWorldBoundingBox().intersects(drop->getEntity()->getWorldBoundingBox()))
    {
      auto dropAmmo = std::dynamic_pointer_cast<AmmoDrop>(drop);
      auto dropHealth = std::dynamic_pointer_cast<HealthRestoreDrop>(drop);
      if (dropAmmo != nullptr)
      {
        _player->addAmmo(dropAmmo->getMagazineToRecover());
      } else if (dropHealth != nullptr)
      {
        _core->restoreHealth(GameConstants::healthToRecover);
      }

      _drops.erase(_drops.begin() + i);
    }
  }

  updateGUIWaveState();
}

void PlayState::changeToTacticalState ()
{
  _raySceneQuery = _sceneMgr->createRayQuery(Ogre::Ray());

  /* Stop Movement Player */
  _player->stopFront();
  _player->stopBack();
  _player->stopLeft();
  _player->stopRight();

  /* Stop Shooting */
  _player->stopShooting();

  /* Clear enemies vector */
  _enemies.clear();

  _currentState = GameEnums::StateGame::TacticalState;
  _typeSelectedTurret = GameEnums::TypeTurret::NoneType;
  _selectedTurret = nullptr;

  /* Show mouse cursor */
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().
      setPosition(CEGUI::Vector2f(_root->getAutoCreatedWindow()->getWidth()/2,
                                  _root->getAutoCreatedWindow()->getHeight()/2));
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();

  /* Hide GUI Wave State and show GUI Tactical State */
  //hideGUIWaveState();
  showGUITacticalState();

  _player->hidePlayer();

  _timeToNextRound = GameConstants::maxTimeBetweenRounds;

  /* Chage viewport to main camera */
  _root->getAutoCreatedWindow()->removeAllViewports();
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

  /* Set Position of camera and look at origen */
  _mainCameraNode->resetOrientation();

  _camera->lookAt(0, 0, 0);
}

void PlayState::changeToWaveState ()
{
  _rotateCamToRight = false;
  _rotateCamToLeft = false;

  _currentState = GameEnums::StateGame::WaveState;

  /* Hide mouse cursor */
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();

  /* Hide GUI Tactical State and show GUI Wave State */
  //hideGUITacticalState();
  showGUIWaveState();

  _player->showPlayer();

  /* Change viewport to player camera */
  _root->getAutoCreatedWindow()->removeAllViewports();
  _viewport = _root->getAutoCreatedWindow()->addViewport(_player->getCamera());

  /* Init round data */
  ++_round;
  _roundStarted = false;

  _timeToStartRound = GameConstants::timeToStartRound;
  _timeLastSpawn = GameConstants::timeBetweenSpawnMonsters;
  _enemiesToSpawn = (int) ((_round + 3)/2 + 6);
  _totalMonstersKilled = _totalMonstersKilled + _enemiesSpawned;
  _enemiesSpawned = 0;
  _enemiesDied = 0;
}

void PlayState::createGUITacticalState ()
{
  CEGUI::Window* tacticalGui = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("TacticalLayout.layout");
  tacticalGui -> setVisible(false);
  _sheet -> addChild(tacticalGui);

  CEGUI::Window* imgnamemachine = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameMachine");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameMachine","machine.png");
  imgnamemachine -> setProperty("Image","INameMachine");
  imgnamemachine -> setProperty("BackgroundEnabled","False");
  imgnamemachine -> setProperty("FrameEnabled","False");
  imgnamemachine -> setSize(CEGUI::USize(CEGUI::UDim(0.07f,0),CEGUI::UDim(0.07f,0)));
  imgnamemachine -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.70f,0),CEGUI::UDim(0.75f,0)));
  _sheet -> addChild(imgnamemachine);

  CEGUI::Window* imgnamecannon = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameCannon");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameCannon","cannon.png");
  imgnamecannon -> setProperty("Image","INameCannon");
  imgnamecannon -> setProperty("BackgroundEnabled","False");
  imgnamecannon -> setProperty("FrameEnabled","False");
  imgnamecannon -> setSize(CEGUI::USize(CEGUI::UDim(0.07f,0),CEGUI::UDim(0.07f,0)));
  imgnamecannon -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.82f,0),CEGUI::UDim(0.75f,0)));
  _sheet -> addChild(imgnamecannon);

  CEGUI::Window* cannonButton = tacticalGui->getChild("CannonButton");
  cannonButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
          CEGUI::Event::Subscriber(&PlayState::cannonButtonC, this));

  CEGUI::Window* machineButton = tacticalGui->getChild("MachineButton");
  machineButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
          CEGUI::Event::Subscriber(&PlayState::machineButtonC, this));

}

void PlayState::destroyGUITacticalState ()
{
  _sheet -> destroyChild("TacticalLayout");
  CEGUI::Renderer * renderer = IntroState::getSingletonPtr()->getRenderer();
  string namesImg[4] = {"INameCoins", "INamePoints", "INameMachine", "INameCannon" };
  for (int i = 0; i < std::end(namesImg) - std::begin(namesImg); i++) {
    _sheet -> destroyChild(namesImg[i]);
    CEGUI::ImageManager::getSingleton().destroy(namesImg[i]);
    renderer -> destroyTexture(namesImg[i]);
  }
}

void PlayState::showGUITacticalState ()
{
  _sheet -> getChild("TacticalLayout")-> setVisible(true);
  _sheet -> getChild("GameLayout")-> setVisible(false);
  string namesImg[6] = {"INameGun", "INameEnemy", "INameRounds", "INameCore", "INameAmmo", "INameRifle"};
  for (int i = 0; i < std::end(namesImg) - std::begin(namesImg); i++) {
    if(_sheet->getChild(namesImg[i])->isVisible()){
        _sheet->getChild(namesImg[i])->setVisible(false);
    }
  }
  _sheet->getChild("INameMachine")->setVisible(true);
  _sheet->getChild("INameCannon")->setVisible(true);
  _overlayManager->getByName("Apuntador") -> hide();
}

/* Only call for hide complet */
void PlayState::hideGUITacticalState ()
{
  _sheet -> getChild("TacticalLayout")-> setVisible(false);
  string namesImg[4] = {"INameCoins", "INamePoints", "INameMachine", "INameCannon" };
  for (int i = 0; i < std::end(namesImg) - std::begin(namesImg); i++) {
    _sheet->getChild(namesImg[i])->setVisible(false);
  }
  _overlayManager->getByName("Apuntador") -> hide();
}

void PlayState::updateGUITacticalState ()
{
  updateTacticalGui("Coins", (int) _player->getCoins());
  updateTacticalGui("Points", (int) _player-> getPuntuation());
  updateTime("InfoTime", _timeToNextRound);
  _sheet->getChild("INameRifle")->setVisible(false);
  _sheet->getChild("INameGun")->setVisible(false);
}

void PlayState::createGUIWaveState ()
{
  /* Show overlay */
  Ogre::Overlay *overlay = _overlayManager->getByName("Apuntador");
  overlay->show();

  /* Create GUI */
  CEGUI::Window* gameGui = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("GameLayout.layout");
  gameGui -> moveToFront();
  _sheet -> addChild(gameGui);

  gameGui->getChild("InfoCore")-> setVisible(false);

  CEGUI::Window* imgnameenemy = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameEnemy");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameEnemy","enemies.png");
  imgnameenemy -> setProperty("Image","INameEnemy");
  imgnameenemy -> setProperty("BackgroundEnabled","False");
  imgnameenemy -> setProperty("FrameEnabled","False");
  imgnameenemy -> setSize(CEGUI::USize(CEGUI::UDim(0.12f,0),CEGUI::UDim(0.10f,0)));
  imgnameenemy -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.01f,0),CEGUI::UDim(0.12f,0)));
  _sheet -> addChild(imgnameenemy);

  CEGUI::Window* imgnamerounds = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameRounds");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameRounds","rounds.png");
  imgnamerounds -> setProperty("Image","INameRounds");
  imgnamerounds -> setProperty("BackgroundEnabled","False");
  imgnamerounds -> setProperty("FrameEnabled","False");
  imgnamerounds -> setSize(CEGUI::USize(CEGUI::UDim(0.12f,0),CEGUI::UDim(0.10f,0)));
  imgnamerounds -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.01f,0),CEGUI::UDim(0.03f,0)));
  _sheet -> addChild(imgnamerounds);

  CEGUI::Window* imgnamepoints = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INamePoints");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INamePoints","points.png");
  imgnamepoints -> setProperty("Image","INamePoints");
  imgnamepoints -> setProperty("BackgroundEnabled","False");
  imgnamepoints -> setProperty("FrameEnabled","False");
  imgnamepoints -> setSize(CEGUI::USize(CEGUI::UDim(0.12f,0),CEGUI::UDim(0.10f,0)));
  imgnamepoints -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.01f,0),CEGUI::UDim(0.84f,0)));
  _sheet -> addChild(imgnamepoints);

  CEGUI::Window* imgnamecoins = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameCoins");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameCoins","coins.png");
  imgnamecoins -> setProperty("Image","INameCoins");
  imgnamecoins -> setProperty("BackgroundEnabled","False");
  imgnamecoins -> setProperty("FrameEnabled","False");
  imgnamecoins -> setSize(CEGUI::USize(CEGUI::UDim(0.12f,0),CEGUI::UDim(0.10f,0)));
  imgnamecoins -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.01f,0),CEGUI::UDim(0.72f,0)));
  _sheet -> addChild(imgnamecoins);

  CEGUI::Window* imgnamegun = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameGun");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameGun","arm.png");
  imgnamegun -> setProperty("Image","INameGun");
  imgnamegun -> setProperty("BackgroundEnabled","False");
  imgnamegun -> setProperty("FrameEnabled","False");
  imgnamegun -> setSize(CEGUI::USize(CEGUI::UDim(0.07f,0),CEGUI::UDim(0.07f,0)));
  imgnamegun -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.84f,0),CEGUI::UDim(0.70f,0)));
  _sheet -> addChild(imgnamegun);

  CEGUI::Window* imgnamerifle = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameRifle");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameRifle","rifle.png");
  imgnamerifle -> setProperty("Image","INameRifle");
  imgnamerifle -> setProperty("BackgroundEnabled","False");
  imgnamerifle -> setProperty("FrameEnabled","False");
  imgnamerifle -> setSize(CEGUI::USize(CEGUI::UDim(0.07f,0),CEGUI::UDim(0.07f,0)));
  imgnamerifle -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.85f,0),CEGUI::UDim(0.68f,0)));
  _sheet -> addChild(imgnamerifle);

  CEGUI::Window* imgnameammo = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameAmmo");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameAmmo","municion.png");
  imgnameammo -> setProperty("Image","INameAmmo");
  imgnameammo -> setProperty("BackgroundEnabled","False");
  imgnameammo -> setProperty("FrameEnabled","False");
  imgnameammo -> setSize(CEGUI::USize(CEGUI::UDim(0.07f,0),CEGUI::UDim(0.07f,0)));
  imgnameammo -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.89f,0),CEGUI::UDim(0.81f,0)));
  _sheet -> addChild(imgnameammo);

  CEGUI::Window* imgnamecore = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameCore");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameCore","nucleo.png");
  imgnamecore -> setProperty("Image","INameCore");
  imgnamecore -> setProperty("BackgroundEnabled","False");
  imgnamecore -> setProperty("FrameEnabled","False");
  imgnamecore -> setSize(CEGUI::USize(CEGUI::UDim(0.07f,0),CEGUI::UDim(0.07f,0)));
  imgnamecore -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.76f,0),CEGUI::UDim(0.03f,0)));
  _sheet -> addChild(imgnamecore);
}

void PlayState::destroyGUIWaveState ()
{
  _sheet -> destroyChild("GameLayout");
  CEGUI::Renderer * renderer = IntroState::getSingletonPtr()->getRenderer();
  string namesImg[6] = {"INameCore", "INameRounds", "INameAmmo", "INameEnemy", "INameRifle", "INameGun" };
  for (int i = 0; i < std::end(namesImg) - std::begin(namesImg); i++) {
    _sheet -> destroyChild(namesImg[i]);
    CEGUI::ImageManager::getSingleton().destroy(namesImg[i]);
    renderer -> destroyTexture(namesImg[i]);
  }
}

void PlayState::showGUIWaveState ()
{
  /* Show overlay */
  Ogre::Overlay *overlay = _overlayManager->getByName("Apuntador");
  overlay->show();

  _sheet -> getChild("TacticalLayout")-> setVisible(false);
  _sheet -> getChild("GameLayout")-> setVisible(true);
  string namesImg[4] = {"INameEnemy", "INameRounds", "INameCore", "INameAmmo"};
  for (int i = 0; i < std::end(namesImg) - std::begin(namesImg); i++) {
      _sheet->getChild(namesImg[i])->setVisible(true);
  }
  _sheet->getChild("INameMachine")->setVisible(false);
  _sheet->getChild("INameCannon")->setVisible(false);
  _overlayManager->getByName("Apuntador") -> show();
}

/* Only call for hide complet */
void PlayState::hideGUIWaveState ()
{
  /* Hide overlay */
  Ogre::Overlay* overlay = _overlayManager->getByName("Apuntador");
  overlay->hide();

  _sheet -> getChild("TacticalLayout")-> setVisible(true);
  _sheet -> getChild("GameLayout")-> setVisible(false);
  string namesImg[8] = {"INameGun", "INameEnemy", "INameRounds", "INameCore", "INameAmmo", "INameCoins", "INamePoints","INameRifle"};
  for (int i = 0; i < std::end(namesImg) - std::begin(namesImg); i++) {
      _sheet->getChild(namesImg[i])->setVisible(false);
  }
  _overlayManager->getByName("Apuntador") -> hide();
}

void PlayState::updateGUIWaveState ()
{
  TypesDef::WeaponPtr ActWeapon = _player->getCurrentWeapon();

  if (_player -> getIdCurrentWeapon() == GameConstants::numMainWeapon) {
    _sheet->getChild("INameRifle")->setVisible(true);
    _sheet->getChild("INameGun")->setVisible(false);
  } else if (_player -> getIdCurrentWeapon() == GameConstants::numSecondaryWeapon) {
    _sheet->getChild("INameRifle")->setVisible(false);
    _sheet->getChild("INameGun")->setVisible(true);
  }

  updateGameGui("TxAmmo", (int) ActWeapon->getAmmoMagazine());
  updateGameGui("TxTotalAmmo", (int) ActWeapon->getAmmoReserve());
  updateGameGui("Coins", (int) _player->getCoins());
  updateGameGui("Points", (int) _player-> getPuntuation());
  updateGameGui("Core", _core -> getHealth());
  updateGameGui("Round", _round);
  updateGameGui("Enemy", (int) (_enemiesToSpawn - _enemiesDied));
}

Ogre::Ray PlayState::setRayQuery (int posx, int posy, uint32_t mask)
{
  Ogre::Ray rayMouse = _camera->getCameraToViewportRay
            (posx/float(_root->getAutoCreatedWindow()->getWidth()),
             posy/float(_root->getAutoCreatedWindow()->getHeight()));

  _raySceneQuery->setRay(rayMouse);
  _raySceneQuery->setSortByDistance(true);
  _raySceneQuery->setQueryMask(mask);

  return rayMouse;
}

void PlayState::hitCore (unsigned int dmg)
{
  _core->hit(dmg);
}

bool PlayState::isAllEnemiesDead ()
{
  return (_enemiesSpawned == _enemiesToSpawn && _enemiesDied == _enemiesToSpawn);
}

void PlayState::createScene ()
{
  /* create SkyBox */
  _sceneMgr->setSkyBox(true, "MySky");

  /* Create Lights */
  _sceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);
  Ogre::Light* light = _sceneMgr->createLight("Light1");
  light->setType(Ogre::Light::LT_DIRECTIONAL);
  light->setDirection(Ogre::Vector3(-1, -1, 0));

  /* Create Map */
  createMap();
}

void PlayState::createMap ()
{
  _map = MapGenerator::getSingletonPtr()->generateMap();

  /* Creating map features: Walls, Portals & Core */
  int rows = _map->getRows();
  int columns = _map->getColumns();
  Cell** mapMatrix = _map->getMapMatrix();
  TypesDef::GraphPtr graph = _map->getMapGraph();

  unsigned int nWalls = 0;
  unsigned int nPortals = 0;

  for (int i = 0; i < rows; ++i)
  {
    for (int j = 0; j < columns; ++j)
    {
      if (mapMatrix[i][j].getType() == GameEnums::TypeCell::FreeCell)
      {
        Platform plat_aux(_map, i, j);
        _platforms.push_back(plat_aux);
      } else if (mapMatrix[i][j].getType() == GameEnums::TypeCell::CoreCell)
      {
        _core = std::make_shared<Core>(_map, i, j);
      }

      if (mapMatrix[i][j].getType() == GameEnums::TypeCell::SpawnCell)
      {
        Portal portal_aux(_map, i, j, nPortals);
        ++nPortals;
        _portals.push_back(portal_aux);
      } else
      {
        if ((i == 0) || (i == (rows - 1)))
        {

          Wall wall_aux(_map, i, j, nWalls, 0);
          ++nWalls;
          _walls.push_back(wall_aux);
        }

        if ((j == 0) || (j == (columns - 1)))
        {
          Wall wall_aux(_map, i, j, nWalls, 1);
          ++nWalls;
          _walls.push_back(wall_aux);
        }
      }
    }
  }
}

void PlayState::createEnemy ()
{
  int typeaux = rand() % (10);

  if (typeaux < 4)
  {
    _enemies.push_back(EnemyFactory::getSingletonPtr()->createEnemy(GameEnums::TypeEnemy::Enemy_Type_1, _map, _round, _enemiesSpawned));
  } else
  {
    _enemies.push_back(EnemyFactory::getSingletonPtr()->createEnemy(GameEnums::TypeEnemy::Enemy_Type_2, _map, _round, _enemiesSpawned));
  }
  _timeLastSpawn = GameConstants::timeBetweenSpawnMonsters;
  ++_enemiesSpawned;
}

void PlayState::updateGameGui(std::string nametext , int data){
  CEGUI::Window* element = _sheet -> getChild("GameLayout")-> getChild(nametext);
  std::ostringstream os;
  os << data;
  element -> setText(os.str());
}

void PlayState::updateTacticalGui(std::string nametext , int data){
  CEGUI::Window* element = _sheet -> getChild("TacticalLayout")-> getChild(nametext);
  std::ostringstream os;
  os << data;
  element -> setText(os.str());
}

void PlayState::updateTime(std::string nametext , Ogre::Real data){
  CEGUI::Window* element = _sheet -> getChild("TacticalLayout")-> getChild(nametext);
  std::ostringstream os;
  os << setprecision(3) << data;
  element -> setText(os.str());
}

bool PlayState::machineButtonC(const CEGUI::EventArgs &e){
  _typeSelectedTurret = GameEnums::TypeTurret::MachineGunTurret;
  return true;
}

bool PlayState::cannonButtonC(const CEGUI::EventArgs &e){
  _typeSelectedTurret = GameEnums::TypeTurret::CannonTurret;
  return true;
}
