#include "FinalState.h"

#include "MouseConverter.h"

template<> FinalState* Ogre::Singleton<FinalState>::msSingleton = 0;

void
FinalState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();
  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("MainCamera");
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

  _back = false;
  _mainmenu = false;
  _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible(true);

  createGUIFinalState();
  _exitGame = false;
}

void
FinalState::exit ()
{
    _sceneMgr -> clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    hideGUI();
    destroyGUI();
}

void
FinalState::pause ()
{
}

void
FinalState::resume ()
{
}

bool
FinalState::frameStarted
(const Ogre::FrameEvent& evt)
{
  if (_back) {
      _exitGame = true;
  }
  if (_mainmenu)
  {
    changeState(MenuState::getSingletonPtr());
  }
  return true;
}

bool
FinalState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
FinalState::keyPressed
(const OIS::KeyEvent &e) {
  // Tecla p --> Estado anterior.
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
  CEGUI::System::getSingleton().getDefaultGUIContext().injectChar(e.text);
}

void
FinalState::keyReleased
(const OIS::KeyEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectKeyDown(static_cast<CEGUI::Key::Scan>(e.key));
}

void
FinalState::mouseMoved
(const OIS::MouseEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void
FinalState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

void
FinalState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

FinalState*
FinalState::getSingletonPtr ()
{
return msSingleton;
}

FinalState&
FinalState::getSingleton ()
{
  assert(msSingleton);
  return *msSingleton;
}

void FinalState::createGUIFinalState ()
{
  finalGui = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("FinalStateLayout.layout");
  finalGui -> moveToFront();
  _sheet -> addChild(finalGui);
  finalGui -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.20f,0),CEGUI::UDim(0.10f,0)));

  CEGUI::Window* exitButton = finalGui->getChild("ExitButton");
  exitButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
      CEGUI::Event::Subscriber(&FinalState::exitB, this));

  CEGUI::Window* mainButton = finalGui->getChild("MainMenuButton");
  mainButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
      CEGUI::Event::Subscriber(&FinalState::mainB, this));

  CEGUI::Window* imgnameend = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/StaticImage","INameEnd");
  CEGUI::ImageManager::getSingleton().addFromImageFile("INameEnd","endOfmission.png");
  imgnameend -> setProperty("Image","INameEnd");
  imgnameend -> setProperty("BackgroundEnabled","False");
  imgnameend -> setProperty("FrameEnabled","False");
  imgnameend -> setSize(CEGUI::USize(CEGUI::UDim(0.12f,0),CEGUI::UDim(0.07f,0)));
  imgnameend -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.39f,0),CEGUI::UDim(0.20f,0)));
  _sheet -> addChild(imgnameend);

  std::ostringstream es;
  es << _round;
  finalGui ->getChild("RoundsIn")->setText(es.str());
  es.clear();es.str("");
  es << _points;
  finalGui -> getChild("PointsIn") -> setText(es.str());

  _rankPos = new RankingPosition(atoi(finalGui->getChild("RoundsIn")->getText().c_str()) , atoi(finalGui->getChild("PointsIn")->getText().c_str()),  finalGui->getChild("EditName")->getText().c_str() );
}


void FinalState::destroyGUI(){
  _sheet -> destroyChild("FinalLayout");
  CEGUI::Renderer * renderer = IntroState::getSingletonPtr()->getRenderer();
  CEGUI::ImageManager::getSingleton().destroy("INameEnd");
  renderer -> destroyTexture("INameEnd");

  _sheet->destroyChild("INameEnd");
}

void FinalState::hideGUI(){
  _sheet -> getChild("INameEnd")->setVisible(false);
}

bool FinalState::exitB(const CEGUI::EventArgs &e){
  _rankPos -> setName(finalGui->getChild("EditName")->getText().c_str());
  RankingManager::getSingletonPtr()->saveResult(_rankPos);
  _back = true;
  return true;
}

bool FinalState::mainB(const CEGUI::EventArgs &e){
  _rankPos -> setName(finalGui->getChild("EditName")->getText().c_str());
  RankingManager::getSingletonPtr()->saveResult(_rankPos);
  _mainmenu = true;
  return true;
}

void FinalState::updateInfo(unsigned int points, unsigned int round){
    _points = points;
    _round = round;
}
