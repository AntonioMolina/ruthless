#include "RankingState.h"

#include "RankingManager.h"
#include "MouseConverter.h"

template<> RankingState* Singleton<RankingState>::msSingleton = 0;

RankingState::RankingState () : _sheet(nullptr), _root(nullptr),
        _sceneMgr(nullptr), _camera(nullptr), _back(false) {
}

void RankingState::enter () {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _back = false;
    createGUI();
    createBackground();
}

void RankingState::exit () {
    _root->getAutoCreatedWindow()->removeAllViewports();
    deleteGUI();
    deleteBackground ();
}

void RankingState::pause () {}

void RankingState::resume () {}

void RankingState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
        case OIS::KC_R:
            _back = true;
            break;
        default:
            break;
    }
}

void RankingState::keyReleased (const OIS::KeyEvent &e) {}

void RankingState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void RankingState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonDown(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

void RankingState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonUp(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

bool RankingState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool RankingState::frameEnded (const FrameEvent& evt) {
    if (_back) {
        popState();
    }

    return true;
}

RankingState& RankingState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

RankingState* RankingState::getSingletonPtr () {
    return msSingleton;
}

void RankingState::createGUI () {

  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Credits");

  /* Back Menu button */

  CEGUI::Window* backButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "BackButton");

    backButton->setText("Volver");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&RankingState::back, this));

    stringstream rcdsString;
    rcdsString.str("");

    rcdsString << "[colour='FFD7D7D7'][font='DejaVuSans-25']Position      Points\n";

    std::vector<RankingPosition> vector= RankingManager::getSingletonPtr()->createStringRanking();

    stringstream names;
    stringstream points;
    stringstream rounds;

    names.str("");
    names<< "[colour='FFD7D7D7'][font='DejaVuSans-25']NAMES\n";

    points.str("");
    points<< "[colour='FFD7D7D7'][font='DejaVuSans-25']POINTS\n";

    rounds.str("");
    rounds<< "[colour='FFD7D7D7'][font='DejaVuSans-25']ROUNDS\n";

    for(unsigned int i=0;i<vector.size();i++){
      names << vector.at(i).getName() << " \n";
      points << vector.at(i).getPoints() << " \n";
      rounds <<  vector.at(i).getRound() << " \n";
    }

    CEGUI::Window* recordsLabeln = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "RecordsLabeln");
    recordsLabeln->setText(names.str());//rcdsString.str());
    recordsLabeln->setSize(CEGUI::USize(CEGUI::UDim(0.45,0),CEGUI::UDim(0.45,0)));
    recordsLabeln->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.85/2,0),CEGUI::UDim(0.15,0)));


    CEGUI::Window* recordsLabelp = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "RecordsLabelp");
    recordsLabelp->setText(points.str());//rcdsString.str());
    recordsLabelp->setSize(CEGUI::USize(CEGUI::UDim(0.45,0),CEGUI::UDim(0.45,0)));
    recordsLabelp->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.05/2,0),CEGUI::UDim(0.15,0)));
    //recordsLabel->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.45/2,0),CEGUI::UDim(0.15,0)));

    CEGUI::Window* recordsLabelr = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Label", "RecordsLabelr");
    recordsLabelr->setText(rounds.str());//rcdsString.str());
    recordsLabelr->setSize(CEGUI::USize(CEGUI::UDim(0.45,0),CEGUI::UDim(0.45,0)));
    recordsLabelr->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.45/2,0),CEGUI::UDim(0.15,0)));

    _sheet->addChild(recordsLabeln);
    _sheet->addChild(recordsLabelp);
    _sheet->addChild(recordsLabelr);

    _sheet->addChild(backButton);

    CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);

}

void RankingState::deleteGUI () {
  _sheet->destroyChild("BackButton");
  _sheet->destroyChild("RecordsLabeln");
  _sheet->destroyChild("RecordsLabelp");
  _sheet->destroyChild("RecordsLabelr");

  CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->destroyChild(_sheet);
}

void RankingState::createBackground () {
  MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
  material->getTechnique(0)->getPass(0)->createTextureUnitState("backgroundMenu.png");
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

  Rectangle2D* rect = new Ogre::Rectangle2D(true);
  rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  rect->setMaterial("Background");

  rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);

  AxisAlignedBox aabInf;
  aabInf.setInfinite();
  rect->setBoundingBox(aabInf);

  SceneNode* node = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundMenu");
  node->attachObject(rect);
}

void RankingState::deleteBackground () {
  _sceneMgr->clearScene();
}

bool RankingState::back (const CEGUI::EventArgs &e) {
    _back = true;
    return true;
}
