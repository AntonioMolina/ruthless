#include "PauseState.h"

#include "MouseConverter.h"

template<> PauseState* Ogre::Singleton<PauseState>::msSingleton = 0;

void
PauseState::enter ()
{
  _root = Ogre::Root::getSingletonPtr();

  // Se recupera el gestor de escena y la cámara.
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("MainCamera");
  _viewport = _root->getAutoCreatedWindow()->getViewport(0);
  // Nuevo background colour.
  _viewport->setBackgroundColour(Ogre::ColourValue(0.0, 1.0, 0.0));

  _back = false;
  _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible(true);

  createGUIPauseState();
  _exitGame = false;
}

void
PauseState::exit ()
{
    destroyGUI();
    CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible(false);
}

void
PauseState::pause ()
{
}

void
PauseState::resume ()
{
}

bool
PauseState::frameStarted
(const Ogre::FrameEvent& evt)
{
  if (_back) {
    popState();
  }
  return true;
}

bool
PauseState::frameEnded
(const Ogre::FrameEvent& evt)
{
  if (_exitGame)
    return false;

  return true;
}

void
PauseState::keyPressed
(const OIS::KeyEvent &e) {
}

void
PauseState::keyReleased
(const OIS::KeyEvent &e)
{
  // Tecla p --> Estado anterior.
  if (e.key == OIS::KC_ESCAPE || e.key == OIS::KC_P) {
    popState();
  }
}

void
PauseState::mouseMoved
(const OIS::MouseEvent &e)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void
PauseState::mousePressed
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonDown(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

void
PauseState::mouseReleased
(const OIS::MouseEvent &e, OIS::MouseButtonID id)
{
  CEGUI::System::getSingleton().getDefaultGUIContext().injectMouseButtonUp(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

PauseState*
PauseState::getSingletonPtr ()
{
return msSingleton;
}

PauseState&
PauseState::getSingleton ()
{
  assert(msSingleton);
  return *msSingleton;
}

void PauseState::createGUIPauseState ()
{
  CEGUI::Window* pauseGui = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("PauseLayout.layout");
  pauseGui -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.17f,0),CEGUI::UDim(0.07f,0)));
  pauseGui -> moveToFront();
  _sheet -> addChild(pauseGui);

  CEGUI::Window* resumeButton = pauseGui->getChild("Resume");
  resumeButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
      CEGUI::Event::Subscriber(&PauseState::resume, this));
}

bool PauseState::resume(const CEGUI::EventArgs &e){
    _back = true;
    return true;
}

void PauseState::destroyGUI(){
    _sheet -> destroyChild("PauseLayout");
}
