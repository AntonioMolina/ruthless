#include "ControlsState.h"

#include <vector>

#include "MouseConverter.h"

template<> ControlsState* Singleton<ControlsState>::msSingleton = 0;

int indice= 1;

ControlsState::ControlsState () : _sheet(nullptr), _root(nullptr),
        _sceneMgr(nullptr), _camera(nullptr), _back(false) {
}

void ControlsState::enter () {
    _root = Root::getSingletonPtr();
    _sceneMgr = _root->getSceneManager("SceneManager");
    _camera = _sceneMgr->getCamera("MainCamera");

    _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);

    _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));
    std::cout << "ControlsState" << std::endl;

    _back = false;
    createGUI();
    createBackground ();
  //  createOverlay();
}

void ControlsState::exit () {
    _sceneMgr->clearScene();
    _root->getAutoCreatedWindow()->removeAllViewports();
    deleteGUI();
    deleteBackground ();
    //destroyOverlay ();

}

void ControlsState::pause () {}

void ControlsState::resume () {}

void ControlsState::keyPressed (const OIS::KeyEvent &e) {
    switch (e.key) {
        case OIS::KC_ESCAPE:
        case OIS::KC_H:
            _back = true;
            break;
        default:
            break;
    }
}

void ControlsState::keyReleased (const OIS::KeyEvent &e) {}

void ControlsState::mouseMoved (const OIS::MouseEvent &e) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void ControlsState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonDown(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

void ControlsState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
    CEGUI::System::getSingleton().getDefaultGUIContext()
            .injectMouseButtonUp(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

bool ControlsState::frameStarted (const FrameEvent& evt) {
    return true;
}

bool ControlsState::frameEnded (const FrameEvent& evt) {
    if (_back) {
        popState();
    }

    return true;
}

ControlsState& ControlsState::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}

ControlsState* ControlsState::getSingletonPtr () {
    return msSingleton;
}

void ControlsState::createGUI () {
  _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

  /* Back Menu button */

  CEGUI::Window* MenuControls = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("ControlsLayout.layout");

  CEGUI::Window* mainWindow = MenuControls->getChild("FrameWindow2");

  CEGUI::Window* ButtonBack = mainWindow->getChild("ButtonBack");
  ButtonBack->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ControlsState::back, this));

  //_sheet -> addChild(MenuControls);

  CEGUI::Window* buttonleft = mainWindow->getChild("PreviousButton");
  buttonleft->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ControlsState::previousImage, this));


  CEGUI::Window* buttonright = mainWindow->getChild("NextButton");
  buttonright->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ControlsState::nextImage, this));
    /*backButton->setText("Back");
    backButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
    backButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.85,0)));
    backButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&ControlsState::back, this));

    backButton->moveToFront();*/

    CEGUI::Window* staticImage1 = mainWindow->getChild("StaticImage1");
    CEGUI::ImageManager::getSingleton().addFromImageFile("Core","cntCore.png");
    staticImage1 -> setProperty("Image","Core");


    CEGUI::Window* staticImage2 = mainWindow->getChild("StaticImage2");
    CEGUI::ImageManager::getSingleton().addFromImageFile("ASDW","cntKeyboard.png");
    staticImage2 -> setProperty("Image","ASDW");
    staticImage2->hide();

    CEGUI::Window* staticImage3 = mainWindow->getChild("StaticImage3");
    CEGUI::ImageManager::getSingleton().addFromImageFile("controlsIm","cntMouse.png");
    staticImage3 -> setProperty("Image","controlsIm");
    staticImage3->hide();

    CEGUI::Window* staticImage4 = mainWindow->getChild("StaticImage4");
    CEGUI::ImageManager::getSingleton().addFromImageFile("TurretsIm","cntTurrets.png");
    staticImage4 -> setProperty("Image","TurretsIm");
    staticImage4->hide();

  //  staticImage->hide();


    //CEGUI::Window* ex1 = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("render.layout");
    //CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(ex1);
    //CEGUI::Window* frameWindow = ex1->getChild("RTTWindow");
    //frameWindow->setPosition(CEGUI::UVector2(CEGUI::UDim(0.2,0),CEGUI::UDim(0,0)));




    _sheet -> addChild(mainWindow);
    //_sheet->addChild(staticImage);

    //CEGUI::Window* RTTWindow = ex1->getChild("RTTWindow");
    //RTTWindow->setProperty("Image","RTTImage");

    //_sheet->addChild(ex1);
    //ex1->show();
    //_sheet->addChild(backButton);

    //CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow(_sheet);

}
void ControlsState::createBackground () {
  MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
  material->getTechnique(0)->getPass(0)->createTextureUnitState("backgroundMenu.png");
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

  Rectangle2D* rect = new Ogre::Rectangle2D(true);
  rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  rect->setMaterial("Background");

  rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);

  AxisAlignedBox aabInf;
  aabInf.setInfinite();
  rect->setBoundingBox(aabInf);

  SceneNode* node = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundMenu");
  node->attachObject(rect);
}

void ControlsState::createOverlay() {

  _overlayManager = Ogre::OverlayManager::getSingletonPtr();

  Ogre::Overlay *overlay = _overlayManager->getByName("ASDW");

  overlay->show();


  Ogre::Entity *ent_player = _sceneMgr->createEntity("windows", "windows.mesh");
  Ogre::SceneNode *node_player = _sceneMgr->createSceneNode("windowsNode");
  _sceneMgr->getRootSceneNode()->addChild(node_player);
  node_player->attachObject(ent_player);
  node_player->setPosition(Ogre::Vector3(3.5,-4,0));
  node_player->scale(Ogre::Vector3(1,1,1));
  node_player->roll(Ogre::Degree(180));
  node_player->pitch(Ogre::Degree(180));
  node_player->yaw(Ogre::Degree(90));
  node_player->translate(Ogre::Vector3(0,1.5,0));
  ent_player->setMaterialName("Materialwindows");

}
void ControlsState::destroyOverlay () {
    Overlay *overlay = _overlayManager->getByName("ASDW");
    overlay->hide();
}

void ControlsState::deleteBackground () {
  _sceneMgr->clearScene();
}

void ControlsState::deleteGUI () {

  _sheet->destroyChild("FrameWindow2");
  CEGUI::Renderer * renderer = IntroState::getSingletonPtr()->getRenderer();
  std::string namesImg[4] = {"Core", "ASDW", "controlsIm", "TurretsIm"};
  for (int i = 0; i < std::end(namesImg) - std::begin(namesImg); i++) {
        CEGUI::ImageManager::getSingleton().destroy(namesImg[i]);
        renderer -> destroyTexture(namesImg[i]);
  }

}


bool ControlsState::back (const CEGUI::EventArgs &e) {
    _back = true;
    return true;
}

bool ControlsState::previousImage (const CEGUI::EventArgs &e){


  switch ( indice ){
           case 1:
              indice= 4;
              _sheet->getChild("FrameWindow2")->getChild("StaticImage1")->hide();
              _sheet->getChild("FrameWindow2")->getChild("StaticImage4")->show();
              break;
           case 2:
             indice=1;
             _sheet->getChild("FrameWindow2")->getChild("StaticImage2")->hide();
             _sheet->getChild("FrameWindow2")->getChild("StaticImage1")->show();
              break;

              case 3:
              indice=2;
              _sheet->getChild("FrameWindow2")->getChild("StaticImage3")->hide();
              _sheet->getChild("FrameWindow2")->getChild("StaticImage2")->show();
              break;

              case 4:
              indice=3;
              _sheet->getChild("FrameWindow2")->getChild("StaticImage4")->hide();
              _sheet->getChild("FrameWindow2")->getChild("StaticImage3")->show();
              break;

           default:

           break;

        }
  //CEGUI::Window* MenuContro = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("MenuIniRuthless.layout");
  //_sheet -> addChild(MenuContro);
  //_sheet->getChild("Core")->setVisible(false);
    //_sheet->getChild()->setVisible(true);

  //  _sheet->getChild("FrameWindow2")->getChild("StaticImage1")->hide();
  return true;
}

bool ControlsState::nextImage (const CEGUI::EventArgs &e){
  switch ( indice ){
    case 1:
      indice= 2;
      _sheet->getChild("FrameWindow2")->getChild("StaticImage1")->hide();
      _sheet->getChild("FrameWindow2")->getChild("StaticImage2")->show();
      break;
    case 2:
      indice=3;
      _sheet->getChild("FrameWindow2")->getChild("StaticImage2")->hide();
      _sheet->getChild("FrameWindow2")->getChild("StaticImage3")->show();
      break;
    case 3:
      indice=4;
      _sheet->getChild("FrameWindow2")->getChild("StaticImage3")->hide();
      _sheet->getChild("FrameWindow2")->getChild("StaticImage4")->show();
      break;
    case 4:
      indice=1;
      _sheet->getChild("FrameWindow2")->getChild("StaticImage4")->hide();
      _sheet->getChild("FrameWindow2")->getChild("StaticImage1")->show();
      break;
    default:
       break;
  }

  return true;
}
