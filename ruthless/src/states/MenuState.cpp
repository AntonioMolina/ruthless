#include "MenuState.h"

#include "MouseConverter.h"

#include "CreditsState.h"
#include "PlayState.h"
#include "RankingState.h"
#include "ControlsState.h"

template<> MenuState* Ogre::Singleton<MenuState>::msSingleton = 0;

MenuState::MenuState () : _sheet(nullptr),
                          _root(nullptr),
                          _sceneMgr(nullptr),
                          _camera(nullptr),
                          _exit(false)
{
  _pTrackManager = TrackManager::getSingletonPtr();
  _pSoundFXManager = SoundFXManager::getSingletonPtr();
}

void MenuState::enter () {
  _root = Ogre::Root::getSingletonPtr();
  _sceneMgr = _root->getSceneManager("SceneManager");
  _camera = _sceneMgr->getCamera("MainCamera");

  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible(false);

CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setVisible(true);
  _exit = false;

  createBackground();
  createGUI();

  _pTrackMenu = _pTrackManager->load("Music/MenuMusic.mp3");
  _pTrackMenu->play();
}

void MenuState::exit () {
  _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();

  deleteGUI();
  deleteBackground();

  _pTrackMenu->stop();
}

void MenuState::pause () {
    _sceneMgr->clearScene();
  _root->getAutoCreatedWindow()->removeAllViewports();

  deleteGUI();
  deleteBackground();
}

void MenuState::resume () {
  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  _exit = false;
  createGUI();
  createBackground();
}

void MenuState::keyPressed (const OIS::KeyEvent &e) {
  switch (e.key) {
    case OIS::KC_SPACE:
      changeState(PlayState::getSingletonPtr());
      break;
    case OIS::KC_C:
      pushState(CreditsState::getSingletonPtr());
      break;
    case OIS::KC_R:
      pushState(RankingState::getSingletonPtr());
      break;
    case OIS::KC_H:
      pushState(ControlsState::getSingletonPtr());
      break;
    case OIS::KC_ESCAPE:
      _exit = true;
      break;
    default:
      break;
  }
}

void MenuState::keyReleased (const OIS::KeyEvent &e) {}

void MenuState::mouseMoved (const OIS::MouseEvent &e) {
  CEGUI::System::getSingleton().getDefaultGUIContext()
      .injectMouseMove(e.state.X.rel, e.state.Y.rel);
}

void MenuState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
  CEGUI::System::getSingleton().getDefaultGUIContext()
      .injectMouseButtonDown(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

void MenuState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
  CEGUI::System::getSingleton().getDefaultGUIContext()
        .injectMouseButtonUp(MouseConverter::getSingletonPtr()->convertMouseButton(id));
}

bool MenuState::frameStarted (const Ogre::FrameEvent& evt) {
  return true;
}

bool MenuState::frameEnded (const Ogre::FrameEvent& evt) {
  return !_exit;
}

MenuState& MenuState::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

MenuState* MenuState::getSingletonPtr () {
  return msSingleton;
}

void MenuState::createBackground () {
  MaterialPtr material = Ogre::MaterialManager::getSingleton().create("Background", "General");
  material->getTechnique(0)->getPass(0)->createTextureUnitState("backgroundMenu.jpg");
  material->getTechnique(0)->getPass(0)->setDepthCheckEnabled(false);
  material->getTechnique(0)->getPass(0)->setDepthWriteEnabled(false);
  material->getTechnique(0)->getPass(0)->setLightingEnabled(false);

  Rectangle2D* rect = new Ogre::Rectangle2D(true);
  rect->setCorners(-1.0, 1.0, 1.0, -1.0);
  rect->setMaterial("Background");

  rect->setRenderQueueGroup(RENDER_QUEUE_BACKGROUND);

  AxisAlignedBox aabInf;
  aabInf.setInfinite();
  rect->setBoundingBox(aabInf);

  SceneNode* node = _sceneMgr->getRootSceneNode()->createChildSceneNode("BackgroundMenu");
  node->attachObject(rect);
}

void MenuState::deleteBackground () {
  _sceneMgr->clearScene();
}

void MenuState::createGUI () {
  //_sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow", "Menu");
  _sheet = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

  CEGUI::Window* MenuIni = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("MenuIniRuthless.layout");

  MenuIni -> setPosition(CEGUI::UVector2(CEGUI::UDim(0.22f,0),CEGUI::UDim(0.18f,0)));
  _sheet -> addChild(MenuIni);

  /*CEGUI::Window* playButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "PlayButton");
  playButton->setText("Play");
  playButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  playButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.5,0)));
  playButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::start, this));
  */

  CEGUI::Window* playButton = MenuIni->getChild("PlayB");
    playButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
          CEGUI::Event::Subscriber(&MenuState::start, this));

  /*CEGUI::Window* rankingButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "RankingButton");
  rankingButton->setText("Ranking");
  rankingButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  rankingButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.6,0)));
  rankingButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::ranking, this));
  */

  CEGUI::Window* creditsButton = MenuIni->getChild("CreditsB");
    creditsButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
          CEGUI::Event::Subscriber(&MenuState::credits, this));

  CEGUI::Window* controlsButton = MenuIni->getChild("ControlsB");
    controlsButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
                  CEGUI::Event::Subscriber(&MenuState::controls, this));

  /*CEGUI::Window* creditsButton = CEGUI::WindowManager::getSingleton().createWindow("TaharezLook/Button", "CreditsButton");
  creditsButton->setText("Credits");
  creditsButton->setSize(CEGUI::USize(CEGUI::UDim(0.15,0),CEGUI::UDim(0.05,0)));
  creditsButton->setPosition(CEGUI::UVector2(CEGUI::UDim(0.5-0.15/2,0),CEGUI::UDim(0.7,0)));
  creditsButton->subscribeEvent(CEGUI::PushButton::EventClicked, CEGUI::Event::Subscriber(&MenuState::credits, this));
  */

  CEGUI::Window* rankingButton = MenuIni->getChild("RankingB");
    rankingButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
          CEGUI::Event::Subscriber(&MenuState::ranking , this));

  CEGUI::Window* exitButton = MenuIni->getChild("ExitB");
    exitButton -> subscribeEvent(CEGUI::PushButton::EventClicked,
                  CEGUI::Event::Subscriber(&MenuState::quit , this));

  //_sheet->addChild(playButton);
  //_sheet->addChild(rankingButton);
  //_sheet->addChild(creditsButton);

  //CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->addChild(_sheet);
}

void MenuState::deleteGUI () {
  //_sheet->destroyChild("PlayButton");
  //_sheet->destroyChild("RankingButton");
  //_sheet->destroyChild("CreditsButton");
  _sheet->destroyChild("MenuIniRuthless");


  //CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->destroyChild(_sheet);
}

bool MenuState::start (const CEGUI::EventArgs &e) {
  changeState(PlayState::getSingletonPtr());
  return true;
}

bool MenuState::quit (const CEGUI::EventArgs &e) {
  _exit = true;
  return true;
}

bool MenuState::credits (const CEGUI::EventArgs &e) {
  pushState(CreditsState::getSingletonPtr());
  return true;
}

bool MenuState::ranking (const CEGUI::EventArgs &e) {
  pushState(RankingState::getSingletonPtr());
  return true;
}

bool MenuState::controls (const CEGUI::EventArgs &e) {
  pushState(ControlsState::getSingletonPtr());
  return true;
}
