#include "IntroState.h"

#include "MenuState.h"

template<> IntroState* Ogre::Singleton<IntroState>::msSingleton = 0;

IntroState::IntroState () : _root(nullptr), _sceneMgr(nullptr), _camera(nullptr) {
}

void IntroState::enter () {
  _root = Ogre::Root::getSingletonPtr();

  _sceneMgr = _root->getSceneManager("SceneManager");
  _sceneMgr->setAmbientLight(Ogre::ColourValue(1, 1, 1));

  _camera = _sceneMgr->createCamera("MainCamera");
  _camera->setNearClipDistance(0.1);
  _camera->setFarClipDistance(1000);

  _camera->setPosition(0.0f, 30.0f, -25.0f);
  _camera->lookAt(0.0f, 0.0f, 0.0f);

  _viewport = _root->getAutoCreatedWindow()->addViewport(_camera);
  _viewport->setBackgroundColour(Ogre::ColourValue(1.0, 1.0, 1.0));

  setCEGUI();
}

void IntroState::exit() {
  _root->getAutoCreatedWindow()->removeAllViewports();
}

void IntroState::pause () {}

void IntroState::resume () {}

bool IntroState::frameStarted (const Ogre::FrameEvent& evt) {
  changeState(MenuState::getSingletonPtr());
  return true;
}

bool IntroState::frameEnded (const Ogre::FrameEvent& evt) {
  return false;
}

void IntroState::keyPressed (const OIS::KeyEvent &e) {}

void IntroState::keyReleased (const OIS::KeyEvent &e ) {}

void IntroState::mouseMoved (const OIS::MouseEvent &e) {}

void IntroState::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

void IntroState::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {}

IntroState* IntroState::getSingletonPtr () {
  return msSingleton;
}

IntroState& IntroState::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

void IntroState::setCEGUI () {
  //CEGUI
  renderer = &CEGUI::OgreRenderer::bootstrapSystem();
  CEGUI::Scheme::setDefaultResourceGroup("Schemes");
  CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
  CEGUI::Font::setDefaultResourceGroup("Fonts");
  CEGUI::WindowManager::setDefaultResourceGroup("Layouts");
  CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");

  //Para el Raton
  CEGUI::SchemeManager::getSingleton().createFromFile("OgreTray.scheme");
  CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
 // CEGUI::SchemeManager::getSingleton().createFromFile("Vanilla.scheme");

  CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-12");
  CEGUI::FontManager::getSingleton().createAll("*.font", "Fonts");
  //CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-40");
  CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().setDefaultImage("OgreTrayImages/MouseArrow");

  _sheet = CEGUI::WindowManager::getSingleton().createWindow("DefaultWindow","Ruthless/Sheet");
  CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(_sheet);


}

CEGUI::Renderer * IntroState::getRenderer(){
  return renderer;
}