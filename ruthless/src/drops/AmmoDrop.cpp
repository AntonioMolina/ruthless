#include "AmmoDrop.h"

#include "Drop.h"

AmmoDrop::AmmoDrop (Ogre::Vector3 position, unsigned int id) : Drop(),
    _magazineToRecover(GameConstants::magazinesToRecover)
{

  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");
  Ogre::SceneNode* nodeTerrain = sceneMgr->getSceneNode("TF");

  std::stringstream entName;
  entName.str("");
  entName << "drop_" << id;
  _entDrop = sceneMgr->createEntity(entName.str(), "Municion.mesh");
  _nodeDrop = sceneMgr->createSceneNode(entName.str());
  _nodeDrop->attachObject(_entDrop);
  nodeTerrain->addChild(_nodeDrop);
  #ifdef _DEBUG
  _nodeDrop->showBoundingBox(true);
  #endif

  _position = position;
  _nodeDrop->setPosition(position);

  /* Init animations */
  Ogre::AnimationState* animStateJump = _entDrop->getAnimationState("SaltoInicial");
  animStateJump->setEnabled(true);
  animStateJump->setLoop(false);
  animStateJump->setTimePosition(0.0);

  Ogre::AnimationState* animStateRot = _entDrop->getAnimationState("Rotacion");
  animStateRot->setEnabled(false);
  animStateRot->setLoop(true);
  animStateRot->setTimePosition(0.0);

  _firstAnimEnded = false;
}

AmmoDrop::~AmmoDrop ()
{
  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingleton().getSceneManager("SceneManager");

  _nodeDrop->detachObject(_entDrop);
  sceneMgr->destroyEntity(_entDrop);

  _nodeDrop->getParentSceneNode()->removeChild(_nodeDrop->getName());
  sceneMgr->destroySceneNode(_nodeDrop);
}

void AmmoDrop::update(Ogre::Real deltaT)
{
  if (!_firstAnimEnded)
  {
    Ogre::AnimationState* animStateJump = _entDrop->getAnimationState("SaltoInicial");
    if (!animStateJump->hasEnded())
    {
      animStateJump->addTime(deltaT);
    } else
    {
      Ogre::AnimationState* animStateRot = _entDrop->getAnimationState("Rotacion");
      animStateRot->setEnabled(true);
      animStateRot->setLoop(true);
      animStateRot->setTimePosition(0.0);

      _firstAnimEnded = true;
    }
  } else
  {
    Ogre::AnimationState* animStateRot = _entDrop->getAnimationState("Rotacion");
    if (animStateRot->hasEnded())
    {
      animStateRot->setEnabled(true);
      animStateRot->setTimePosition(0.0);
    } else
    {
      animStateRot->addTime(deltaT);
    }
  }
}
