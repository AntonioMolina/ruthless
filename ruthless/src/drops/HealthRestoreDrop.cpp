#include "HealthRestoreDrop.h"

#include "Drop.h"

HealthRestoreDrop::HealthRestoreDrop (Ogre::Vector3 position, unsigned int id) :
  Drop(), _healthToRecover(GameConstants::healthToRecover)
{

  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");
  Ogre::SceneNode* nodeTerrain = sceneMgr->getSceneNode("TF");

  std::stringstream entName;
  entName.str("");
  entName << "drop_" << id;
  _entDrop = sceneMgr->createEntity(entName.str(), "muros.mesh");
  _nodeDrop = sceneMgr->createSceneNode(entName.str());
  _nodeDrop->attachObject(_entDrop);
  nodeTerrain->addChild(_nodeDrop);
  #ifdef _DEBUG
  _nodeDrop->showBoundingBox(true);
  #endif

  _position = position;
  _nodeDrop->setPosition(position);
}

HealthRestoreDrop::~HealthRestoreDrop ()
{
  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingleton().getSceneManager("SceneManager");

  _nodeDrop->detachObject(_entDrop);
  sceneMgr->destroyEntity(_entDrop);

  _nodeDrop->getParentSceneNode()->removeChild(_nodeDrop->getName());
  sceneMgr->destroySceneNode(_nodeDrop);
}

void HealthRestoreDrop::update(Ogre::Real deltaT)
{

}
