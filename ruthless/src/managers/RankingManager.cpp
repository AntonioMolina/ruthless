#include "RankingManager.h"
#include "RankingPosition.h"
#include <vector>

template<> RankingManager* Singleton<RankingManager>::msSingleton = 0;

RankingManager::RankingManager () {
    createRankingFile();
}
bool sort_by_rounds(RankingPosition a1, RankingPosition a2) {
  return a1.getRound() > a2.getRound();
}

bool RankingManager::saveResult (RankingPosition *puntuation) {
    string line;
    fstream rankingFile;

    RankingPosition* rp;

    std::vector<RankingPosition> ranking;
    ranking.reserve(5);

    rankingFile.open("records.rcd", ios::in | ios::out | ios::binary);

    if(rankingFile.good()){
      while(getline(rankingFile, line)){
        if(line.size()>0){
          try{
            string name= line.c_str();
            getline(rankingFile,line);
            int rounds = atoi(line.c_str());
            getline(rankingFile,line);
            int points= atoi(line.c_str());

            rp= new RankingPosition(rounds,points,name);
            ranking.push_back(*rp);

            //cout << "PRUEBA 2 " << name << rounds << points << endl;

          }catch(...){
              cout << "No se ha podido leer el dato. Error" << endl;
          }
        }
      }

    }

    rankingFile.close();

    unsigned int roundBack= ranking.at(4).getRound();

    //cout<< "TOP DEL RANKING "<< roundBack <<endl;

    if(puntuation->getRound()>roundBack){
      ranking.push_back(*puntuation);
      sort(ranking.begin(), ranking.end(),sort_by_rounds);
    }

    rankingFile.open("records.rcd", ios::out | ios::trunc | ios::binary);

    for(unsigned int i=0; i< ranking.size();i++){
      if(i<5){
        rankingFile<< ranking.at(i).getName() << endl;
        rankingFile << ranking.at(i).getRound() << endl;
        rankingFile << ranking.at(i).getPoints() <<endl;
      }else{
        break;
      }
    }
    return 5;
}



void RankingManager::createRankingFile () {
    fstream rankingFile;
    rankingFile.open("records.rcd", ios::in | ios::out | ios::binary);
    if (!rankingFile.good()) {
        rankingFile.close();
        rankingFile.open("records.rcd", ios::out | ios::trunc | ios::binary);

        rankingFile.close();
    } else {
        rankingFile.close();
    }
}
std::vector<RankingPosition> RankingManager::createStringRanking(){
    fstream rankingFile;
    string line;
    RankingPosition* rp;
    int i;
    std::vector<string> vector;
    vector.reserve(15);
    std::vector<RankingPosition> vectorRanking;
    vectorRanking.reserve(5);

    rankingFile.open("records.rcd", ios::in | ios::out | ios::binary);

    if(rankingFile.good()){
      i = 0;
      while(getline(rankingFile, line)){
        if(i<15){
          if(line.size()>0){
            try{
              vector.push_back(line);
              i++;

            }catch (...){
              cout << "No se ha podido leer el dato. Error" << endl;
            }
          }else{
            vector.push_back(0);
            i++;
          }
        }
      }

    }

    for(int i=0;i<15;i=i+3){
      string nombre= vector.at(i);
      int rounds= atoi(vector.at(i+1).c_str());
      int points= atoi(vector.at(i+2).c_str());

      rp= new RankingPosition(rounds,points,nombre);
      vectorRanking.push_back(*rp);


    }

    rankingFile.close();

 return vectorRanking;

}

string RankingManager::createStringRankingAnterior () {
    stringstream res;
    res.str("");
    fstream rankingFile;
    string line;

    std::vector<int> ranking;
    ranking.reserve(5);

    rankingFile.open("records.rcd", ios::in | ios::out | ios::binary);

    int i;
    if (rankingFile.good()) {
        /* Se guardan primero en un vector */
        i = 0;
        while (getline(rankingFile, line)) {
            /* Solo se leen los 5 primeros datos. */
            if (i < 5) {
                if (line.size() > 0) {
                    try {
                        int puntos = atoi(line.c_str());
                        ranking.push_back(puntos);
                        i++;
                    } catch (...) {
                        cout << "No se ha podido leer el dato. Error" << endl;
                    }
                } else {
                    ranking.push_back(0);
                    i++;
                }
            }
        }

        while (i < 5) {
            ranking.push_back(0);
            i++;
        }

        i = 0;
        while (i < 5) {
            res << (i + 1) << " " << ranking.at(i) << "\n";
            i++;
        }
    }

    rankingFile.close();

    return res.str();
}

RankingManager& RankingManager::getSingleton () {
    assert(msSingleton);
    return *msSingleton;
}


RankingManager* RankingManager::getSingletonPtr () {
    return msSingleton;
}

std::vector<string> RankingManager::split (std::string str, char delimiter) {
    std::vector<string> internal;
    stringstream ss(str);
    string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
}
