#include "PhysicsManager.h"

template<> PhysicsManager* Ogre::Singleton<PhysicsManager>::msSingleton = 0;

OgreBulletDynamics::DynamicsWorld* PhysicsManager::createNewWorld ()
{
  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");
  _world = new OgreBulletDynamics::DynamicsWorld(sceneMgr, _worldBounds,
                                                 _gravity);

  #ifdef _DEBUG
  _debugDrawer = new OgreBulletCollisions::DebugDrawer();
  _debugDrawer->setDrawWireframe(true);

  Ogre::SceneNode* node = sceneMgr->getRootSceneNode()->
      createChildSceneNode("debugNode", Ogre::Vector3::ZERO);
  node->attachObject(static_cast <Ogre::SimpleRenderable*>(_debugDrawer));

  _world->setDebugDrawer(_debugDrawer);
  _world->setShowDebugShapes(true);
  #endif

  return _world;
}

PhysicsManager& PhysicsManager::getSingleton()
{
  assert(msSingleton);
  return *msSingleton;
}

PhysicsManager* PhysicsManager::getSingletonPtr()
{
  return msSingleton;
}
