#include <Ogre.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_mixer.h>

#include "GameManager.h"

#include "GameState.h"

#include "SoundFXManager.h"
#include "TrackManager.h"

template<> GameManager* Ogre::Singleton<GameManager>::msSingleton = 0;

GameManager::GameManager () : _root(nullptr),
                              _sceneMgr(nullptr),
                              _renderWindow(nullptr),
                              _inputMgr(nullptr) {
  _root = new Ogre::Root();

  _sceneMgr= _root->createSceneManager(Ogre::ST_GENERIC,"SceneManager");

	_sceneMgr->addRenderQueueListener( new Ogre::OverlaySystem);

  loadResources();

  if (!configure())
    return;

  _inputMgr = new InputManager;
  _inputMgr->initialise(_renderWindow);

  _inputMgr->addKeyListener(this, "GameManager");
  _inputMgr->addMouseListener(this, "GameManager");

  _root->addFrameListener(this);

  initSDL();

  /*_pTrackManager = TrackManager::getSingletonPtr();
  _pSoundFXManager = SoundFXManager::getSingletonPtr();*/

//_pTrackMenu = _pTrackManager->load("Music_Menus.mp3");

}

GameManager::~GameManager () {
  while (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }

  if (_root)
    delete _root;
}

void GameManager::start (GameState* state) {
  changeState(state);

  _root->startRendering();
}

void GameManager::changeState (GameState* state) {
  if (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }

  _states.push(state);
  _states.top()->enter();
}

void GameManager::pushState (GameState* state) {
  if (!_states.empty())
    _states.top()->pause();
  _states.push(state);
  _states.top()->enter();
}

void GameManager::popState () {
  if (!_states.empty()) {
    _states.top()->exit();
    _states.pop();
  }

  if (!_states.empty())
    _states.top()->resume();
}

void GameManager::loadResources () {
  Ogre::ConfigFile cf;
  cf.load("resources.cfg");

  Ogre::ConfigFile::SectionIterator sI = cf.getSectionIterator();
  Ogre::String sectionstr, typestr, datastr;
  while (sI.hasMoreElements()) {
    sectionstr = sI.peekNextKey();
    Ogre::ConfigFile::SettingsMultiMap *settings = sI.getNext();
    Ogre::ConfigFile::SettingsMultiMap::iterator i;
    for (i = settings->begin(); i != settings->end(); ++i) {
      typestr = i->first;    datastr = i->second;
      Ogre::ResourceGroupManager::getSingleton().addResourceLocation
            (datastr, typestr, sectionstr);
    }
  }
}

bool GameManager::configure () {
  if (!_root->restoreConfig()) {
    if (!_root->showConfigDialog()) {
      return false;
    }
  }

  _renderWindow = _root->initialise(true, "Game State Example");

  Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

  return true;
}

GameManager* GameManager::getSingletonPtr () {
  return msSingleton;
}

GameManager& GameManager::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

bool GameManager::frameStarted (const Ogre::FrameEvent& evt) {
  _inputMgr->capture();
  return _states.top()->frameStarted(evt);
}

bool GameManager::frameEnded (const Ogre::FrameEvent& evt) {
  return _states.top()->frameEnded(evt);
}

bool GameManager::keyPressed (const OIS::KeyEvent &e) {
  _states.top()->keyPressed(e);
  return true;
}

bool GameManager::keyReleased (const OIS::KeyEvent &e) {
  _states.top()->keyReleased(e);
  return true;
}

bool GameManager::mouseMoved (const OIS::MouseEvent &e) {
  _states.top()->mouseMoved(e);
  return true;
}

bool GameManager::mousePressed (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
  _states.top()->mousePressed(e, id);
  return true;
}

bool GameManager::mouseReleased (const OIS::MouseEvent &e, OIS::MouseButtonID id) {
  _states.top()->mouseReleased(e, id);
  return true;
}

bool GameManager::initSDL () {
    if (SDL_Init(SDL_INIT_AUDIO) < 0) {
        return false;
    }

    atexit (SDL_Quit);

    if (Mix_OpenAudio (MIX_DEFAULT_FREQUENCY, MIX_DEFAULT_FORMAT, MIX_DEFAULT_CHANNELS, 4096) < 0) {
        return false;
    }

    atexit (Mix_CloseAudio);

    return true;
}
