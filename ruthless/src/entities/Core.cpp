#include "Core.h"

#include <OgreBulletDynamicsRigidBody.h>
#include <Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "Constants.h"
#include "PhysicsManager.h"

Core::Core (TypesDef::MapPtr map, int i, int j) :
  _health(GameConstants::maxLifeCore)
{
  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");

  Ogre::SceneNode* nodeMap = map->getSceneNode();
  TypesDef::GraphPtr graph = map->getMapGraph();
  int columns = map->getColumns();

  std::stringstream entName;
  entName.str("");
  entName << "entCore";

  _entCore = sceneMgr->createEntity(entName.str(), "nucleo.mesh");
  _nodeCore = sceneMgr->createSceneNode(entName.str());
  _nodeCore->attachObject(_entCore);
  nodeMap->addChild(_nodeCore);

  Ogre::Vector3 core_Position = graph->getVertex(i * columns + j)->getData().getPosition();
  _nodeCore->setPosition(core_Position);

  OgreBulletCollisions::StaticMeshToShapeConverter* trimeshCore = new
      OgreBulletCollisions::StaticMeshToShapeConverter(_entCore);
  _shapeCore = trimeshCore->createTrimesh();

  _rigBodyCore = new OgreBulletDynamics::RigidBody(entName.str(), PhysicsManager::getSingletonPtr()->getWorld());
  _rigBodyCore->setStaticShape(_shapeCore, 0.1, 0.8, core_Position);
}

Core::~Core () {
  if (_shapeCore)
  {
    delete _shapeCore;
  }

  if (_rigBodyCore)
  {
    delete _rigBodyCore;
  }

  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");

  _nodeCore->detachObject(_entCore);
  sceneMgr->destroyEntity(_entCore);

  _nodeCore->getParentSceneNode()->removeChild(_nodeCore->getName());
  sceneMgr->destroySceneNode(_nodeCore);
}

void Core::hit (unsigned int dmg) {
  if (dmg > _health)
  {
    _health = 0;
  } else
  {
    _health -= dmg;
  }
}
