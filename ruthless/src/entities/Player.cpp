#include "Player.h"

#include <Ogre.h>

#include <Shapes/OgreBulletCollisionsCapsuleShape.h>
#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "PlayState.h"

#include "Constants.h"
#include "PhysicsManager.h"
#include "Pistol.h"
#include "WeaponFactory.h"

Player::Player () : _coins(GameConstants::initialCoins),
                    _puntuation(0)
{
  _sceneMgr = Ogre::Root::getSingletonPtr()->getSceneManager("SceneManager");

  /* Camera creation */
  _cameraPlayer = _sceneMgr->createCamera("PlayerCamera");
  _cameraPlayer->setNearClipDistance(0.1);
  _cameraPlayer->setFarClipDistance(1000);

  /* Node, Entity, Shape and Body */
  _nodePlayer = _sceneMgr->getSceneNode("TF")->createChildSceneNode("Player");
  _entPlayer = _sceneMgr->createEntity("Player", "personaje.mesh");
  _nodePlayer->attachObject(_entPlayer);

  _nodePlayer->attachObject(_cameraPlayer);
  Ogre::Vector3 cameraPos = _nodePlayer->getPosition();
  cameraPos.y += 0.3;
  _cameraPlayer->setPosition(cameraPos);

  _playerShape = new OgreBulletCollisions::CapsuleCollisionShape(0.25f, 1.8f, Ogre::Vector3(0, 1, 0));

  _bodyPlayer = new OgreBulletDynamics::RigidBody("Player", PhysicsManager::getSingletonPtr()->getWorld());
  _bodyPlayer->setShape(_nodePlayer, _playerShape, 0.0f, 1.0f, 75.0f, Ogre::Vector3(0, 1, 0));

  _bodyPlayer->disableDeactivation();
  _bodyPlayer->getBulletRigidBody()->setAngularFactor(btVector3(0.0f, 0.0f, 0.0f));
  _bodyPlayer->getBulletRigidBody()->setUserPointer(this);

  /* Movement variables */
  _front = false; _back = false; _left = false; _right = false;
  _isJumping = false;

  /* Create weapons */
  _shooting = false;
  _createWeapons();

  _idCurrentWeapon = GameConstants::numMainWeapon;
  _currentWeapon = _weapons.at(_idCurrentWeapon);
  _currentWeapon->getWeaponEnt()->setVisible(true);

  _swapingWeapons = false;
  _timeLeftSwapingWeapons = 0;
}

Player::~Player () {
  if (_playerShape)
  {
    delete _playerShape;
  }

  if (_bodyPlayer)
  {
    delete _bodyPlayer;
  }

  _nodePlayer->detachObject(_cameraPlayer);
  _sceneMgr->destroyCamera(_cameraPlayer);

  _nodePlayer->detachObject(_entPlayer);
  _sceneMgr->destroyEntity(_entPlayer);

  _nodePlayer->getParentSceneNode()->removeChild(_nodePlayer->getName());
  _sceneMgr->destroySceneNode(_nodePlayer);

  _currentWeapon = nullptr;

  _sceneMgr->getSceneNode("WeaponNode")->detachAllObjects();
  for (unsigned int i = 0; i < _weapons.size(); ++i) {
    _weapons.pop_back();
  }

  _sceneMgr->destroySceneNode(_sceneMgr->getSceneNode("WeaponNode"));
  _sceneMgr = nullptr;
}

void Player::update (Ogre::Real deltaT, std::vector<TypesDef::EnemyPtr> enemies) {
  /* Movement from the player. */
  Ogre::Vector3 movement(0.0f, 0.0f, 0.0f);

  if (_front || _back)
  {
    if (_front)
    {
      movement.z -= GameConstants::playerSpeed;
    } else if (_back)
    {
      movement.z = GameConstants::playerSpeed;
    }
  }

  if (_left || _right)
  {
    if (_left)
    {
      movement.x -= GameConstants::playerSpeed/2;
    } else if (_right)
    {
      movement.x = GameConstants::playerSpeed/2;
    }
  }

  if (!_front && !_back && !_left && !_right)
  {
    _bodyPlayer->setLinearVelocity(Ogre::Vector3(0.0f, _bodyPlayer->getLinearVelocity().y, 0.0f));
  }
  else {
    Ogre::Quaternion orientationCam = _cameraPlayer->getOrientation();
    orientationCam.x = 0;
    orientationCam.z = 0;

    Ogre::Vector3 qv(orientationCam.x, orientationCam.y, orientationCam.z);
    Ogre::Vector3 uv = qv.crossProduct(movement);
    Ogre::Vector3 uuv = qv.crossProduct(uv);
    uv *= (2.0 * orientationCam.w);
    uuv *= 2.0;

    Ogre::Vector3 res(movement + uv + uuv);
    Ogre::Vector3 linVelocityPlayer = Ogre::Vector3(res.x, res.y + _bodyPlayer->getLinearVelocity().y, res.z);

    _bodyPlayer->setLinearVelocity(linVelocityPlayer);
  }

  /* If is jumping and touch ground then player isn't jumping */
  if (_isJumping)
  {
    _checkJumpingState();
  }

  /* Reposition weaponNode */
  Ogre::SceneNode* weaponNode = _sceneMgr->getSceneNode("WeaponNode");
  weaponNode->setPosition(_cameraPlayer->getPosition());
  weaponNode->setOrientation(_cameraPlayer->getOrientation());
  if (_idCurrentWeapon == GameConstants::numMainWeapon)
  {
    weaponNode->translate(0.05f, -0.105f, -0.07f, Ogre::Node::TS_LOCAL);
  } else if (_idCurrentWeapon == GameConstants::numSecondaryWeapon)
  {
    weaponNode->translate(0.05f, -0.12f, -0.03f, Ogre::Node::TS_LOCAL);
  }

  _currentWeapon->update(deltaT);

  if (_shooting)
  {
    _currentWeapon->shoot(enemies);
  }
}

void Player::rotateCameraPlayer (Ogre::Real rot_x, Ogre::Real rot_y)
{
  _cameraPlayer->yaw(Ogre::Radian(rot_x));
  _cameraPlayer->pitch(Ogre::Radian(rot_y));

  /* Reposition weaponNode */
  Ogre::SceneNode* weaponNode = _sceneMgr->getSceneNode("WeaponNode");
  weaponNode->setPosition(_cameraPlayer->getPosition());
  weaponNode->setOrientation(_cameraPlayer->getOrientation());
  if (_idCurrentWeapon == GameConstants::numMainWeapon)
  {
    weaponNode->translate(0.05f, -0.105f, -0.07f, Ogre::Node::TS_LOCAL);
  } else if (_idCurrentWeapon == GameConstants::numSecondaryWeapon)
  {
    weaponNode->translate(0.05f, -0.12f, -0.03f, Ogre::Node::TS_LOCAL);
  }
}

void Player::jump () {
  if (!_isJumping)
  {
    Ogre::Vector3 actualLinearVelocity = _bodyPlayer->getLinearVelocity();
    _bodyPlayer->setLinearVelocity(Ogre::Vector3(actualLinearVelocity.x, 6.0f, actualLinearVelocity.z));
    _isJumping = true;
  }
}

void Player::hidePlayer () {
  _nodePlayer->setVisible(false, false);

  _currentWeapon->setVisibility(false);
}

void Player::showPlayer () {
  _nodePlayer->setVisible(true, false);

  _currentWeapon->setVisibility(true);
}

void Player::reload () {
  _currentWeapon->reload();
}

void Player::swapWeapons () {
  if (_idCurrentWeapon == GameConstants::numMainWeapon) {
    _idCurrentWeapon = GameConstants::numSecondaryWeapon;
  } else if (_idCurrentWeapon == GameConstants::numSecondaryWeapon) {
    _idCurrentWeapon = GameConstants::numMainWeapon;
  }

  _currentWeapon = _weapons.at(_idCurrentWeapon);

  for (unsigned int i = 0; i < _weapons.size(); ++i)
  {
    _weapons.at(i)->swap();
  }
}

void Player::startShooting ()
{
  _shooting = true;
}

void Player::stopShooting ()
{
  _shooting = false;
  if (_idCurrentWeapon == GameConstants::numSecondaryWeapon)
  {
    TypesDef::PistolPtr pistolAux = std::dynamic_pointer_cast<Pistol>(_currentWeapon);
    pistolAux->stopShooting();
  }
}

void Player::_createWeapons ()
{
  _nodeWeapon = _nodePlayer->createChildSceneNode("WeaponNode");
  _weapons.reserve(GameConstants::numWeapons);
  _weapons.push_back(WeaponFactory::getSingletonPtr()->createWeapon(GameEnums::TypeWeapon::Rifle));
  _weapons.push_back(WeaponFactory::getSingletonPtr()->createWeapon(GameEnums::TypeWeapon::Pistol));
}

void Player::_checkJumpingState ()
{
  if (_nodePlayer->getPosition().y <= 1.15f)
  {
    _isJumping = false;
  }
    /*OgreBulletDynamics::DynamicsWorld* world = PhysicsManager::getSingletonPtr()->getWorld();
    TypesDef::MapPtr map = PlayState::getSingletonPtr()->getMap();

    btCollisionWorld *bulletWorld = world->getBulletCollisionWorld();
    int numManifolds = bulletWorld->getDispatcher()->getNumManifolds();

    for (int i = 0; i < numManifolds; i++)
    {
      btPersistentManifold* contactManifold = bulletWorld->getDispatcher()->
          getManifoldByIndexInternal(i);

      btCollisionObject* objA = (btCollisionObject*) (contactManifold->getBody0());
      btCollisionObject* objB = (btCollisionObject*) (contactManifold->getBody1());

      btCollisionObject* objGround = map->getRigidBody()->getBulletObject();
      btCollisionObject* objPlayer = _bodyPlayer->getBulletObject();

      OgreBulletCollisions::Object* obOB_A = world->findObject(objA);
      OgreBulletCollisions::Object* obOB_B = world->findObject(objB);

      OgreBulletCollisions::Object* obGround = world->findObject(objGround);
      OgreBulletCollisions::Object* obPlayer = world->findObject(objPlayer);

      if ((obOB_A == obGround && obOB_B == obPlayer) ||
          (obOB_A == obPlayer && obOB_B == obGround))
      {
        _isJumping = false;
      }
    }*/
}
