#include "MachineGunTurret.h"

#include <math.h>

#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "Constants.h"
#include "Turret.h"

MachineGunTurret::MachineGunTurret (unsigned int id, Ogre::Vector3 position) :
      Turret(GameConstants::dmgMachineGunTurret, GameConstants::rateOfFire_MachineGunTurret, id, position)
{
  _timeLastShoot = GameConstants::rateOfFire_MachineGunTurret;

  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");
  Ogre::SceneNode* nodeTerrain = sceneMgr->getSceneNode("TF");

  std::stringstream entName;
  entName.str("");

  entName << "turret_" << _id;
  _turretEnt = sceneMgr->createEntity(entName.str(), "metralleta.mesh");
  _turretNode = sceneMgr->createSceneNode(entName.str());
  _turretNode->attachObject(_turretEnt);
  nodeTerrain->addChild(_turretNode);
  _turretNode->setPosition(position);
  //_turretNode->lookAt(Ogre::Vector3(0,0,0), Ogre::Node::TransformSpace::TS_PARENT);

  _shapeTurret = nullptr;
  _rigBodyTurret = nullptr;
}

MachineGunTurret::~MachineGunTurret ()
{
  if (_shapeTurret)
  {
    delete _shapeTurret;
  }

  if (_rigBodyTurret)
  {
    delete _rigBodyTurret;
  }

  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");

  _turretNode->detachObject(_turretEnt);
  sceneMgr->destroyEntity(_turretEnt);

  _turretNode->getParentSceneNode()->removeChild(_turretNode->getName());
  sceneMgr->destroySceneNode(_turretNode);
}

void MachineGunTurret::update (Ogre::Real deltaT, std::vector<TypesDef::EnemyPtr> enemies)
{
  if (_timeLastShoot > 0)
  {
    _timeLastShoot -= deltaT;
  }

  if (_timeLastShoot <= 0)
  {
    /* Check previous enemy */
    if (_currentEnemy != nullptr)
    {
      if (_currentEnemy->getHealth() == 0)
      {
        _currentEnemy = nullptr;
      } else
      {
        /* Check range */
        Ogre::Vector3 posTurret = _turretNode->getPosition();
        Ogre::Vector3 posEnemy = _currentEnemy->getNode()->getPosition();
        if ((pow((posTurret.x - posEnemy.x), 2) + pow((posTurret.z - posEnemy.z), 2)) >
            GameConstants::squareRangeMachineGunTurret)
        {
          _currentEnemy = nullptr;
        }
      }
    }

    if (_currentEnemy == nullptr)
    {
      _currentEnemy = getBestEnemy(enemies);
    }

    if (_currentEnemy != nullptr)
    {
      _currentEnemy->hit(_dmg);
      if (_currentEnemy->getHealth() == 0)
      {
        _currentEnemy = nullptr;
      }
      _timeLastShoot = GameConstants::rateOfFire_MachineGunTurret;

      // PARTICULAS DISPARO
    }
  }

  /* Look at enemy */
  if (_currentEnemy != nullptr)
  {
    Ogre::Vector3 posToLook (0.0f, 0.0f, 0.0f);
    Ogre::Vector3 worldPosEnemy = _currentEnemy->getNode()->//_currentEnemy->getNode()->_getDerivedPosition();
        convertLocalToWorldPosition(_currentEnemy->getNode()->getPosition());
    posToLook.x = worldPosEnemy.x;
    posToLook.y = _turretNode->getPosition().y;
    posToLook.z = worldPosEnemy.z;

    _turretNode->lookAt(posToLook, Ogre::Node::TransformSpace::TS_WORLD);
  }
}

TypesDef::EnemyPtr MachineGunTurret::getBestEnemy (std::vector<TypesDef::EnemyPtr> enemies)
{
  TypesDef::EnemyPtr bestEnemy = nullptr;
  Ogre::Vector3 posTurret = _turretNode->getPosition();
  for (unsigned int i = 0; i < enemies.size(); ++i)
  {
    if (enemies.at(i)->getHealth() > 0)
    {
      Ogre::Vector3 posEnemy = enemies.at(i)->getNode()->getPosition();

      if ((pow((posTurret.x - posEnemy.x), 2) + pow((posTurret.z - posEnemy.z), 2)) <=
          GameConstants::squareRangeMachineGunTurret)
      {
        bestEnemy = enemies.at(i);
        break;
      }
    }
  }

  return bestEnemy;
}
