#include "Enemy_Type_1.h"

#include <Ogre.h>

#include "Cell.h"

#include "Graph.h"
#include "GraphNode.h"
#include "GraphVertex.h"
#include "Map.h"
#include "PhysicsManager.h"
#include "PlayState.h"

Enemy_Type_1::Enemy_Type_1 (TypesDef::MapPtr map, unsigned int round, unsigned int index) :
    Enemy(GameConstants::dmgEnemy_1,
          (GameConstants::healthEnemy_1 + ((round - 1) * GameConstants::increasedHealthRoundEnemy_1)),
          map)
{
  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");
  Ogre::SceneNode* nodeTerrain = sceneMgr->getSceneNode("TF");

  std::stringstream entName;
  entName.str("");
  entName << "enemy_" << round << "_" << index;
  _enemyEnt = sceneMgr->createEntity(entName.str(), "Enemy.mesh");
  _enemyEnt->setQueryFlags(Masks::ENEMY_MASK);
  _enemyNode = sceneMgr->createSceneNode(entName.str());
  _enemyNode->attachObject(_enemyEnt);
  nodeTerrain->addChild(_enemyNode);
  #ifdef _DEBUG
  _enemyNode->showBoundingBox(true);
  #endif

  OgreBulletCollisions::StaticMeshToShapeConverter* trimeshConverterMap = new
			OgreBulletCollisions::StaticMeshToShapeConverter(_enemyEnt);
	_enemyShape = trimeshConverterMap->createTrimesh();
	_bodyEnemy = new OgreBulletDynamics::RigidBody(entName.str(),
      PhysicsManager::getSingletonPtr()->getWorld());

  _bodyEnemy->setShape(_enemyNode, _enemyShape, 0.0, 0.8, 70, Ogre::Vector3(0.0f, 1.0f, 0.0f));
  _bodyEnemy->disableDeactivation();
  _bodyEnemy->setKinematicObject(true);
  _bodyEnemy->getBulletRigidBody()->setAngularFactor(btVector3(0.0f, 0.0f, 0.0f));
  _bodyEnemy->getBulletRigidBody()->setUserPointer(this);

  respawn();
}

Enemy_Type_1::~Enemy_Type_1 ()
{
  _map = nullptr;
  _currentVertex = nullptr;
  _nextVert = nullptr;

  if (_enemyShape)
  {
    delete _enemyShape;
  }

  if (_bodyEnemy)
  {
    delete _bodyEnemy;
  }

  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingleton().getSceneManager("SceneManager");

  _enemyNode->detachObject(_enemyEnt);
  sceneMgr->destroyEntity(_enemyEnt);

  _enemyNode->getParentSceneNode()->removeChild(_enemyNode->getName());
  sceneMgr->destroySceneNode(_enemyNode);
}

void Enemy_Type_1::hit (unsigned int dmgTaken)
{

  _pSoundFXManager = SoundFXManager::getSingletonPtr();

  _effectDied=_pSoundFXManager->load("Sounds/died.wav");

  if (_health > 0)
  {
    if (dmgTaken >= _health)
    {
      _health = 0;
      PlayState::getSingletonPtr()->addEnemyDie(_enemyNode->getPosition());
      /* Add Points & Coins to Player*/
      PlayState::getSingletonPtr()->getPlayer()->addPointsMonsterKilled(GameConstants::pointsEnemy_1);
      PlayState::getSingletonPtr()->getPlayer()->addCoinsMonsterKilled(GameConstants::pointsEnemy_1);

      _enemyNode->setVisible(false);
      _effectDied->play();

      /* Delete rigid body and shape */
      if (_enemyShape)
      {
        delete _enemyShape;
        _enemyShape = nullptr;
      }

      if (_bodyEnemy)
      {
        delete _bodyEnemy;
        _bodyEnemy = nullptr;
      }
    } else
    {
      _health -= dmgTaken;
    }
  }
}
