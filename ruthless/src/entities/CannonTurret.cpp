#include "CannonTurret.h"

#include <math.h>

#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "Constants.h"
#include "PhysicsManager.h"
#include "Turret.h"

CannonTurret::CannonTurret (unsigned int id, Ogre::Vector3 position) :
      Turret(GameConstants::dmgCannonTurret, GameConstants::rateOfFire_CannonTurret, id, position)
{
  _timeLastShoot = GameConstants::rateOfFire_CannonTurret;

  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");
  Ogre::SceneNode* nodeTerrain = sceneMgr->getSceneNode("TF");

  std::stringstream entName;
  entName.str("");

  entName << "turret_" << _id;
  _turretEnt = sceneMgr->createEntity(entName.str(), "torretaCanon.mesh");
  _turretNode = sceneMgr->createSceneNode(entName.str());
  _turretNode->attachObject(_turretEnt);
  nodeTerrain->addChild(_turretNode);
  _turretNode->setPosition(position);
  _turretNode->yaw(Ogre::Radian(Ogre::Degree(180)));
  //_turretNode->lookAt(Ogre::Vector3(0,0,0), Ogre::Node::TransformSpace::TS_PARENT);

  _shapeTurret = nullptr;
  _rigBodyTurret = nullptr;
  /*OgreBulletCollisions::StaticMeshToShapeConverter* trimeshTurret = new
      OgreBulletCollisions::StaticMeshToShapeConverter(_turretEnt);
  _shapeTurret = trimeshTurret->createTrimesh();
  _rigBodyTurret = new OgreBulletDynamics::RigidBody(entName.str(), PhysicsManager::getSingletonPtr()->getWorld());
  _rigBodyTurret->setStaticShape(_shapeTurret, 0.1, 0.8, position);*/
}

CannonTurret::~CannonTurret ()
{
  if (_shapeTurret)
  {
    delete _shapeTurret;
  }

  if (_rigBodyTurret)
  {
    delete _rigBodyTurret;
  }

  Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");

  _turretNode->detachObject(_turretEnt);
  sceneMgr->destroyEntity(_turretEnt);

  _turretNode->getParentSceneNode()->removeChild(_turretNode->getName());
  sceneMgr->destroySceneNode(_turretNode);
}

void CannonTurret::update (Ogre::Real deltaT, std::vector<TypesDef::EnemyPtr> enemies)
{
  if (_timeLastShoot > 0)
  {
    _timeLastShoot -= deltaT;
  }

  if (_timeLastShoot <= 0)
  {
    /* Check previous enemy */
    if (_currentEnemy != nullptr)
    {
      if (_currentEnemy->getHealth() == 0)
      {
        _currentEnemy = nullptr;
      } else
      {
        /* Check range */
        Ogre::Vector3 posTurret = _turretNode->getPosition();
        Ogre::Vector3 posEnemy = _currentEnemy->getNode()->getPosition();
        if ((pow((posTurret.x - posEnemy.x), 2) + pow((posTurret.z - posEnemy.z), 2)) >
            GameConstants::squareRangeCannonTurret)
        {
          _currentEnemy = nullptr;
        }
      }
    }

    if (_currentEnemy == nullptr)
    {
      _currentEnemy = getBestEnemy(enemies);
    }

    if (_currentEnemy != nullptr)
    {
      _currentEnemy->hit(_dmg);
      if (_currentEnemy->getHealth() == 0)
      {
        _currentEnemy = nullptr;
      }
      _timeLastShoot = GameConstants::rateOfFire_CannonTurret;

      // PARTICULAS DISPARO
    }
  }

  /* Look at enemy */
  if (_currentEnemy != nullptr)
  {
    Ogre::Vector3 posToLook (0.0f, 0.0f, 0.0f);
    Ogre::Vector3 worldPosEnemy = _currentEnemy->getNode()->_getDerivedPosition();//_currentEnemy->getNode()->
        //convertLocalToWorldPosition(_currentEnemy->getNode()->getPosition());
    posToLook.x = worldPosEnemy.x;
    posToLook.y = _turretNode->getPosition().y;
    posToLook.z = worldPosEnemy.z;

    _turretNode->lookAt(posToLook, Ogre::Node::TransformSpace::TS_WORLD);
  }
}

TypesDef::EnemyPtr CannonTurret::getBestEnemy (std::vector<TypesDef::EnemyPtr> enemies)
{
  TypesDef::EnemyPtr bestEnemy = nullptr;
  Ogre::Vector3 posTurret = _turretNode->getPosition();
  for (unsigned int i = 0; i < enemies.size(); ++i)
  {
    if (enemies.at(i)->getHealth() > 0)
    {
      Ogre::Vector3 posEnemy = enemies.at(i)->getNode()->getPosition();

      if ((pow((posTurret.x - posEnemy.x), 2) + pow((posTurret.z - posEnemy.z), 2)) <=
          GameConstants::squareRangeCannonTurret)
      {
        bestEnemy = enemies.at(i);
        break;
      }
    }
  }

  return bestEnemy;
}
