#include "MouseConverter.h"

template<> MouseConverter* Ogre::Singleton<MouseConverter>::msSingleton = 0;

CEGUI::MouseButton MouseConverter::convertMouseButton (OIS::MouseButtonID id) {
    CEGUI::MouseButton ceguiId;

    switch (id) {
        case OIS::MB_Left:
            ceguiId = CEGUI::LeftButton;
            break;
        case OIS::MB_Right:
            ceguiId = CEGUI::RightButton;
            break;
        case OIS::MB_Middle:
            ceguiId = CEGUI::MiddleButton;
            break;
        default:
            ceguiId = CEGUI::LeftButton;
    }

    return ceguiId;
}

MouseConverter* MouseConverter::getSingletonPtr () {
  return msSingleton;
}

MouseConverter& MouseConverter::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}
