#include "MapConverterToGraph.h"

#include "Graph.h"
#include "GraphEdge.h"
#include "GraphNode.h"
#include "GraphVertex.h"

template<> MapConverterToGraph* Ogre::Singleton<MapConverterToGraph>::msSingleton = 0;

TypesDef::GraphPtr MapConverterToGraph::convertMatrix (TypesDef::MapPtr mp)
{
  TypesDef::GraphPtr graph = std::make_shared<Graph>();

  Cell** mapMatrix = mp->getMapMatrix();

  int rows = mp->getRows();
  int columns = mp->getColumns();

  for (int i = 0; i < rows; ++i)
  {
    for (int j = 0; j < columns; ++j)
    {
      int index = i * columns + j;

      double x = 0;
      double z = 0;

      if ((columns % 2) == 0)
      {
        int halfColumns = columns/2;
        x = (j - halfColumns) * GameConstants::sidePlatform + (GameConstants::sidePlatform / 2.0);
      } else
      {
        int halfColumns = columns/2;
        x = (j - halfColumns) * GameConstants::sidePlatform;
      }

      if ((rows % 2) == 0)
      {
        int halfRows = rows/2;
        z = (i - halfRows) * GameConstants::sidePlatform + (GameConstants::sidePlatform / 2.0);
      } else
      {
        int halfRows = rows/2;
        z = (i - halfRows) * GameConstants::sidePlatform;
      }

      Ogre::Vector3 position = Ogre::Vector3(x, 0.1, z);

      GraphNode node = GraphNode (index, position, mapMatrix[i][j].getType());
      TypesDef::GraphVertexPtr vertex_aux = std::make_shared<GraphVertex>(node);
      graph->addVertex(vertex_aux);
    }
  }

  for (int i = 0; i < rows; ++i)
  {
    for (int j = 0; j < columns; ++j)
    {
      int index = i * columns + j;
      TypesDef::GraphVertexPtr vertex_aux = graph->getVertex(index);
      int index_dest;
      if (i > 0)
      {
        index_dest = (i - 1) * columns + j;
        TypesDef::GraphVertexPtr vertex_dest = graph->getVertex(index_dest);
        graph->addEdge(vertex_aux, vertex_dest, false);
      }

      if (i < (rows - 1))
      {
        index_dest = (i + 1) * columns + j;
        TypesDef::GraphVertexPtr vertex_dest = graph->getVertex(index_dest);
        graph->addEdge(vertex_aux, vertex_dest, false);
      }

      if (j > 0)
      {
        index_dest = i * columns + (j - 1);
        TypesDef::GraphVertexPtr vertex_dest = graph->getVertex(index_dest);
        graph->addEdge(vertex_aux, vertex_dest, false);
      }

      if (j < (columns - 1))
      {
        index_dest = i * columns + (j + 1);
        TypesDef::GraphVertexPtr vertex_dest = graph->getVertex(index_dest);
        graph->addEdge(vertex_aux, vertex_dest, false);
      }
    }
  }

  return graph;
}

MapConverterToGraph& MapConverterToGraph::getSingleton ()
{
  assert(msSingleton);
  return *msSingleton;
}

MapConverterToGraph* MapConverterToGraph::getSingletonPtr ()
{
  return msSingleton;
}
