#include "RankingPosition.h"

RankingPosition::RankingPosition(unsigned int rounds, unsigned int points, std::string name){
  _rounds = rounds;
  _points = points;
  _name = name;
}

RankingPosition::~RankingPosition(){}

void RankingPosition::setRound(unsigned int rounds){
  _rounds= rounds;
}

void RankingPosition::setPoints(unsigned int points){
  _points=points;
}

void RankingPosition::setName(std::string name){
  _name=name;
}

unsigned int RankingPosition::getPoints(){
  return _points;
}

unsigned int RankingPosition::getRound(){
  return _rounds;
}

std::string RankingPosition::getName(){
  return _name;
}
