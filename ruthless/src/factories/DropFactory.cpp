#include "DropFactory.h"

#include <memory>

#include <Ogre.h>

#include "Constants.h"
#include "AmmoDrop.h"
#include "HealthRestoreDrop.h"

template<> DropFactory* Ogre::Singleton<DropFactory>::msSingleton = 0;

TypesDef::DropPtr DropFactory::createDrop (GameEnums::TypesDrop type, Ogre::Vector3 pos, unsigned int index)
{
  if (type == GameEnums::TypesDrop::Drop_Ammo) {
    return std::make_shared<AmmoDrop>(pos, index);
  } else if (type == GameEnums::TypesDrop::Drop_Health) {
    return std::make_shared<HealthRestoreDrop>(pos, index);
  }

  return nullptr;
}

DropFactory& DropFactory::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

DropFactory* DropFactory::getSingletonPtr () {
  return msSingleton;
}
