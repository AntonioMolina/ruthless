#include "EnemyFactory.h"

#include <memory>

#include <Ogre.h>

#include "Constants.h"
#include "Enemy_Type_1.h"
#include "Enemy_Type_2.h"

template<> EnemyFactory* Ogre::Singleton<EnemyFactory>::msSingleton = 0;

TypesDef::EnemyPtr EnemyFactory::createEnemy (GameEnums::TypeEnemy type, TypesDef::MapPtr map,
    unsigned int round, unsigned int index)
{
  if (type == GameEnums::TypeEnemy::Enemy_Type_1) {
    return std::make_shared<Enemy_Type_1>(map, round, index);
  } else if (type == GameEnums::TypeEnemy::Enemy_Type_2) {
    return std::make_shared<Enemy_Type_2>(map, round, index);
  }

  return nullptr;
}

EnemyFactory& EnemyFactory::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

EnemyFactory* EnemyFactory::getSingletonPtr () {
  return msSingleton;
}
