#include "WeaponFactory.h"

#include <memory>

#include <Ogre.h>

#include "Constants.h"
#include "Pistol.h"
#include "Rifle.h"

template<> WeaponFactory* Ogre::Singleton<WeaponFactory>::msSingleton = 0;

TypesDef::WeaponPtr WeaponFactory::createWeapon (GameEnums::TypeWeapon type) {
  if (type == GameEnums::TypeWeapon::Pistol) {
    return std::make_shared<Pistol>();
  } else if (type == GameEnums::TypeWeapon::Rifle) {
    return std::make_shared<Rifle>();
  }

  return nullptr;
}

WeaponFactory& WeaponFactory::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

WeaponFactory* WeaponFactory::getSingletonPtr () {
  return msSingleton;
}
