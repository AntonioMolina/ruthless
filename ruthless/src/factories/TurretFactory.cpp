#include "TurretFactory.h"

#include <memory>

#include <Ogre.h>

#include "Constants.h"
#include "CannonTurret.h"
#include "MachineGunTurret.h"

template<> TurretFactory* Ogre::Singleton<TurretFactory>::msSingleton = 0;

TypesDef::TurretPtr TurretFactory::createTurret (GameEnums::TypeTurret type,
      unsigned int id, Ogre::Vector3 position) {
  if (type == GameEnums::TypeTurret::CannonTurret) {
    return std::make_shared<CannonTurret>(id, position);
  } else if (type == GameEnums::TypeTurret::MachineGunTurret) {
    return std::make_shared<MachineGunTurret>(id, position);
  }

  return nullptr;
}

TurretFactory& TurretFactory::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}

TurretFactory* TurretFactory::getSingletonPtr () {
  return msSingleton;
}
