#include "GraphNode.h"

#include "Constants.h"

GraphNode::GraphNode () : _type(GameEnums::TypeCell::FreeCell)
{
}

GraphNode::GraphNode
(const int& index, const Ogre::Vector3& position, const GameEnums::TypeCell type)
{
  _index = index;
  _position = position;
  _type = type;
}

GraphNode::~GraphNode ()
{
}

GraphNode::operator
std::string() const
{
  std::stringstream r;
  r << "[GraphNode: " << _index << " Type: " << _type << " ("
    << _position.x << ", " << _position.y << ", "
    << _position.z << ")]";
  return r.str();
}
