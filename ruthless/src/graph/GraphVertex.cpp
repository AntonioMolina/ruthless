#include "GraphVertex.h"

GraphVertex::GraphVertex (const GraphNode& data) : _data(data)
{
}

GraphVertex::GraphVertex () {}

GraphVertex::~GraphVertex ()
{
  _edges.clear();
}

std::vector<TypesDef::GraphVertexPtr> GraphVertex::adjacents () const
{
  std::vector<TypesDef::GraphVertexPtr> result;
  std::vector<TypesDef::GraphEdgePtr>::const_iterator it;

  for (it = _edges.begin(); it != _edges.end(); ++it) {
    result.push_back((*it)->getDestination());
  }

  return result;
}
