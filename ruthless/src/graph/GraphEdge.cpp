#include "GraphEdge.h"

GraphEdge::GraphEdge (TypesDef::GraphVertexPtr origin,
                      TypesDef::GraphVertexPtr dest) :
                        _pOrigin(origin),
                        _pDestination(dest)
{
}

GraphEdge::~GraphEdge ()
{
  _pOrigin = nullptr;
  _pDestination = nullptr;
}
