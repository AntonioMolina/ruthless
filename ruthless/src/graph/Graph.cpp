#include "Graph.h"

Graph::Graph ()
{
  _vertexes.reserve(25);
  _edges.reserve(25);
}

Graph::~Graph ()
{
  _vertexes.clear();
  _edges.clear();
}

void Graph::addVertex (TypesDef::GraphVertexPtr pVertex)
{
  _vertexes.push_back(pVertex);
}

void Graph::addEdge (TypesDef::GraphVertexPtr pOrigin,
                     TypesDef::GraphVertexPtr pDestination,
                     bool undirected)
{
  TypesDef::GraphEdgePtr pEdge = std::make_shared<GraphEdge> (pOrigin, pDestination);
  _edges.push_back(pEdge);
  pOrigin->addEdge(pEdge);

  if (undirected) {
    TypesDef::GraphEdgePtr pEdge2 = std::make_shared<GraphEdge> (pDestination, pOrigin);
    _edges.push_back(pEdge2);
    pDestination->addEdge(pEdge2);
  }
}

std::vector<TypesDef::GraphVertexPtr> Graph::adjacents (int index) const
{
  std::vector<TypesDef::GraphVertexPtr> result;
  std::vector<TypesDef::GraphVertexPtr>::const_iterator it;

  for (it = _vertexes.begin(); it != _vertexes.end(); ++it) {
    if ((*it)->getData().getIndex() == index)
      return (*it)->adjacents();
  }

  return result;
}

TypesDef::GraphVertexPtr Graph::getVertex (int index) const
{
  std::vector<TypesDef::GraphVertexPtr>::const_iterator it;

  for (it = _vertexes.begin(); it != _vertexes.end(); ++it) {
    if ((*it)->getData().getIndex() == index)
      return (*it);
  }

  return nullptr;
}
