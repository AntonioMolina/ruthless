#include "MapGenerator.h"

#include <memory>

#include "AStar.h"
#include "MapConverterToGraph.h"

template<> MapGenerator* Ogre::Singleton<MapGenerator>::msSingleton = 0;

TypesDef::MapPtr MapGenerator::generateMap ()
{
  /* Generate random rows and columns */
  int rows = GameConstants::minRows + rand() %
                      (GameConstants::maxRows + 1 - GameConstants::minRows);
  int columns = GameConstants::minColumns + rand() %
                      (GameConstants::maxColumns + 1 - GameConstants::minColumns);

  /* Create and initiaze map, spawns and points */
  Cell** mapMatrix = new Cell* [rows];
  for (int i = 0; i < rows; ++i)
  {
    mapMatrix[i] = new Cell [columns];
    for (int j = 0; j < columns; ++j)
    {
      mapMatrix[i][j] = Cell(i, j, GameEnums::TypeCell::FreeCell);
    }
  }

  Ogre::Vector2 core;
  std::vector<Ogre::Vector2> randomPoints;
  std::vector<Ogre::Vector2> spawns;

  /* Put Core in a random column in the first row */
  int coreColumn = 2 + rand() % (columns - 4);
  mapMatrix[0][coreColumn].setType(GameEnums::TypeCell::CoreCell);
  core.x = 0;
  core.y = coreColumn;

  /* Generate random spawns. Between GameConstants::minNumSpawns and GameConstants::minNumSpawns
     (see include/utils/Constants.h) */
  int nspawns = GameConstants::minNumSpawns + rand() %
                         (GameConstants::maxNumSpawns + 1 - GameConstants::minNumSpawns);

  int rowSecondSpawn = 0;
  int columnSecondSpawn = 0;

  for (int i = 0; i < nspawns; ++i)
  {
    int rowSpawn = 0;
    int columnSpawn = 0;

    if (i == 0) /* First spawn */
    {
      rowSpawn = (rows - 1);

      columnSpawn = 2 + rand() % (columns - 4);
    } else if (i == 1) /* Second spawn */
    {
      if ((rand() % 2) == 0)
      {
        columnSpawn = 0;
      } else
      {
        columnSpawn = (columns - 1);
      }

      columnSecondSpawn = columnSpawn;

      rowSpawn = (rows * 2/3) + rand() % (rows - 2 - (rows * 2/3));
      rowSecondSpawn = rowSpawn;
    } else if (i == 2) /* Third spawn */
    {
      if (columnSecondSpawn == 0)
      {
        columnSpawn = (columns - 1);
      } else if (columnSecondSpawn == (columns - 1))
      {
        columnSpawn = 0;
      }
      
      rowSpawn = rowSecondSpawn;
    }

    mapMatrix[rowSpawn][columnSpawn].setType(GameEnums::TypeCell::SpawnCell);
    spawns.push_back(Ogre::Vector2(rowSpawn, columnSpawn));
  }

  /* Generate two random points */
  int point_x_1, point_x_2;
  int point_y_1, point_y_2;

  if (nspawns == 1)
  {
    if ((rows % 2) == 0)
    {
      point_x_1 = (rows/2) + rand() % (rows/2 - 1);
    } else
    {
      point_x_1 = (rows/2) + rand() % (rows/2);
    }

    Ogre::Vector2 spawn = spawns[0];
    int columnSpawn = (int) (spawn.y);
    if (columnSpawn > columns/2)
    {
      point_y_1 = 1 + rand() % (columns/2 - 1);
    } else
    {
      point_y_1 = (columns/2) + rand() % (columns/2);
    }
  } else if (nspawns == 2)
  {
    Ogre::Vector2 spawn = spawns[1];

    int rowSpawn = (int) (spawn.x);
    point_x_1 = rowSpawn;//(rows/2) + rand() % (rowSpawn - (rows/2));

    int columnSpawn = (int) (spawn.y);
    if (columnSpawn < columns/2)
    {
      point_y_1 = 1 + rand() % (columns/2 - 1);
    } else
    {
      point_y_1 = (columns/2) + rand() % (columns/2);
    }
  } else if (nspawns == 3)
  {
    point_x_1 = rowSecondSpawn;

    if ((columns % 2) == 0)
    {
      point_y_1 = columns/2;
    } else
    {
      if ((rand() % 2) == 0)
      {
        point_y_1 = columns/2;
      } else
      {
        point_y_1 = columns/2 + 1;
      }
    }
  }

  mapMatrix[point_x_1][point_y_1].setType(GameEnums::TypeCell::PointCell);
  randomPoints.push_back(Ogre::Vector2(point_x_1, point_y_1));

  point_x_2 = 3 + rand() % (rows * 2/5 - 2);
  if (coreColumn > (rows/2))
  {
    point_y_2 = 1 + rand() % (columns/2 - 1);
  } else
  {
    point_y_2 = (columns/2) + rand() % (columns/2);
  }

  mapMatrix[point_x_2][point_y_2].setType(GameEnums::TypeCell::PointCell);
  randomPoints.push_back(Ogre::Vector2(point_x_2, point_y_2));

  AStar aStar = AStar(mapMatrix, rows, columns, core, randomPoints, spawns);
  mapMatrix = aStar.createPath();

  for (int i = 0; i < nspawns; ++i)
  {
    mapMatrix[(int)spawns.at(i).x][(int)spawns.at(i).y].setType(GameEnums::TypeCell::SpawnCell);
  }

  TypesDef::MapPtr newMap = std::make_shared<Map>(mapMatrix, rows, columns,
      nullptr, core, randomPoints, spawns);

  TypesDef::GraphPtr newGraph = MapConverterToGraph::getSingletonPtr()->convertMatrix(newMap);

  newMap->setMapGraph(newGraph);

  return newMap;
}

MapGenerator* MapGenerator::getSingletonPtr () {
  return msSingleton;
}

MapGenerator& MapGenerator::getSingleton () {
  assert(msSingleton);
  return *msSingleton;
}
