#include "AStar.h"

#include <math.h>

AStar::AStar (Cell** mapMatrix, int rows, int columns,
              Ogre::Vector2 core,
              std::vector<Ogre::Vector2> randomPoints,
              std::vector<Ogre::Vector2> spawns) :
              _rows(rows), _columns(columns), _core(core),
              _randomPoints(randomPoints), _spawns(spawns)
{
  _mapMatrix = new Cell* [_rows];
  for (int i = 0; i < _rows; ++i)
  {
    _mapMatrix[i] = new Cell [_columns];
    for (int j = 0; j < _columns; ++j)
    {
      _mapMatrix[i][j] = Cell(mapMatrix[i][j].getRow(),
              mapMatrix[i][j].getColumn(), mapMatrix[i][j].getType());
    }
  }
}

AStar::~AStar ()
{
}

Cell** AStar::createPath ()
{
  Cell coreCell = _mapMatrix[(int)(_core.x)][(int)(_core.y)];
  Cell firstPoint = _mapMatrix[(int)(_randomPoints.at(0).x)][(int)(_randomPoints.at(0).y)];
  Cell secondPoint = _mapMatrix[(int)(_randomPoints.at(1).x)][(int)(_randomPoints.at(1).y)];

  /* Generate path from spawn points to the first random point */
  for (int i = (_spawns.size() - 1); i >= 0; --i)
  {
    std::priority_queue<Cell, std::vector<Cell>, std::greater<Cell>> cells;

    Cell first_cell = _mapMatrix[(int)(_spawns.at(i).x)][(int)(_spawns.at(i).y)];
    cells.push(first_cell);
    bool sol = false;

    while (!sol && !cells.empty())
    {
      Cell cellAux = cells.top();
      cells.pop();

      if (cellAux.getRow() == firstPoint.getRow() && cellAux.getColumn() == firstPoint.getColumn())
      {
        sol = true;
        unsigned int elements = cells.size();
        for (unsigned int i = 0; i < elements; ++i)
        {
          cells.pop();
        }
      } else
      {
        std::vector<Cell> adjacents = getAdjacents(_mapMatrix, cellAux.getRow(), cellAux.getColumn());

        unsigned int elements = cells.size();
        for (unsigned int i = 0; i < elements; ++i)
        {
          cells.pop();
        }

        for (unsigned int i = 0; i < adjacents.size(); ++i)
        {
          if (estimateCost(adjacents.at(i), firstPoint) < estimateCost(cellAux, firstPoint))
          {
            cells.push(adjacents.at(i));
          }
        }
        _mapMatrix[cellAux.getRow()][cellAux.getColumn()].setType(GameEnums::TypeCell::PathCell);
      }
    }
  }

  /* Generate path between first and second points */
  std::priority_queue<Cell, std::vector<Cell>, std::greater<Cell>> cells_secondA;
  cells_secondA.push(firstPoint);
  bool sol_betweenPoints = false;

  while (!sol_betweenPoints && !cells_secondA.empty())
  {
    Cell cellAux = cells_secondA.top();
    cells_secondA.pop();

    if (cellAux.getRow() == secondPoint.getRow() && cellAux.getColumn() == secondPoint.getColumn())
    {
      sol_betweenPoints = true;

      unsigned int elements = cells_secondA.size();
      for (unsigned int i = 0; i < elements; ++i)
      {
        cells_secondA.pop();
      }
    } else
    {
      std::vector<Cell> adjacents = getAdjacents(_mapMatrix, cellAux.getRow(), cellAux.getColumn());

      unsigned int elements = cells_secondA.size();
      for (unsigned int i = 0; i < elements; ++i)
      {
        cells_secondA.pop();
      }

      for (unsigned int i = 0; i < adjacents.size(); ++i)
      {
        if (estimateCost(adjacents.at(i), secondPoint) < estimateCost(cellAux, secondPoint))
        {
          cells_secondA.push(adjacents.at(i));
        }
      }

      _mapMatrix[cellAux.getRow()][cellAux.getColumn()].setType(GameEnums::TypeCell::PathCell);
    }
  }

  /* Generate path from second point to core */
  std::priority_queue<Cell, std::vector<Cell>, std::greater<Cell>> cells_toCore;
  cells_toCore.push(secondPoint);
  bool sol_toCore = false;

  while (!sol_toCore && !cells_toCore.empty())
  {
    Cell cellAux = cells_toCore.top();
    cells_toCore.pop();

    if (cellAux.getRow() == coreCell.getRow() && cellAux.getColumn() == coreCell.getColumn())
    {
      sol_toCore = true;

      unsigned int elements = cells_toCore.size();
      for (unsigned int i = 0; i < elements; ++i)
      {
        cells_toCore.pop();
      }
    } else
    {
      std::vector<Cell> adjacents = getAdjacents(_mapMatrix, cellAux.getRow(), cellAux.getColumn());

      unsigned int elements = cells_toCore.size();
      for (unsigned int i = 0; i < elements; ++i)
      {
        cells_toCore.pop();
      }

      for (unsigned int i = 0; i < adjacents.size(); ++i)
      {
        if (estimateCost(adjacents.at(i), coreCell) < estimateCost(cellAux, coreCell))
        {
          cells_toCore.push(adjacents.at(i));
        }
      }

      _mapMatrix[cellAux.getRow()][cellAux.getColumn()].setType(GameEnums::TypeCell::PathCell);
    }
  }

  return _mapMatrix;
}

double AStar::estimateCost(Cell cell_1, Cell cell_2)
{
  return (pow((cell_1.getRow() - cell_2.getRow()), 2) + pow((cell_1.getColumn() - cell_2.getColumn()), 2));
  //return sqrt((pow((cell_1.getRow() - cell_2.getRow()), 2) + pow((cell_1.getColumn() - cell_2.getColumn()), 2)));
}

std::vector<Cell> AStar::getAdjacents (Cell** mapMatrix, int row, int column)
{
  std::vector<Cell> adjacents;

  if (row > 0)
  {
    bool topValid = true;
    bool rightValid = true;
    bool leftValid = true;
    if (mapMatrix[row-1][column].getType() == GameEnums::TypeCell::FreeCell)
    {
      if ((row - 1) > 0)
      {
        if (mapMatrix[row-2][column].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row-2][column].getType() == GameEnums::TypeCell::SpawnCell)
        {
          topValid = false;
        }
      }

      if (column > 0)
      {
        if (mapMatrix[row-1][column-1].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row-1][column-1].getType() == GameEnums::TypeCell::SpawnCell)
        {
          leftValid = false;
        }
      }

      if (column < (_columns - 1))
      {
        if (mapMatrix[row-1][column+1].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row-1][column+1].getType() == GameEnums::TypeCell::SpawnCell)
        {
          rightValid = false;
        }
      }

      if (topValid && rightValid && leftValid)
      {
        adjacents.push_back(mapMatrix[row-1][column]);
      }
    }
  }

  if (row < (_rows - 1))
  {
    bool botValid = true;
    bool rightValid = true;
    bool leftValid = true;

    if (mapMatrix[row+1][column].getType() == GameEnums::TypeCell::FreeCell)
    {
      if ((row + 1) < (_rows - 1))
      {
        if (mapMatrix[row+2][column].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row+2][column].getType() == GameEnums::TypeCell::SpawnCell)
        {
          botValid = false;
        }
      }

      if (column > 0)
      {
        if (mapMatrix[row+1][column-1].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row+1][column-1].getType() == GameEnums::TypeCell::SpawnCell)
        {
          leftValid = false;
        }
      }

      if (column < (_columns - 1))
      {
        if (mapMatrix[row+1][column+1].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row+1][column+1].getType() == GameEnums::TypeCell::SpawnCell)
        {
          rightValid = false;
        }
      }

      if (botValid && rightValid && leftValid)
      {
        adjacents.push_back(mapMatrix[row+1][column]);
      }
    }
  }

  if (column > 0)
  {
    bool topValid = true;
    bool botValid = true;
    bool leftValid = true;

    if (mapMatrix[row][column-1].getType() == GameEnums::TypeCell::FreeCell)
    {
      if (row > 0)
      {
        if (mapMatrix[row-1][column-1].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row-1][column-1].getType() == GameEnums::TypeCell::SpawnCell)
        {
          topValid = false;
        }
      }

      if (row < (_rows - 1))
      {
        if (mapMatrix[row+1][column-1].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row+1][column-1].getType() == GameEnums::TypeCell::SpawnCell)
        {
          botValid = false;
        }
      }

      if ((column - 1) > 0)
      {
        if (mapMatrix[row][column-2].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row][column-2].getType() == GameEnums::TypeCell::SpawnCell)
        {
          leftValid = false;
        }
      }

      if (topValid && botValid && leftValid)
      {
        adjacents.push_back(mapMatrix[row][column-1]);
      }
    }
  }

  if (column < (_columns - 1))
  {
    bool topValid = true;
    bool botValid = true;
    bool rightValid = true;

    if (mapMatrix[row][column+1].getType() == GameEnums::TypeCell::FreeCell)
    {
      if (row > 0)
      {
        if (mapMatrix[row-1][column+1].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row-1][column+1].getType() == GameEnums::TypeCell::SpawnCell)
        {
          topValid = false;
        }
      }

      if (row < (_rows - 1))
      {
        if (mapMatrix[row+1][column+1].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row+1][column+1].getType() == GameEnums::TypeCell::SpawnCell)
        {
          botValid = false;
        }
      }

      if ((column + 1) < (_columns - 1))
      {
        if (mapMatrix[row][column+2].getType() == GameEnums::TypeCell::PathCell ||
            mapMatrix[row][column+2].getType() == GameEnums::TypeCell::SpawnCell)
        {
          rightValid = false;
        }
      }

      if (topValid && botValid && rightValid)
      {
        adjacents.push_back(mapMatrix[row][column+1]);
      }
    }
  }

  return adjacents;
}
