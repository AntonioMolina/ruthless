#include "Cell.h"

Cell::Cell (int row, int column, GameEnums::TypeCell type)
        : _row(row), _column(column), _type(type), _distance(0), _cost(0)
{}

Cell::Cell (const Cell& other)
{
  _row = other.getRow();
  _column = other.getColumn();
  _type = other.getType();
  _distance = other.getDistance();
}

int Cell::getRow () const
{
  return _row;
}

int Cell::getColumn () const
{
  return _column;
}

GameEnums::TypeCell Cell::getType () const
{
  return _type;
}

int Cell::getDistance () const
{
  return _distance;
}

void Cell::setRow (int row)
{
  _row = row;
}

void Cell::setColumn (int column)
{
  _column = column;
}

void Cell::setType (GameEnums::TypeCell type)
{
  _type = type; 
}

void Cell::setDistance (int distance)
{
  _distance = distance;
}
