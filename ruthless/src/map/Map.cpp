#include "Map.h"

#include <Shapes/OgreBulletCollisionsTrimeshShape.h>
#include <Shapes/OgreBulletCollisionsStaticPlaneShape.h>
#include <Utils/OgreBulletCollisionsMeshToShapeConverter.h>

#include "MapGenerator.h"
#include "PhysicsManager.h"

Map::Map (Cell** mapMatrix, int rows, int columns,
					TypesDef::GraphPtr graph, Ogre::Vector2 core,
	        std::vector<Ogre::Vector2> points, std::vector<Ogre::Vector2> spawns) :
					_mapMatrix(mapMatrix), _rows(rows), _columns(columns),
					_mapGraph(graph), _core(core), _points(points), _spawns(spawns)
{
	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
	    getSceneManager("SceneManager");
	_entMap = sceneMgr->createEntity("TF", "terreno.mesh");
	_nodeMap = sceneMgr->getRootSceneNode()->createChildSceneNode("TF");
	_nodeMap->attachObject(_entMap);
	_nodeMap->setPosition(Ogre::Vector3(0, 0, 0));

	_shapeMap = new OgreBulletCollisions::StaticPlaneCollisionShape(Ogre::Vector3(0, 1, 0), 0);
	_rigBodyMap = new OgreBulletDynamics::RigidBody("Terreno",
      PhysicsManager::getSingletonPtr()->getWorld());

	_rigBodyMap->setStaticShape(_shapeMap, 0.1, 0.8);
}

Map::~Map () {
	if (_shapeMap)
	{
		_shapeMap = nullptr;
	}

	if (_rigBodyMap)
	{
		_rigBodyMap = nullptr;
	}

	Ogre::SceneManager* sceneMgr = Ogre::Root::getSingletonPtr()->
      getSceneManager("SceneManager");

  _nodeMap->detachObject(_entMap);
  sceneMgr->destroyEntity(_entMap);

  _nodeMap->getParentSceneNode()->removeChild(_nodeMap->getName());
  sceneMgr->destroySceneNode(_nodeMap);

	_mapGraph = nullptr;

	int size = _points.size();
	for (int i = 0; i < size; ++i)
	{
		_points.pop_back();
	}

	size = _spawns.size();
	for (int i = 0; i < size; ++i)
	{
		_spawns.pop_back();
	}
}

Ogre::Vector3 Map::getCorePosition(){
	return _mapGraph -> getVertex(_core.x * _columns + _core.y)->getData().getPosition();
}
Ogre::Vector3 Map::getSpawnPosition(Ogre::Vector2 vect){
	return _mapGraph -> getVertex(vect.x * _columns + vect.y)->getData().getPosition();
}
