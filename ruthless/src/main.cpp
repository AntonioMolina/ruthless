#include <iostream>
#include <time.h>

#include <Ogre.h>

/* Managers */
#include "GameManager.h"
#include "PhysicsManager.h"
#include "SoundFXManager.h"
#include "TrackManager.h"

/* States */
#include "IntroState.h"
#include "MenuState.h"
#include "PlayState.h"
#include "PauseState.h"
#include "ControlsState.h"
#include "CreditsState.h"
#include "RankingState.h"
#include "FinalState.h"

/* Factories */
#include "DropFactory.h"
#include "TurretFactory.h"
#include "WeaponFactory.h"

/* Utils */
#include "MapConverterToGraph.h"
#include "MapGenerator.h"
#include "MouseConverter.h"

int main() {
  srand(time(0));

  /* Managers */
  auto game = new GameManager;
  new PhysicsManager;
  new SoundFXManager;
  new TrackManager;

  /* States */
  new ControlsState;
  new CreditsState;
  new IntroState;
  new MenuState;
  new PlayState;
  new PauseState;
  new RankingState;
  new FinalState;

  /* Factories */
  new DropFactory;
  new TurretFactory;
  new WeaponFactory;

  /* Utils & Map Generator */
  new MapConverterToGraph;
  new MapGenerator;
  new MouseConverter;

  try {
    // Inicializa el juego y transición al primer estado.
    game->start(IntroState::getSingletonPtr());
  } catch (Ogre::Exception& e) {
    std::cerr << "Excepción detectada: " << e.getFullDescription();
  }

  return 0;
}
